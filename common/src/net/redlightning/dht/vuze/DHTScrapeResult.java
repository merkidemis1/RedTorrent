/*
 * This file is part of mlDHT. mlDHT is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or (at your option) any later version. mlDHT is distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details. You should have received a copy of
 * the GNU General Public License along with mlDHT. If not, see
 * <http://www.gnu.org/licenses/>.
 */
package net.redlightning.dht.vuze;

import java.net.MalformedURLException;
import java.net.URL;
import net.redlightning.redtorrent.common.Downloader;

public class DHTScrapeResult {
	public static final int RT_SUCCESS = 1;
	public static final int RT_ERROR = 2;

	private int seedCount;
	private int peerCount;
	private long scrapeStartTime;

	public DHTScrapeResult(Downloader dl, int seeds, int peers) {
		seedCount = seeds;
		peerCount = peers;
	}

	public Downloader getDownload() {
		// TODO Auto-generated method stub
		return null;
	}

	public long getNextScrapeStartTime() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getNonSeedCount() {
		return peerCount;
	}

	public int getResponseType() {
		return RT_SUCCESS;
	}

	void setScrapeStartTime(long time) {
		scrapeStartTime = time;
	}

	public long getScrapeStartTime() {
		return scrapeStartTime;
	}

	public int getSeedCount() {
		return seedCount;
	}

	public String getStatus() {
		return null;
	}

	public URL getURL() {
		try {
			return new URL("dht", "mldht", "scrape");
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void setNextScrapeStartTime(long nextScrapeStartTime) {
	// TODO Auto-generated method stub
	}
}
