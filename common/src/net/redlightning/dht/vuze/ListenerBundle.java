/**
 * 
 */
package net.redlightning.dht.vuze;

import net.redlightning.redtorrent.common.Downloader;

/**
 * @author Damokles
 * @author Michael Isaacson (Android port)
 */
public class ListenerBundle {
	public int WRITTEN = 1;
	public int WILL_BE_READ = 2;
	private Tracker tracker;

	public ListenerBundle(Tracker tracker) {
		this.tracker = tracker;
	}

	public void cleanup(Downloader download) {
		//download.removeAttributeListener(this, tracker.getNetworks(), WRITTEN);
		//download.removeAttributeListener(this, tracker.getPeerSources(), WRITTEN);
		download.removeListener(this);
	}

	//---------------------[DownloadListener]---------------------------------
	/*
	 * (non-Javadoc)
	 * @see org.gudy.azureus2.plugins.download.DownloadListener#positionChanged
	 * (org.gudy.azureus2.plugins.download.Download, int, int)
	 */
	public void positionChanged(Downloader download, int oldPosition, int newPosition) {}

	/*
	 * (non-Javadoc)
	 * @see org.gudy.azureus2.plugins.download.DownloadListener#stateChanged(
	 * org.gudy.azureus2.plugins.download.Download, int, int)
	 */
	public void stateChanged(Downloader download, int old_state, int new_state) {
		tracker.checkDownload(download);
	}

	//---------------------[DownloadAttributeListener]---------------------------------

	/*
	 * (non-Javadoc)
	 * @seeorg.gudy.azureus2.plugins.download.DownloadAttributeListener#
	 * attributeEventOccurred(org.gudy.azureus2.plugins.download.Download,
	 * org.gudy.azureus2.plugins.torrent.TorrentAttribute, int)
	 */
	public void attributeEventOccurred(Downloader download, TorrentAttribute attribute, int event_type) {
		if (event_type == WRITTEN && (attribute == tracker.getNetworks() || attribute == tracker.getPeerSources())) {
			tracker.checkDownload(download);
		}

	}

	//---------------------[DownloadTrackerListener]---------------------------------
	/*
	 * (non-Javadoc)
	 * @see
	 * org.gudy.azureus2.plugins.download.DownloadTrackerListener#announceResult
	 * (org.gudy.azureus2.plugins.download.DownloadAnnounceResult)
	 */
	public void announceResult(DownloadAnnounceResult result) {
		tracker.checkDownload(result.getDownload());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.gudy.azureus2.plugins.download.DownloadTrackerListener#scrapeResult
	 * (org.gudy.azureus2.plugins.download.DownloadScrapeResult)
	 */
	public void scrapeResult(DHTScrapeResult result) {
		tracker.checkDownload(result.getDownload());
	}

	//---------------------[DownloadTrackerListener]---------------------------------
	/*
	 * (non-Javadoc)
	 * @see
	 * org.gudy.azureus2.plugins.download.DownloadManagerListener#downloadAdded
	 * (org.gudy.azureus2.plugins.download.Download)
	 */
	public void downloadAdded(Downloader download) {
		//download.addAttributeListener(this, tracker.getNetworks(), WRITTEN);
		//download.addAttributeListener(this, tracker.getPeerSources(), WRITTEN);
		download.addListener(this);
		//download.addTrackerListener(this);
		tracker.checkDownload(download);
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.gudy.azureus2.plugins.download.DownloadManagerListener#
	 * downloadRemoved(org.gudy.azureus2.plugins.download.Download)
	 */
	public void downloadRemoved(Downloader download) {
		cleanup(download);
		tracker.removeTrackedTorrent(download, "Download was removed");
	}

}
