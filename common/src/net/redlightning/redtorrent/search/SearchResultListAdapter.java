/**
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.search;

import java.util.List;
import net.redlightning.json.JSONException;
import net.redlightning.json.JSONObject;
import net.redlightning.redtorrent.common.TorrentActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Fills in the views for each returned search result
 * 
 * @author Michael Isaacson
 * @version 11.12.21
 */
public class SearchResultListAdapter extends ArrayAdapter<JSONObject> {
	//private final static String TAG = SearchResultListAdapter.class.getSimpleName();
	private final transient TorrentActivity context;
	private final transient List<JSONObject> items;
	public static int itemLayout;
	public static int name;
	public static int size;
	public static int peers;
	public static int searchResultsList;

	/**
	 * 
	 * @param searchResultItemLayout
	 * @param list
	 * @param nameView
	 * @param sizeView
	 * @param peersView
	 * @param ratingView
	 */
	public static void initialize(final int searchResultItemLayout, final int list, final int nameView, final int sizeView, final int peersView) {
		itemLayout = searchResultItemLayout;
		searchResultsList = list;
		name = nameView;
		size = sizeView;
		peers = peersView;
	}

	/**
	 * Constructor
	 * 
	 * @param bundle the current context
	 * @param resourceID the view ID
	 * @param items the list of items to add
	 */
	public SearchResultListAdapter(final TorrentActivity bundle, final int resourceID, final List<JSONObject> items) {
		super(bundle, resourceID, items);
		this.items = items;
		context = bundle;
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		View view = convertView;

		if (view == null) {
			final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(itemLayout, null);
		}
		//ISOHunt: exempts,pubDate,files,Seeds,original_site,hash,link,votes,original_link,leechers,downloads,size,enclosure_url,guid,category,title,kws,length,tracker_url,tracker,comments
		final JSONObject json = items.get(position);
		if (json != null) {
			try {
				final String title = json.getString("title").replace("<b>", "").replace("</b>", "");

				view.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(final View arg0) {
						TorrentSearch.resultsDialog.dismiss();
						try {
							TorrentSearch.progressDialog = ProgressDialog.show(context, "", "Downloading .torrent...", true);
							final TorrentFileDownloader downloader = new TorrentFileDownloader(title, json.getString("enclosure_url"), TorrentSearch.handler);
							downloader.start();
						} catch (final JSONException e) {
							e.printStackTrace();
						}
					}
				});
				((TextView) view.findViewById(name)).setText(title);
				((TextView) view.findViewById(size)).setText(json.getString("size"));
				((TextView) view.findViewById(peers)).setText("Peers: " + json.getInt("Seeds") + "/" + json.getInt("leechers"));
			} catch (final JSONException e) {
				e.printStackTrace();
			}
		}
		return view;
	}
}