/**
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.search;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import net.redlightning.redtorrent.common.Settings;
import android.os.Handler;

/**
 * Downloads a .torrent file, and can add it to the download queue
 * 
 * @author Michael Isaacson
 * @version 11.9.29
 */
public class TorrentFileDownloader extends Thread {
	//private static final String TAG = ISOHunt.class.getCanonicalName();
	public static final int DOWNLOAD_ERROR = 100000;
	public static final int DOWNLOAD_SUCCESS = 100001;
	private String name;
	private String torrentURL;
	private Handler handler;

	/**
	 * Constructor
	 * 
	 * @param torrentURL
	 * @param handler
	 * @param autoStart
	 */
	public TorrentFileDownloader(String name, String torrentURL, Handler handler) {
		super("TorrentFileDownloader");
		this.name = name;
		this.torrentURL = torrentURL;
		this.handler = handler;
	}

	/*
	 * (non-Javadoc)
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 */
	@Override
	public void run() {
		try {
			final URL source = new URL(torrentURL);
			final URLConnection uc = source.openConnection();
			final InputStream in = uc.getInputStream();
			final FileOutputStream out = new FileOutputStream(Settings.getDownloadPath() + name + ".torrent", false);

			byte[] buffer = new byte[1024];
			int len = in.read(buffer);
			while (len != -1) {
				out.write(buffer, 0, len);
				len = in.read(buffer);
			}

			in.close();
			out.close();

			handler.sendMessage(handler.obtainMessage(DOWNLOAD_SUCCESS, Settings.getDownloadPath() + name + ".torrent"));
		} catch (MalformedURLException e) {
			TorrentSearch.handler.dispatchMessage(TorrentSearch.handler.obtainMessage(DOWNLOAD_ERROR, e.getMessage()));
			e.printStackTrace();
		} catch (IOException e) {
			TorrentSearch.handler.dispatchMessage(TorrentSearch.handler.obtainMessage(DOWNLOAD_ERROR, e.getMessage()));
			e.printStackTrace();
		}
	}
}
