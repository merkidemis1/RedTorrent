/**
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.common;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * Handle when the user taps on the "Manage Networks" button in the Settings activity
 * 
 * @author Michael Isaacson
 * @version 12.6.27
 */
@SuppressWarnings("rawtypes")
public class SSIDManageListener implements OnClickListener {
	private final Context context;
	private final Class activity;

	/**
	 * Constructor
	 * 
	 * @param context the current application context
	 * @param activity the activity that will be started
	 */
	public SSIDManageListener(final Context context, final Class activity) {
		this.context = context;
		this.activity = activity;
	}

	/*
	 * (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(final View view) {
		final Intent intent = new Intent(context, activity);
		context.startActivity(intent);
	}
}