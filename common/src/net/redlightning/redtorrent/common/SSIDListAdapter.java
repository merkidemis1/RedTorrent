/**
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.common;

import java.util.List;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author Michael Isaacson
 * @version 12.6.27
 */
public class SSIDListAdapter extends ArrayAdapter<String> {
	protected static final String TAG = SSIDListAdapter.class.getSimpleName();
	private final transient Context context;
	private final transient List<String> items;
	public static int layout;
	public static int name;
	public static int removeSSID;
	public static int listView;

	public static void setViews(final int layoutID, final int list, final int nameView, final int removeSSIDView) {
		layout = layoutID;
		listView = list;
		name = nameView;
		removeSSID = removeSSIDView;
	}

	/**
	 * Constructor
	 * 
	 * @param context
	 * @param textViewResourceId
	 * @param objects
	 */
	public SSIDListAdapter(final Context context, final int textViewResourceId, final List<String> objects) {
		super(context, textViewResourceId, objects);
		this.context = context;
		this.items = objects;
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		View view;

		if (convertView == null) {
			final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(layout, null);
		} else {
			view = convertView;
		}
		
		TextView nameView;
		ImageView removeView;

		nameView = (TextView) view.findViewById(name);
		removeView = (ImageView) view.findViewById(removeSSID);
		nameView.setText(items.get(position));
		removeView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.i(TAG, "Removing ssid " + items.get(position));
				Settings.getWifiNetworkSSIDs().remove(position);
				SSIDListAdapter.this.notifyDataSetChanged();
				TorrentService.saveSettings();
			}});
		return view;
	}
}