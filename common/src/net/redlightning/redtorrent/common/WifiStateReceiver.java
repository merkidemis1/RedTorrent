/**
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

/**
 * Checks for any limitations for downloading over WiFi when the WiFi state changes
 * 
 * @author Michael Isaacson
 * @version 12.6.29
 */
public class WifiStateReceiver extends BroadcastReceiver {
	/*
	 * (non-Javadoc)
	 * @see android.content.BroadcastReceiver#onReceive(android.content.Context,
	 * android.content.Intent)
	 */
	@Override
	public void onReceive(final Context context, final Intent intent) {
		if (TorrentService.settings == null) {
			TorrentService.loadSettings(context);
		}

		if ((!isWifiConnected(context) && Settings.isWifiOnly()) || (isWifiConnected(context) && !isConnectedToListedSSID(context))) {
			TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.STOP_ALL));
		} else if (Settings.isWifiResume() && isWifiConnected(context) && isConnectedToListedSSID(context)) {
			TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.START_ALL));
		}
	}

	/**
	 * Check to see if WiFi is connected
	 * 
	 * @param context the current application context
	 * @return true if WiFi is connected, false if not
	 */
	public static boolean isWifiConnected(final Context context) {
		return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
	}

	/**
	 * Check to see if we are connected to an approved SSID, if such a restriction was selected
	 * 
	 * @param context the current application context
	 * @return true if we are not using SSID restrictions or we are connected to an approved SSID
	 */
	public static boolean isConnectedToListedSSID(final Context context) {
		final WifiInfo wifiManager = ((WifiManager) context.getSystemService(Context.WIFI_SERVICE)).getConnectionInfo();
		//Either we aren't using the restriction or we are connected to an approved SSID
		return (!Settings.isWifiNetworks() || Settings.getWifiNetworkSSIDs().contains(wifiManager.getSSID()));
	}
}