/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Handle when the power is connected/disconnected
 * 
 * @author Michael Isaacson
 * @version 11.9.29
 */
public class PowerStateReceiver extends BroadcastReceiver {
	public static final boolean POWER_CONNECTED = true;
	public static final boolean POWER_DISCONNECTED = false;
	private static boolean pluggedIn = false;

	/*
	 * (non-Javadoc)
	 * @see android.content.BroadcastReceiver#onReceive(android.content.Context,
	 * android.content.Intent)
	 */
	@Override
	public void onReceive(final Context context, final Intent intent) {
		TorrentService.loadSettings(context);

		if (intent.getAction().equals(Intent.ACTION_POWER_CONNECTED)) {
			setPluggedIn(POWER_CONNECTED);
		} else if (intent.getAction().equals(Intent.ACTION_POWER_DISCONNECTED)) {
			setPluggedIn(POWER_DISCONNECTED);
		}

		if (!isPluggedIn() && Settings.isPluggedInOnly()) {
			TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.STOP_ALL));
		} else if (isPluggedIn() && Settings.isPluggedInResume()) {
			TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.START_ALL));
		}
	}

	/**
	 * @return the pluggedIn
	 */
	public static synchronized boolean isPluggedIn() {
		return pluggedIn;
	}
	
	/**
	 * @param pluggedIn the pluggedIn to set
	 */
	public static synchronized void setPluggedIn(final boolean pluggedIn) {
		PowerStateReceiver.pluggedIn = pluggedIn;
	}
}