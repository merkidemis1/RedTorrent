/**
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.common;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

/**
 * @author Michael Isaacson
 * @version 10.12.23
 */
public class TorrentActionListener implements OnClickListener {
	//private static final String TAG = TorrentActionListener.class.getSimpleName();
	private final transient int position;
	private final transient TorrentActivity context;

	/**
	 * Constructor
	 * 
	 * @param activity the calling activity
	 * @param pos the position of the item in the ListView
	 */
	protected TorrentActionListener(final TorrentActivity activity, final int pos) {
		position = pos;
		context = activity;
	}


	/*
	 * (non-Javadoc)
	 * @see android.content.DialogInterface.OnClickListener#onClick(android.content.DialogInterface, int)
	 */
	@Override
	public void onClick(final DialogInterface dialog, final int which) {
		if (which == 0) {
			//STOP/RESUME
			if (TorrentActivity.getAdapter().getItem(position).getManager().getTorrent().getStatus().equals("Stopped") || TorrentActivity.getAdapter().getItem(position).getManager().getTorrent().getStatus().equals("Queued")) {
				TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.START, TorrentActivity.getAdapter().getItem(position)));
			} else {
				TorrentActivity.getAdapter().getItem(position).stopDownloading();
				TorrentActivity.getAdapter().getItem(position).getManager().getTorrent().setStatus("Stopped");
			}
		} else {
			//REMOVE
			TorrentService.removeTorrent(context, position);
		}
		TorrentActivity.getAdapter().notifyDataSetChanged();
		dialog.dismiss();
	}
}
