/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.common;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * @author Michael Isaacson
 * @version 12.7.3
 */
public class TimeframeReceiver extends BroadcastReceiver {
	//private static final String TAG = TimeframeReceiver.class.getSimpleName();
	public static final int START_ALARM_ID = 432095743;
	public static final int END_ALARM_ID = 432095744;
	public static final String START = "START";
	public static final String END = "END";
	protected static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
	private static boolean timeframeActive = false;

	/*
	 * (non-Javadoc)
	 * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
	 */
	@Override
	public void onReceive(final Context context, final Intent intent) {
		TorrentService.loadSettings(context);

		if (!isTimeframeActive() && Settings.isTimeframeOnly()) {
			TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.STOP_ALL));
		} else if (isTimeframeActive() && Settings.isTimeframeOnly() && Settings.isTimeframeResume()) {
			TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.START_ALL));
		}
	}

	/**
	 * @return the timeframeActive
	 */
	public static boolean isTimeframeActive() {
		setTimeframeActive(false);
		final Calendar timeframeStart = Calendar.getInstance();
		timeframeStart.set(Calendar.HOUR_OF_DAY, Settings.getStartHour());
		timeframeStart.set(Calendar.MINUTE, Settings.getStartMinute());
		timeframeStart.set(Calendar.SECOND, 0);
		timeframeStart.set(Calendar.MILLISECOND, 0);

		final Calendar timeframeEnd = Calendar.getInstance();
		;
		timeframeEnd.set(Calendar.HOUR_OF_DAY, Settings.getEndHour());
		timeframeEnd.set(Calendar.MINUTE, Settings.getEndMinute());
		timeframeEnd.set(Calendar.SECOND, 0);
		timeframeEnd.set(Calendar.MILLISECOND, 0);

		final Calendar startOfDay = Calendar.getInstance();
		startOfDay.set(Calendar.HOUR_OF_DAY, 0);
		startOfDay.set(Calendar.MINUTE, 0);
		startOfDay.set(Calendar.SECOND, 0);
		startOfDay.set(Calendar.MILLISECOND, 0);

		final Calendar endOfDay = Calendar.getInstance();
		endOfDay.set(Calendar.HOUR_OF_DAY, 23);
		endOfDay.set(Calendar.MINUTE, 59);
		endOfDay.set(Calendar.SECOND, 59);
		endOfDay.set(Calendar.MILLISECOND, 0);

		final Calendar current = Calendar.getInstance();

		if (timeframeStart.before(timeframeEnd) && (current.after(timeframeStart) || current.equals(timeframeStart)) && current.before(timeframeEnd)) {
			setTimeframeActive(true);
		}
		if (current.after(startOfDay) && timeframeStart.after(timeframeEnd) && current.before(endOfDay) && current.after(timeframeEnd)) {
			setTimeframeActive(true);
		}

		return timeframeActive;
	}

	/**
	 * @param timeframeActive the timeframeActive to set
	 */
	public static void setTimeframeActive(boolean timeframeActive) {
		TimeframeReceiver.timeframeActive = timeframeActive;
	}
}