/**
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.common;

import java.util.Map;
import net.redlightning.jbittorrent.TorrentFile;
import net.redlightning.jbittorrent.TorrentProcessor;
import android.app.AlertDialog;
import android.util.Log;

/**
 * Create dialogs to make Settings
 * 
 * @author Michael Isaacson
 * @version 11.2.14
 */
public class TorrentFilePickerDialog {
	private static final String TAG = TorrentFilePickerDialog.class.getSimpleName();
	private transient TorrentActivity context;
	private transient AlertDialog.Builder builder;
	private static AlertDialog alertDialog;
	private String torrentFileName;
	private TorrentFile torrent = null;

	/**
	 * Constructor
	 * 
	 * @param activity
	 * @param torrentFile
	 * @param pickedFiles 
	 */
	public TorrentFilePickerDialog(final TorrentActivity activity, String torrentFile) {
		context = activity;
		builder = new AlertDialog.Builder(activity);
		builder.setCancelable(true);
		torrentFileName = torrentFile;

		Map<?, ?> data;
		TorrentProcessor processor;
		processor = new TorrentProcessor();
		data = processor.parseTorrent(torrentFile);

		if (data != null) {
			torrent = processor.getTorrentFile(data);
		} else {
			Log.w(TAG, "Torrent data is null");
		}
	}

	/**
	 * Build the dialog
	 * 
	 * @return the prepared dialog
	 */
	public AlertDialog build() {
		TorrentFilePickerListener listener;
		CharSequence[] files;
		boolean[] selected;

		if (torrent != null && torrent.getFileNames() != null) {
			files = new CharSequence[torrent.getFileNames().size()];
			selected = new boolean[torrent.getFileNames().size()];
			for (int i = 0; i < files.length; i++) {
				files[i] = torrent.getFileNames().get(i);
				selected[i] = true;
			}
		} else {
			files = new CharSequence[0];
			selected = new boolean[0];
		}
		
		listener = new TorrentFilePickerListener(context, files, selected, torrentFileName);
		builder.setIcon(android.R.drawable.ic_menu_add);
		builder.setTitle("Select Files");
		builder.setMultiChoiceItems(files, selected, listener);
		builder.setNegativeButton("Cancel", listener);
		builder.setNeutralButton("Clear All", listener);
		builder.setPositiveButton("Ok", listener);
		alertDialog = builder.create();
		return alertDialog;
	}
}
