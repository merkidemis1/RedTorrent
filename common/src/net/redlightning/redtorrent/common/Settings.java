/**
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.common;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView.OnEditorActionListener;

/**
 * @author Michael Isaacson
 * @version 12.7.3
 */
public class Settings extends Activity {
	protected static final String TAG = Settings.class.getSimpleName();

	public static final int SAVE_PATH_REQUEST = 500;
	public static Context context;
	public static String savePathClassName;
	/*
	 * GUI Controls
	 */
	public static CheckBox wifiOnlyCheckbox;
	public static CheckBox wifiResumeCheckbox;
	public static CheckBox wifiNetworksCheckbox;
	public static Button wifiNetworksButton;
	public static CheckBox pluggedInOnlyCheckbox;
	public static CheckBox pluggedInResumeCheckbox;
	public static CheckBox useOngoingCheckbox;
	public static CheckBox timeframeOnlyCheckbox;
	public static CheckBox timeframeResumeCheckbox;
	public static TimePicker startTimePicker;
	public static TimePicker endTimePicker;
	public static TextView startTimeLabel;
	public static TextView endTimeLabel;
	public static CheckBox dhtBox;
	public static EditText downloadPathEdit;
	public static Button downloadButton;
	public static SeekBar piecesBar;
	public static SeekBar connectionsBar;
	public static SeekBar maxActiveBar;
	public static transient TextView piecesText;
	public static transient TextView connectionsText;
	public static transient TextView maxActiveText;
	public static transient EditText lowPortEdit;
	public static transient EditText highPortEdit;

	/*
	 * Settings
	 */
	private static short highPort = 6889;
	private static short lowPort = 6881;
	private static int maxPieces = 30;
	private static int maxConnections = 10;
	private static int maxActive = 3;
	private static boolean pluggedInResume = false;
	private static boolean pluggedInOnly = false;
	private static boolean wifiResume = false;
	private static boolean wifiOnly = false;
	private static boolean wifiNetworks = false;

	private static boolean timeframeOnly = false;
	private static boolean timeframeResume = false;
	private static int startHour = 0;
	private static int startMinute = 0;
	private static int endHour = 0;
	private static int endMinute = 0;
	private static String downloadPath = "/sdcard/download/";
	private static boolean useDHT = true;
	private static boolean useOngoing = true;
	private static int searchProvider = 0;
	private static List<String> wifiNetworkSSIDs = new ArrayList<String>();

	//Click listener for the WiFi only checkbox
	protected final transient OnClickListener wifiClickListener = new View.OnClickListener() {
		@Override
		public void onClick(final View view) {
			final CheckBox wifiBox = (CheckBox) view;
			wifiOnly = wifiBox.isChecked();
			TorrentService.saveSettings();
		}
	};

	//Click listener for the WiFi resume checkbox
	protected final transient OnClickListener resumeClickListener = new View.OnClickListener() {
		@Override
		public void onClick(final View view) {
			final CheckBox resumeBox = (CheckBox) view;
			wifiResume = resumeBox.isChecked();
			TorrentService.saveSettings();
		}
	};

	//Click listener for the restrict to specific networks checkbox
	protected final transient OnClickListener wifiNetworksClickListener = new View.OnClickListener() {
		@Override
		public void onClick(final View view) {
			final CheckBox networksBox = (CheckBox) view;
			wifiNetworks = networksBox.isChecked();
			if (networksBox.isChecked()) {
				wifiNetworksButton.setVisibility(View.VISIBLE);
			} else {
				wifiNetworksButton.setVisibility(View.GONE);
			}
			TorrentService.saveSettings();
		}
	};

	//Click listener for the Plugged In Only checkbox
	protected final transient OnClickListener pluggedInClickListener = new View.OnClickListener() {
		@Override
		public void onClick(final View view) {
			final CheckBox pluggedInBox = (CheckBox) view;
			pluggedInOnly = pluggedInBox.isChecked();
			TorrentService.saveSettings();
			//			if (pluggedInBox.isChecked()) {
			//				pluggedInResumeCheckbox.setVisibility(View.VISIBLE);
			//			} else {
			//				pluggedInResumeCheckbox.setVisibility(View.GONE);
			//			}
		}
	};

	//Click listener for the Plugged-in resume checkbox
	protected final transient OnClickListener pluggedInResumeClickListener = new View.OnClickListener() {
		@Override
		public void onClick(final View view) {
			final CheckBox pluggedInResumeBox = (CheckBox) view;
			pluggedInResume = pluggedInResumeBox.isChecked();
			TorrentService.saveSettings();
		}
	};

	//Click listener for the Timeframe Only checkbox
	protected final transient OnClickListener timeframeClickListener = new View.OnClickListener() {
		@Override
		public void onClick(final View view) {
			final CheckBox timeframeBox = (CheckBox) view;
			timeframeOnly = timeframeBox.isChecked();
			TorrentService.saveSettings();
			if (timeframeBox.isChecked()) {
				startTimeLabel.setVisibility(View.VISIBLE);
				endTimeLabel.setVisibility(View.VISIBLE);
				timeframeResumeCheckbox.setVisibility(View.VISIBLE);
				startTimePicker.setVisibility(View.VISIBLE);
				endTimePicker.setVisibility(View.VISIBLE);
			} else {
				startTimeLabel.setVisibility(View.GONE);
				endTimeLabel.setVisibility(View.GONE);
				timeframeResumeCheckbox.setVisibility(View.GONE);
				startTimePicker.setVisibility(View.GONE);
				endTimePicker.setVisibility(View.GONE);
			}
		}
	};

	//Click listener for the start time picker
	protected final transient OnTimeChangedListener startTimeListener = new OnTimeChangedListener() {
		@Override
		public void onTimeChanged(final TimePicker picker, final int hourOfDay, final int minute) {
			startHour = hourOfDay;
			startMinute = minute;
			//Log.i(TAG, "Start time set: " + startHour + ":" + startMinute);
			TorrentService.saveSettings();

			final Calendar triggerTime = Calendar.getInstance();
			triggerTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
			triggerTime.set(Calendar.MINUTE, minute);
			triggerTime.set(Calendar.SECOND, 0);
			triggerTime.set(Calendar.MILLISECOND, 0);
			final PendingIntent sender = PendingIntent.getBroadcast(context, TimeframeReceiver.START_ALARM_ID, new Intent(context, TimeframeReceiver.class), PendingIntent.FLAG_UPDATE_CURRENT);
			final AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarm.setRepeating(AlarmManager.RTC, triggerTime.getTimeInMillis(), AlarmManager.INTERVAL_DAY, sender);
		}
	};

	//Click listener for the end time picker
	protected final transient OnTimeChangedListener endTimeListener = new OnTimeChangedListener() {
		@Override
		public void onTimeChanged(final TimePicker picker, final int hourOfDay, final int minute) {
			endHour = hourOfDay;
			endMinute = minute;
			//Log.i(TAG, "End time set: " + endHour + ":" + endMinute);
			TorrentService.saveSettings();
			final Calendar triggerTime = Calendar.getInstance();
			triggerTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
			triggerTime.set(Calendar.MINUTE, minute);
			triggerTime.set(Calendar.SECOND, 0);
			triggerTime.set(Calendar.MILLISECOND, 0);
			final PendingIntent sender = PendingIntent.getBroadcast(context, TimeframeReceiver.END_ALARM_ID, new Intent(context, TimeframeReceiver.class), PendingIntent.FLAG_UPDATE_CURRENT);
			final AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarm.setRepeating(AlarmManager.RTC, triggerTime.getTimeInMillis(), AlarmManager.INTERVAL_DAY, sender);
		}
	};

	//Click listener for the Timeframe resume checkbox
	protected final transient OnClickListener timeframeResumeClickListener = new View.OnClickListener() {
		@Override
		public void onClick(final View view) {
			final CheckBox fimeframeResumeBox = (CheckBox) view;
			timeframeResume = fimeframeResumeBox.isChecked();
			TorrentService.saveSettings();
		}
	};

	//Click listener for the path search button
	protected final transient OnClickListener pathSearchClickListener = new View.OnClickListener() {
		@Override
		public void onClick(final View view) {
			final Intent intent = new Intent(savePathClassName);
			intent.setClassName(context, savePathClassName);
			startActivityForResult(intent, SAVE_PATH_REQUEST);
		}
	};

	//Save the new path when the edit box looses focus
	protected final transient OnEditorActionListener pathEditListener = new TextView.OnEditorActionListener() {
		@Override
		public boolean onEditorAction(final TextView view, final int actionID, final KeyEvent event) {
			if (((event != null) && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionID == EditorInfo.IME_ACTION_DONE)) {
				setDownloadPath();
			}
			return false;
		}
	};

	//Save the new path when the edit box looses focus
	protected final transient OnFocusChangeListener pathFocusListener = new View.OnFocusChangeListener() {
		@Override
		public void onFocusChange(final View view, final boolean hasFocus) {
			if (!hasFocus) {
				setDownloadPath();
			}
		}
	};

	//Save the new path when the edit box looses focus
	protected final transient OnEditorActionListener portEditListener = new TextView.OnEditorActionListener() {
		@Override
		public boolean onEditorAction(final TextView view, final int actionID, final KeyEvent event) {
			if (((event != null) && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionID == EditorInfo.IME_ACTION_DONE)) {
				setPorts();
			}
			return false;
		}
	};

	//Save the new path when the edit box looses focus
	protected final transient OnFocusChangeListener portFocusListener = new View.OnFocusChangeListener() {
		@Override
		public void onFocusChange(final View view, final boolean hasFocus) {
			if (!hasFocus) {
				setPorts();
			}
		}
	};

	// Handle the changes to the Max Active Torrents slider
	protected final transient OnSeekBarChangeListener maxActiveChangeListener = new OnSeekBarChangeListener() {
		@Override
		public void onProgressChanged(final SeekBar seekBar, final int progress, final boolean fromUser) {
			maxActiveText.setText(String.valueOf(progress + 1));
			maxActive = progress + 1;
			TorrentService.saveSettings();
		}

		@Override
		public void onStartTrackingTouch(final SeekBar seekBar) {
			//Nothing to do here
		}

		@Override
		public void onStopTrackingTouch(final SeekBar seekBar) {
			//Nothing to do here
		}
	};

	// Handle the changes to the Max Connections slider
	protected final transient OnSeekBarChangeListener maxConnectionsChangeListener = new OnSeekBarChangeListener() {
		@Override
		public void onProgressChanged(final SeekBar seekBar, final int progress, final boolean fromUser) {
			connectionsText.setText(String.valueOf((10 * progress) + 10));
			maxConnections = (10 * progress) + 10;
			TorrentService.saveSettings();
		}

		@Override
		public void onStartTrackingTouch(final SeekBar seekBar) {
			//Nothing to do here
		}

		@Override
		public void onStopTrackingTouch(final SeekBar seekBar) {
			//Nothing to do here
		}
	};

	// Handle the changes to the Max Pieces slider
	protected final transient OnSeekBarChangeListener piecesChangeListener = new OnSeekBarChangeListener() {
		@Override
		public void onProgressChanged(final SeekBar seekBar, final int progress, final boolean fromUser) {
			piecesText.setText(String.valueOf((5 * progress) + 5));
			maxPieces = (5 * progress) + 5;
			TorrentService.saveSettings();
		}

		@Override
		public void onStartTrackingTouch(final SeekBar seekBar) {
			//Nothing to do here
		}

		@Override
		public void onStopTrackingTouch(final SeekBar seekBar) {
			//Nothing to do here
		}
	};

	//Click listener for the Use Ongoing Notification checkbox
	protected final transient OnClickListener useOngoingClickListener = new View.OnClickListener() {
		@Override
		public void onClick(final View view) {
			final CheckBox useOngoingBox = (CheckBox) view;
			useOngoing = useOngoingBox.isChecked();
			if (useOngoing) {
				TorrentService.displayOngoingNotification();
			} else {
				TorrentService.cancelOngoingNotification();
			}
			TorrentService.saveSettings();
		}
	};

	/**
	 * @return the downloadPath
	 */
	public static synchronized String getDownloadPath() {
		return downloadPath;
	}

	/**
	 * @return the endHour
	 */
	public static int getEndHour() {
		return endHour;
	}

	/**
	 * @return the endMinute
	 */
	public static int getEndMinute() {
		return endMinute;
	}

	/**
	 * @return the highPort
	 */
	public static synchronized short getHighPort() {
		return highPort;
	}

	/**
	 * @return the lowPort
	 */
	public static synchronized short getLowPort() {
		return lowPort;
	}

	/**
	 * @return the maxActive
	 */
	public static synchronized int getMaxActive() {
		return maxActive;
	}

	/**
	 * @return the maxConnections
	 */
	public static synchronized int getMaxConnections() {
		return maxConnections;
	}

	/**
	 * @return the maxPieces
	 */
	public static synchronized int getMaxPieces() {
		return maxPieces;
	}

	/**
	 * @return the searchProvider
	 */
	public static synchronized int getSearchProvider() {
		return searchProvider;
	}

	/**
	 * @return the startHour
	 */
	public static int getStartHour() {
		return startHour;
	}

	/**
	 * @return the startMinute
	 */
	public static int getStartMinute() {
		return startMinute;
	}

	/**
	 * @return the wifiNetworkSSIDs
	 */
	public static List<String> getWifiNetworkSSIDs() {
		return wifiNetworkSSIDs;
	}

	/**
	 * @return the SSID list as a pipe delimited string
	 */
	public static String getWifiNetworkSSIDString() {
		final StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < wifiNetworkSSIDs.size(); i++) {
			buffer.append(wifiNetworkSSIDs.get(i));
			buffer.append("\t");
		}
		return buffer.toString();
	}

	/**
	 * @return the pluggedInOnly
	 */
	public static synchronized boolean isPluggedInOnly() {
		return pluggedInOnly;
	}

	/**
	 * @return the pluggedInResume
	 */
	public static synchronized boolean isPluggedInResume() {
		return pluggedInResume;
	}

	/**
	 * @return the timeframeOnly
	 */
	public static boolean isTimeframeOnly() {
		return timeframeOnly;
	}

	/**
	 * @return the timeframeResume
	 */
	public static boolean isTimeframeResume() {
		return timeframeResume;
	}

	/**
	 * @return the useDHT
	 */
	public static synchronized boolean isUseDHT() {
		return useDHT;
	}

	/**
	 * @return the wifiNetworks
	 */
	public static boolean isWifiNetworks() {
		return wifiNetworks;
	}

	/**
	 * @return the wifiOnly
	 */
	public static synchronized boolean isWifiOnly() {
		return wifiOnly;
	}

	/**
	 * @return the wifiResume
	 */
	public static synchronized boolean isWifiResume() {
		return wifiResume;
	}

	/**
	 * @param downloadPath the downloadPath to set
	 */
	public static synchronized void setDownloadPath(final String downloadPath) {
		Settings.downloadPath = downloadPath;
	}

	/**
	 * @param endHour the endHour to set
	 */
	public static void setEndHour(final int endHour) {
		Settings.endHour = endHour;
	}

	/**
	 * @param endMinute the endMinute to set
	 */
	public static void setEndMinute(final int endMinute) {
		Settings.endMinute = endMinute;
	}

	/**
	 * @param highPort the highPort to set
	 */
	public static synchronized void setHighPort(final short highPort) {
		Settings.highPort = highPort;
	}

	/**
	 * @param lowPort the lowPort to set
	 */
	public static synchronized void setLowPort(final short lowPort) {
		Settings.lowPort = lowPort;
	}

	/**
	 * @param maxActive the maxActive to set
	 */
	public static synchronized void setMaxActive(final int maxActive) {
		Settings.maxActive = maxActive;
	}

	/**
	 * @param maxConnections the maxConnections to set
	 */
	public static synchronized void setMaxConnections(final int maxConnections) {
		if (maxConnections > 250) {
			Settings.maxConnections = 250;
		} else {
			Settings.maxConnections = maxConnections;
		}
	}

	/**
	 * @param maxPieces the maxPieces to set
	 */
	public static synchronized void setMaxPieces(final int maxPieces) {
		if (maxPieces > 50) {
			Settings.maxPieces = 50;
		} else {
			Settings.maxPieces = maxPieces;
		}
	}

	/**
	 * @param pluggedInOnly the pluggedInOnly to set
	 */
	public static synchronized void setPluggedInOnly(final boolean pluggedInOnly) {
		Settings.pluggedInOnly = pluggedInOnly;
	}

	/**
	 * @param pluggedInResume the pluggedInResume to set
	 */
	public static synchronized void setPluggedInResume(final boolean pluggedInResume) {
		Settings.pluggedInResume = pluggedInResume;
	}

	/**
	 * @param searchProvider the searchProvider to set
	 */
	public static synchronized void setSearchProvider(final int searchProvider) {
		Settings.searchProvider = searchProvider;
	}

	/**
	 * @param startHour the startHour to set
	 */
	public static void setStartHour(final int startHour) {
		Settings.startHour = startHour;
	}

	/**
	 * @param startMinute the startMinute to set
	 */
	public static void setStartMinute(final int startMinute) {
		Settings.startMinute = startMinute;
	}

	/**
	 * @param timeframeOnly the timeframeOnly to set
	 */
	public static void setTimeframeOnly(final boolean timeframeOnly) {
		Settings.timeframeOnly = timeframeOnly;
	}

	/**
	 * @param timeframeResume the timeframeResume to set
	 */
	public static void setTimeframeResume(final boolean timeframeResume) {
		Settings.timeframeResume = timeframeResume;
	}

	/**
	 * @param useDHT the useDHT to set
	 */
	public static synchronized void setUseDHT(final boolean useDHT) {
		Settings.useDHT = useDHT;
	}

	public static synchronized void setUseOngoing(final boolean ongoing) {
		Settings.useOngoing = ongoing;
	}

	/**
	 * @param wifiNetworks the wifiNetworks to set
	 */
	public static void setWifiNetworks(final boolean wifiNetworks) {
		Settings.wifiNetworks = wifiNetworks;
	}

	/**
	 * @param wifiNetworkSSIDs the wifiNetworkSSIDs to set
	 */
	public static void setWifiNetworkSSIDs(final List<String> wifiNetworkSSIDs) {
		Settings.wifiNetworkSSIDs = wifiNetworkSSIDs;
	}

	/**
	 * 
	 * @param delimitedList pipe delimited list of network SSIDs
	 */
	public static void setWifiNetworkSSIDs(final String delimitedList) {
		wifiNetworkSSIDs = new ArrayList<String>();
		final String[] ssids = delimitedList.split("\t");

		for (int i = 0; i < ssids.length; i++) {
			if (ssids[i] != null && ssids[i].length() > 0) {
				wifiNetworkSSIDs.add(ssids[i]);
			}
		}
	}

	/**
	 * @param wifiOnly the wifiOnly to set
	 */
	public static synchronized void setWifiOnly(final boolean wifiOnly) {
		Settings.wifiOnly = wifiOnly;
	}

	/**
	 * @param wifiResume the wifiResume to set
	 */
	public static synchronized void setWifiResume(final boolean wifiResume) {
		Settings.wifiResume = wifiResume;
	}

	public static synchronized boolean useOngoing() {
		return useOngoing;
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onActivityResult(int, int,
	 * android.content.Intent)
	 */
	@Override
	public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		if ((SAVE_PATH_REQUEST == requestCode) && (resultCode == RESULT_OK) && (data != null)) {
			final String path = data.getStringExtra("path");
			if ((path != null) && (path.length() > 0)) {
				if (downloadPathEdit != null) {
					downloadPathEdit.setText(path);
				}
				setDownloadPath(path);
				TorrentService.saveSettings();
			} else {
				Toast.makeText(this, "Unable to save path.  Try again", Toast.LENGTH_SHORT).show();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onStop()
	 */
	@Override
	public void onBackPressed() {
		//Only close if everything is correct
		if (setDownloadPath() && setPorts()) {
			finish();
		}
	}

	/**
	 * Sets the location to download files into
	 */
	private final boolean setDownloadPath() {
		boolean successful = true;
		//Save the new path
		String path = downloadPathEdit.getText().toString();
		if (!path.endsWith("/")) {
			path = path + "/";
			downloadPathEdit.setText(path);
		}

		if (path.length() == 0) {
			Toast.makeText(Settings.this, "You must enter a download location!", Toast.LENGTH_SHORT).show();
			successful = false;
		} else if (!path.equals(downloadPath)) {
			//Check to make sure the path is reachable and writable
			File newDir;
			newDir = new File(path);
			if (newDir.isDirectory()) {
				if (newDir.canWrite()) {
					downloadPath = path;
					TorrentService.saveSettings();
				} else {
					Toast.makeText(Settings.this, "Path '" + path + "' exists, but is not writable", Toast.LENGTH_LONG).show();
					downloadPathEdit.setText(downloadPath);
					successful = false;
				}
			} else {
				Toast.makeText(Settings.this, "Path '" + path + "' does not exist or is not a directory", Toast.LENGTH_LONG).show();
				downloadPathEdit.setText(downloadPath);
				successful = false;
			}
		}
		return successful;
	}

	/**
	 * Set the port range. Must be greater then 19 and less then 65536. High port must be greater then low port
	 * 
	 * @return true if ports were set successfully, false if not
	 */
	private boolean setPorts() {
		try {
			//Verify that highPort is higher then lowPort
			if (Integer.parseInt(lowPortEdit.getText().toString()) >= Integer.parseInt(highPortEdit.getText().toString())) {
				highPortEdit.setText(String.valueOf(Integer.parseInt(lowPortEdit.getText().toString()) + 1));
				Toast.makeText(Settings.this, "High port must be greater then low port", Toast.LENGTH_LONG).show();
			}
		} catch (final NumberFormatException nfe) {
			Toast.makeText(Settings.this, "Invalid number for a port, resetting to defaults", Toast.LENGTH_LONG).show();
			lowPortEdit.setText("6881");
			highPortEdit.setText("6889");
		}
		if (Integer.valueOf(lowPortEdit.getText().toString()) < 20) {
			Toast.makeText(Settings.this, "Port must be greater than 19", Toast.LENGTH_LONG).show();
			lowPortEdit.setText("20");
		}
		if (Integer.valueOf(highPortEdit.getText().toString()) < 21) {
			Toast.makeText(Settings.this, "Port must be greater than 20", Toast.LENGTH_LONG).show();
			highPortEdit.setText(String.valueOf(Integer.parseInt(lowPortEdit.getText().toString()) + 1));
		}
		if (Integer.valueOf(lowPortEdit.getText().toString()) > 32766) {
			Toast.makeText(Settings.this, "Port must be lower than 32767", Toast.LENGTH_LONG).show();
			lowPortEdit.setText("32766");
			highPortEdit.setText("32767");
		}
		if (Integer.valueOf(highPortEdit.getText().toString()) > 32767) {
			Toast.makeText(Settings.this, "Port must be lower than 32768", Toast.LENGTH_LONG).show();
			highPortEdit.setText("32767");
		}

		//Save the new values
		lowPort = Short.parseShort(lowPortEdit.getText().toString());
		highPort = Short.parseShort(highPortEdit.getText().toString());
		TorrentService.saveSettings();
		return true;
	}

	//	//Click listener for the Enable DHT checkbox
	//	protected final transient OnClickListener dhtClickListener = new View.OnClickListener() {
	//		public void onClick(final View view) {
	//			if (TorrentActivity.settings != null) {
	//				CheckBox dhtBox;
	//				dhtBox = (CheckBox) view;
	//				//TorrentService.useDHT = dhtBox.isChecked();
	//				TorrentActivity.saveSettings();
	//
	//				if (dhtBox.isChecked() && TorrentActivity.dhtTask.getStatus() != Status.RUNNING) {
	//					try {
	//						DHTHandler dht = new DHTHandler();
	//						TorrentActivity.dhtTask.execute(dht);
	//					} catch (IllegalStateException ise) {
	//						Log.d(TAG, "DHT Task already running");
	//					}
	//				} else {
	//					TorrentActivity.dhtTask.cancel(true);
	//				}
	//			}
	//		}
	//	};
}