/**
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.common;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.redlightning.jbittorrent.DownloadManager;
import net.redlightning.jbittorrent.Piece;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Validate previously downloaded content in the background while the main
 * thread starts downloading
 * 
 * @author Michael Isaacson
 * @version 11.9.29
 */
public class ValidationTask extends AsyncTask<DownloadManager, Integer, Integer> {
	private static final String TAG = ValidationTask.class.getSimpleName();
	private static ConcurrentLinkedQueue<DownloadManager> managerList = new ConcurrentLinkedQueue<DownloadManager>();

	/**
	 * @return the list of managers currently in the process of being allocated and verified
	 */
	public static ConcurrentLinkedQueue<DownloadManager> getManagerList() {
		return managerList;
	}

	/**
	 * 
	 * @param managerList
	 */
	public static void setManagerList(final ConcurrentLinkedQueue<DownloadManager> managerList) {
		ValidationTask.managerList = managerList;
	}

	/**
	 * Check to see if each piece has been downloaded successfully yet
	 */
	@Override
	protected Integer doInBackground(final DownloadManager... managers) {
		getManagerList().add(managers[0]);
		while (!getManagerList().isEmpty() && !isCancelled()) {
			final DownloadManager manager = getManagerList().poll();

			if (manager != null) {
				//Set up the files
				String saveas = ""; //torrent.savePath;
				if (manager.getTorrent().getLength().size() > 1) {
					saveas = manager.getTorrent().getTorrentFileName().substring(manager.getTorrent().getTorrentFileName().lastIndexOf("/"));
					saveas = saveas.replace(".torrent", "") + "/";
				}

				if (new File(Settings.getDownloadPath() + saveas).mkdirs()) {
					Log.i(TAG, "Made new folder " + saveas);
				}

				for (int i = 0; i < manager.getTorrent().getLength().size(); i++) {
					TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.ALLOCATING, manager.getTorrent().getTorrentFileName()));
					//Log.i(TAG, "Checking: " + torrent.fileNames.get(i));
					if ((!isCancelled() && (manager.getTorrent().getSelectedFiles() == null)) || manager.getTorrent().getSelectedFiles().contains(manager.getTorrent().getFileNames().get(i))) {
						final File temp = new File(Settings.getDownloadPath() + saveas + manager.getTorrent().getFileNames().get(i));
						if (!temp.exists() || (temp.length() != manager.getTorrent().getLength().get(i))) {
							RandomAccessFile rand = null;
							try {
								//Log.i(TAG, "Creating file for: " + manager.getTorrent().getFileNames().get(i));
								rand = new RandomAccessFile(Settings.getDownloadPath() + saveas + manager.getTorrent().getFileNames().get(i), "rw");
								manager.getOutputFiles().put(i, rand);
							} catch (final IOException ioe) {
								Log.e(TAG, "Could not create temp files");
								ioe.printStackTrace();
							}
						}
					}
					manager.getTorrent().setBlocked(i);
				}

				manager.getTorrent().setBlocked(-1);
				
				//Allocate
				manager.setAllocating(true);
				int file = 0;
				int fileoffset = 0;

				/*
				 * Construct all the pieces with the correct length and hash value
				 */
				for (int i = 0; i < manager.getTorrent().getPieceHashAsBinary().size(); i++) {
					manager.getTorrent().setAllocated(i);
					TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.ALLOCATING, manager.getTorrent().getTorrentFileName()));
					final Map<Integer, Integer> tm = new TreeMap<Integer, Integer>();
					int pieceoffset = 0;

					do {
						tm.put(file, fileoffset);

						if ((file < manager.getTorrent().getLength().size()) && (((fileoffset + manager.getTorrent().getPieceLength()) - pieceoffset) >= (manager.getTorrent().getLength().get(file))) && (i != (manager.getTorrent().getPieceHashAsBinary().size() - 1))) {
							pieceoffset += (manager.getTorrent().getLength().get(file)).intValue() - fileoffset;
							file++;
							fileoffset = 0;
							if (pieceoffset == manager.getTorrent().getPieceLength()) {
								break;
							}
						} else {
							fileoffset += manager.getTorrent().getPieceLength() - pieceoffset;
							break;
						}
					} while (!isCancelled() && !"Stopped".equals(manager.getTorrent().getStatus()));

					final Piece newPiece = new Piece(i, (i != (manager.getTorrent().getPieceHashAsBinary().size() - 1)) ? manager.getTorrent().getPieceLength() : ((Long) (manager.getTorrent().getTotalLength() % manager.getTorrent().getPieceLength())).intValue(), 16384, tm);

					//Add peice to the incomplete list for downloading only if is belongs to a selected file
					if ((file < manager.getTorrent().getFileNames().size()) && ((manager.getTorrent().getSelectedFiles() == null) || manager.getTorrent().getSelectedFiles().contains(manager.getTorrent().getFileNames().get(file)))) {
						//LOGGER.severe("Piece added for selected file " + this.torrent.fileNames.get(file));
						newPiece.setBlock(0, manager.getPieceFromFiles(newPiece));

						final boolean complete = newPiece.verify(manager.getTorrent().getPieceHashAsBinary().get(newPiece.getIndex()));
						if (complete) {
							manager.getTorrent().setLeft(manager.getTorrent().getLeft() - newPiece.getLength());
							manager.getTorrent().setComplete(i, true);
							//Log.i(TAG, "Piece " + i + " is complete");
						} else {
							manager.getTorrent().getIncompletePieces().put(i, i);
							//Log.i(TAG, "Piece " + i + " is incomplete");
						}
					} else if (file < manager.getTorrent().getFileNames().size()) {
						manager.getTorrent().setLeft(manager.getTorrent().getLeft() - newPiece.getLength());
						manager.getTorrent().setComplete(i, true);
					}
					manager.getOffsetList().add(tm);
					manager.updateProgress();
				}
			}
		}
		return null;
	}
}