/*
 * Java Bittorrent API as its name indicates is a JAVA API that implements the
 * Bittorrent Protocol This project contains two packages: 1. jBittorrentAPI is
 * the "client" part, i.e. it implements all classes needed to publish files,
 * share them and download them. This package also contains example classes on
 * how a developer could create new applications. 2. trackerBT is the "tracker"
 * part, i.e. it implements a all classes needed to run a Bittorrent tracker
 * that coordinates peers exchanges. * Copyright (C) 2007 Baptiste Dubuis,
 * Artificial Intelligence Laboratory, EPFL This file is part of
 * jbittorrentapi-v1.0.zip Java Bittorrent API is free software and a free user
 * study set-up; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version. Java
 * Bittorrent API is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with Java Bittorrent API; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * @version 1.0
 * @author Baptiste Dubuis To contact the author: email:
 * baptiste.dubuis@gmail.com More information about Java Bittorrent API:
 * http://sourceforge.net/projects/bitext/
 */

package net.redlightning.jbittorrent;

import java.util.*;

/**
 * Class representing a piece according to bittorrent definition. The piece is a
 * part of data of the target file(s)
 * 
 * @author Baptiste Dubuis (baptiste.dubuis@gmail.com)
 * @author Michael Isaacson (michael@redlightning.net)
 * @version 0.2
 */
public class Piece {
	private transient Map<Integer, Integer> filesAndOffset;
	private transient Map<Integer, byte[]> pieceBlock = new TreeMap<Integer, byte[]>(); //Map containing the piece data
	private transient int index; //Index of the piece within the file(s)
	private transient int length; //Length of the piece. It should be constant, except for the last piece of the file(s)

	/**
	 * Constructor of a Piece
	 * 
	 * @param index Index of the piece
	 * @param length Length of the piece
	 * @param blockSize Size of a block of data
	 * @param map HashTable containing the file(s) this piece belongs to and the
	 *            index in these
	 */
	public Piece(final int index, final int length, final int blockSize, final Map<Integer, Integer> map) {
		this.index = index;
		this.length = length;

		if (map == null) {
			this.filesAndOffset = new TreeMap<Integer, Integer>();
		} else {
			this.filesAndOffset = map;
		}
	}

	/**
	 * 
	 */
	public Piece() {
	// TODO Auto-generated constructor stub
	}

	/**
	 * @return
	 */
	public synchronized Map<Integer, Integer> getFileAndOffset() {
		return this.filesAndOffset;
	}

	/**
	 * Return the index of the piece
	 * 
	 * @return int
	 */
	public synchronized int getIndex() {
		return this.index;
	}

	/**
	 * Returns the length of the piece
	 * 
	 * @return int
	 */
	public synchronized int getLength() {
		return this.length;
	}

	/**
	 * Set a block of data at the corresponding offset
	 * 
	 * @param offset Offset of the data within the current piece
	 * @param data Data to be set at the given offset
	 */
	public synchronized void setBlock(final int offset, final byte[] data) {
		this.pieceBlock.put(offset, data);
	}

	/**
	 * Returns the concatenated value of the pieceBlock map. This represent the
	 * piece data
	 * 
	 * @return byte[]
	 */
	protected synchronized byte[] data() {
		byte[] data = new byte[0];
		for (final Iterator<Integer> it = this.pieceBlock.keySet().iterator(); it.hasNext();) {
			data = Utils.concat(data, this.pieceBlock.get(it.next()));
		}
		return data;
	}

	/**
	 * Verify if the downloaded data corresponds to the original data contained
	 * in the torrent by comparing it to the SHA1 hash in the torrent
	 * 
	 * @return boolean
	 */
	public synchronized boolean verify(final byte[] sha1) {
		return Utils.toString(Utils.hash(this.data())).matches(Utils.toString(sha1));
	}

	/**
	 * @return the pieceBlock
	 */
	public synchronized Map<Integer, byte[]> getPieceBlock() {
		return pieceBlock;
	}
}
