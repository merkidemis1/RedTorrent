/**
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.jbittorrent.udp;

import java.io.File;
import java.net.SocketException;
import net.redlightning.dht.DHTConfiguration;
import net.redlightning.dht.kad.DHT.DHTtype;
import net.redlightning.redtorrent.common.TorrentActivity;
import net.redlightning.redtorrent.common.TorrentService;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Manages the DHT initial connection process
 * 
 * @author Michael Isaacson
 * @version 11.3.31
 */
public class DHTTask extends AsyncTask<DHTHandler, Integer, Integer> {
	private static final String TAG = DHTTask.class.getSimpleName();
	private boolean running = true;
	private String status = "Idle";

	/*
	 * (non-Javadoc)
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 */
	@Override
	protected Integer doInBackground(DHTHandler... handlers) {
		DHTConfiguration config = new DHTConfiguration();
		config.setNodeCachePath(new File("/sdcard/redtorrent.dht"));
		config.setMultiHoming(false);

		try {
			status = "Initializing";
			handlers[0].getDhts().get(DHTtype.IPV4_DHT).start(config);
			handlers[0].getDhts().get(DHTtype.IPV4_DHT).bootstrap();
			
			while (!this.isCancelled() && running) {
				status = handlers[0].getDhts().get(DHTtype.IPV4_DHT).getStatus().name();
				handlers[0].getDhts().get(DHTtype.IPV4_DHT).update();
				TorrentActivity.getHandler().sendMessage(TorrentActivity.getHandler().obtainMessage(TorrentService.DHT, status));
				Thread.sleep(5000);
			}
		} catch (SocketException e) {
			Log.i(TAG, "DHT Socket Error");
			e.printStackTrace();
		} catch (InterruptedException e) {
			running = false;
			status = "Idle";
			Log.i(TAG, "DHT Task killed");
		}
		return 1;
	}

	public String getDHTStatus() {
		return status;
	}
}
