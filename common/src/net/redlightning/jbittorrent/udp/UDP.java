/**
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.jbittorrent.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import android.util.Log;

/**
 * Handles UDP communications
 * 
 * @author Michael Isaacson
 * @version 11.10.20
 */
public class UDP {
	private final String TAG = UDP.class.getSimpleName();
	private DatagramSocket socket;
	private byte[] responseData;

	/**
	 * Send a packet to the server, and get the response
	 * 
	 * @param request the request to send
	 * @param responseLength the expected length of the response
	 * @return the response from the server, if there is one in the alloted time
	 */
	public byte[] getData(final InetAddress host, final int port, final byte[] request, final int responseLength) {
		//Log.e(TAG, "Get " + responseLength + " bytes of data (" + Runtime.getRuntime().freeMemory() + " free)");
		try {
			socket = new DatagramSocket();
			responseData = new byte[responseLength];
			//request = authenticate(request);

			final DatagramPacket requestPacket = new DatagramPacket(request, request.length, host, port);
			final DatagramPacket responsePacket = new DatagramPacket(responseData, responseData.length);
			socket.setSoTimeout(30000); // Set a receive timeout
			socket.send(requestPacket);
			socket.receive(responsePacket); // Wait for a response from the server
		} catch (final IOException e) {
			Log.i(TAG, "Get Data IOE: " + e.getMessage());
			responseData = null;
		} catch (final Exception e) {
			Log.i(TAG, "Get Data: " + e.getMessage());
			responseData = null;
		} finally {
			if (socket != null) {
				socket.close();
			}
		}
		return responseData;
	}

	//	/**
	//	 * @param buffer
	//	 * @return the 16 byte array for the username and password hash
	//	 */
	//	public byte[] authenticate(final byte[] buffer) {
	//		//	0 8-byte zero-padded string username  
	//		//	8 8-byte password string hash  
	//		if ((username != null) && (password != null)) {
	//			return Utils.concat(buffer, Utils.concat(Utils.padArray(username.getBytes(), 8), Utils.padArray(Utils.hash(password.getBytes()), 8)));
	//		} else {
	//			return buffer;
	//		}
	//	}
}