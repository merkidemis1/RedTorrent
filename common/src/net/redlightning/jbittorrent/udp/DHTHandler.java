/**
 * 
 */
package net.redlightning.jbittorrent.udp;

import java.io.File;
import java.net.SocketException;
import java.util.Map;
import net.redlightning.dht.DHTConfiguration;
import net.redlightning.dht.MainlineDHTProvider;
import net.redlightning.dht.kad.DHT;
import net.redlightning.dht.kad.DHT.DHTtype;
import net.redlightning.dht.vuze.Tracker;

/**
 * @author Michael Isaacson
 * @version 11.3.30
 */
public class DHTHandler {
	//private static final String TAG = DHTHandler.class.getSimpleName();
	private boolean connected = false;
	public static Map<DHTtype, DHT> dhts = null;
	public MainlineDHTProvider mlDHTProvider;
	private int port;
	private Tracker tracker;

	public DHTHandler() {
		setDhts(DHT.createDHTs());

		try {
			mlDHTProvider = new MainlineDHTProvider() {
				/*
				 * (non-Javadoc)
				 * @see lbms.plugins.mldht.MainlineDHTProvider#getDHTPort()
				 */
				public int getDHTPort() {
					return port;
				}

				/*
				 * (non-Javadoc)
				 * @see
				 * lbms.plugins.mldht.MainlineDHTProvider#notifyOfIncomingPort
				 * (java.lang.String, int)
				 */
				public void notifyOfIncomingPort(String ip_addr, int port) {
					for (DHT dht : getDhts().values()) {
						dht.addDHTNode(ip_addr, port);
					}
				}
			};
			//pluginInterface.getMainlineDHTManager().setProvider(mlDHTProvider);
		} catch (Throwable e) {

		}
		tracker = new Tracker();
	}

	public void startDHT() {
		DHTConfiguration config = new DHTConfiguration();
		config.setNodeCachePath(new File("/sdcard/redtorrent.dht"));
		config.setMultiHoming(false);
		
		//view_model.getStatus().setText("Initializing");
		try {
			for (Map.Entry<DHTtype, DHT> e : dhts.entrySet()) {
				e.getValue().start(config);
				e.getValue().bootstrap();
			}

			tracker.start();
			//view_model.getStatus().setText("Running");
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}
	
	public void stopDHT() {
		tracker.stop();
		for (DHT dht : dhts.values()) {
			dht.stop();
		}
	}
	
	/**
	 * @return the connected
	 */
	public synchronized boolean isConnected() {
		return connected;
	}

	/**
	 * @param connected the connected to set
	 */
	public synchronized void setConnected(boolean connected) {
		this.connected = connected;
	}

	/**
	 * @param dhts the dhts to set
	 */
	public void setDhts(Map<DHTtype, DHT> dhts) {
		DHTHandler.dhts = dhts;
	}

	/**
	 * @return the dhts
	 */
	public Map<DHTtype, DHT> getDhts() {
		return dhts;
	}

	/**
	 * @return the tracker
	 */
	public synchronized Tracker getTracker() {
		return tracker;
	}

	/**
	 * @param tracker the tracker to set
	 */
	public synchronized void setTracker(Tracker tracker) {
		this.tracker = tracker;
	}
}
