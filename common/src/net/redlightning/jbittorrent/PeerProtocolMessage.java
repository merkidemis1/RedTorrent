/*
 * Java Bittorrent API as its name indicates is a JAVA API that implements the
 * Bittorrent Protocol This project contains two packages: 1. jBittorrentAPI is
 * the "client" part, i.e. it implements all classes needed to publish files,
 * share them and download them. This package also contains example classes on
 * how a developer could create new applications. 2. trackerBT is the "tracker"
 * part, i.e. it implements a all classes needed to run a Bittorrent tracker
 * that coordinates peers exchanges. * Copyright (C) 2007 Baptiste Dubuis,
 * Artificial Intelligence Laboratory, EPFL This file is part of
 * jbittorrentapi-v1.0.zip Java Bittorrent API is free software and a free user
 * study set-up; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version. Java
 * Bittorrent API is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with Java Bittorrent API; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * @version 1.0
 * @author Baptiste Dubuis To contact the author: email:
 * baptiste.dubuis@gmail.com More information about Java Bittorrent API:
 * http://sourceforge.net/projects/bitext/
 */

package net.redlightning.jbittorrent;

import android.util.Log;
import net.redlightning.jbittorrent.interfaces.AbstractMessage;

/**
 * Represent a Peer Protocol message according to Bittorrent protocol
 * specifications. This message format depends on its identity, so refer to
 * Bittorrent specifications for further information
 * 
 * 0 - choke - no payload
 * 1 - unchoke - no payload
 * 2 - interested - no payload
 * 3 - not interested - no payload
 * 4 - have
 * 5 - bitfield
 * 6 - request
 * 7 - piece
 * 8 - cancel
 * 
 * @author Baptiste Dubuis (baptiste.dubuis@gmail.com)
 * @author Michael Isaacson (michael@redlightning.net) - Android conversion
 * @version 11.3.3
 */
public class PeerProtocolMessage extends AbstractMessage {
	private static final String TAG = PeerProtocolMessage.class.getCanonicalName();
	private byte[] length = new byte[4];
	private byte[] id = new byte[1];
	private byte[] payload;

	/**
	 * Constructor
	 */
	public PeerProtocolMessage() {
		super();
	}

	/**
	 * Constructor
	 * 
	 * @param type
	 * @param priority
	 */
	protected PeerProtocolMessage(int type, int priority) {
		super(type, priority);
		this.setData(type);
	}

	/**
	 * Constructor
	 * 
	 * @param type
	 */
	protected PeerProtocolMessage(int type) {
		this(type, 0);
	}

	/**
	 * Constructor
	 * 
	 * @param type
	 * @param payload
	 * @param priority
	 */
	protected PeerProtocolMessage(int type, byte[] payload, int priority) {
		super(type, priority);
		this.setData(type, payload);
	}

	/**
	 * Constructor
	 * 
	 * @param type
	 * @param payload
	 */
	protected PeerProtocolMessage(int type, byte[] payload) {
		this(type, payload, 0);
	}

	/**
	 * @return the message length
	 */
	public byte[] getLength() {
		return this.length;
	}

	/**
	 * @return the message id
	 */
	public byte[] getID() {
		return this.id;
	}

	/**
	 * @return the message payload
	 */
	public byte[] getPayload() {
		return this.payload;
	}

	/**
	 * @param length the length of the message
	 */
	public void setLength(byte[] length) {
		this.length = length;
	}

	/**
	 * @param id the message id
	 */
	public void setID(int id) {
		this.id[0] = (byte) id;
	}

	/**
	 * @param payload the message payload
	 */
	public void setPayload(byte[] payload) {
		this.payload = payload;
	}

	/**
	 * @param type the message type, 0-4
	 */
	public void setData(int type) {
		this.type = type;
		switch (type) {
			case 0:
				this.length = new byte[] { 0, 0, 0, 0 };
				break;
			case 1:
				this.length = new byte[] { 0, 0, 0, 1 };
				this.id[0] = 0;
				break;
			case 2:
				this.length = new byte[] { 0, 0, 0, 1 };
				this.id[0] = 1;
				break;
			case 3:
				this.length = new byte[] { 0, 0, 0, 1 };
				this.id[0] = 2;
				break;
			case 4:
				this.length = new byte[] { 0, 0, 0, 1 };
				this.id[0] = 3;
				break;
			default:
				Log.d(TAG, "Unknown message type: " + type);
				break;
		}
	}

	/**
	 * @param type the message type, 0-4
	 * @param payload the contents of the message
	 */
	protected void setData(int type, byte[] payload) {
		this.type = type;
		switch (type) {
			case 5:
				this.length = new byte[] { 0, 0, 0, 5 };
				this.id[0] = 4;
				this.payload = payload;
				break;
			case 6:
				this.length = Utils.toArray(1 + payload.length);
				this.id[0] = 5;
				this.payload = payload;
				break;
			case 7:
				this.length = new byte[] { 0, 0, 0, 13 };
				this.id[0] = 6;
				this.payload = payload;
				break;
			case 8:
				this.length = Utils.toArray(1 + payload.length);
				this.id[0] = 7;
				this.payload = payload;
				break;
			case 9:
				this.length = new byte[] { 0, 0, 0, 13 };
				this.id[0] = 8;
				this.payload = payload;
				break;
			case 10:
				this.length = new byte[] { 0, 0, 0, 3 };
				this.id[0] = 9;
				this.payload = payload;
				break;
			default:
				Log.d(TAG, "Unknown message type: " + type);
				break;
		}
	}

	/**
	 * Build a peer protocol message based off current values
	 */
	public byte[] generate() {
		byte[] returnVal;
		if (this.type > 4) {
			returnVal = Utils.concat(Utils.concat(this.length, this.id), this.payload);
		} else if (this.type > 0) {
			returnVal = Utils.concat(this.length, this.id);
		} else {
			returnVal = this.length;
		}
		return returnVal;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuffer buffer;
		int length;

		length = Utils.toInt(this.length);

		buffer = new StringBuffer();
		buffer.append("<length=" + length + ">");
		if (length > 0) {
			buffer.append("<id=" + (int) this.id[0] + ">");
			if (length > 1) {
				switch (this.id[0] + 1) {
					case Peer.HAVE:
						buffer.append("<index=" + Utils.toInt(this.payload) + ">");
						break;
					case Peer.BITFIELD:
						buffer.append("<bitfield=" + Utils.toBitArray(this.payload) + ">");
						break;
					case Peer.REQUEST:
						buffer.append("<index=" + Utils.toInt(Utils.subArray(this.payload, 0, 4)) + ">");
						buffer.append("<begin=" + Utils.toInt(Utils.subArray(this.payload, 4, 4)) + ">");
						buffer.append("<length=" + Utils.toInt(Utils.subArray(this.payload, 8, 4)) + ">");
						break;
					case Peer.PIECE:
						buffer.append("<index=" + Utils.toInt(Utils.subArray(this.payload, 0, 4)) + ">");
						buffer.append("<begin=" + Utils.toInt(Utils.subArray(this.payload, 4, 4)) + ">");
						buffer.append("<block= " + (this.payload.length - 8) + "bytes>");
						break;
					case Peer.CANCEL:
						buffer.append("<index=" + Utils.toInt(Utils.subArray(this.payload, 0, 4)) + ">");
						buffer.append("<begin=" + Utils.toInt(Utils.subArray(this.payload, 4, 4)) + ">");
						buffer.append("<length=" + Utils.toInt(Utils.subArray(this.payload, 8, 4)) + ">");
						break;
					case Peer.PORT:
						break;
					case Peer.EXTENDED:
						Log.d(TAG, "Exteneded message");
					default:
						break;
				}
			}
		}
		return buffer.toString();
	}
}
