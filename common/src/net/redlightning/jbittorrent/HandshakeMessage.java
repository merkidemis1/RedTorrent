/*
 * Java Bittorrent API as its name indicates is a JAVA API that implements the
 * Bittorrent Protocol This project contains two packages: 1. jBittorrentAPI is
 * the "client" part, i.e. it implements all classes needed to publish files,
 * share them and download them. This package also contains example classes on
 * how a developer could create new applications. 2. trackerBT is the "tracker"
 * part, i.e. it implements a all classes needed to run a Bittorrent tracker
 * that coordinates peers exchanges. * Copyright (C) 2007 Baptiste Dubuis,
 * Artificial Intelligence Laboratory, EPFL This file is part of
 * jbittorrentapi-v1.0.zip Java Bittorrent API is free software and a free user
 * study set-up; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version. Java
 * Bittorrent API is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with Java Bittorrent API; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * @version 1.0
 * @author Baptiste Dubuis To contact the author: email:
 * baptiste.dubuis@gmail.com More information about Java Bittorrent API:
 * http://sourceforge.net/projects/bitext/
 */

package net.redlightning.jbittorrent;

import java.util.Arrays;
import net.redlightning.jbittorrent.interfaces.AbstractMessage;

/**
 * Represent a Handshake message according to Bittorrent Protocol. It has the
 * form: <pstrlen><pstr><reserved><info_hash><peer_id> - # pstrlen: string
 * length of <pstr>, as a single raw byte - - # pstr: string identifier of the
 * protocol - - # reserved: eight (8) reserved bytes - - # info_hash: 20-byte
 * SHA1 hash of the info key in the metainfo file - - # peer_id: 20-byte string
 * used as a unique ID for the client -
 * 
 * @author Baptiste Dubuis (baptiste.dubuis@gmail.com)
 * @author Michael Isaacson (michael@redlightning.net) - updates
 * @version 11.4.6
 */
public class HandshakeMessage extends AbstractMessage {
	private byte[] length;
	private byte[] protocol;
	private byte[] reserved;
	private byte[] fileID;
	private byte[] peerID;
	
	/**
	 * Creates an empty HS message
	 */
	public HandshakeMessage() {
		super(-1, 0);
		length = new byte[1];
		protocol = new byte[19];
		reserved = new byte[8];
		fileID = new byte[20];
		peerID = new byte[20];
	}

	/**
	 * Creates a HS message with the given infoHash and peerID and default
	 * values
	 * 
	 * @param infoHash byte[]
	 * @param peerID byte[]
	 */
	protected HandshakeMessage(final byte[] infoHash, final byte[] peerID) {
		this(new byte[] { 19 }, "BitTorrent protocol".getBytes(), new byte[] { 1, 0, 0, 0, 0, 0, 0, 0 }, infoHash, peerID);
		//Set reserved bits to indicate support for extensions
		//reserved[0] |= 0x80; 	//01 - Azureus Extended Messaging
		reserved[5] |= 0x10;	//44 - Extension Protocol
		//reserved[5] |= 0x02;	//47 - Extension negotiation protocol. Prefer LibTorrent Extended protocol
		//reserved[5] |= 0x01;	//48 - Extension negotiation protocol. Prefer Azureus Message Protocol
		//reserved[7] |= 0x08; 	//61 - Mainline NAT Traversal
		//reserved[7] |= 0x04;	//62 - Fast Extension
		//reserved[7] |= 0x02; 	//63 - XBT Peer Exchange
		//reserved[7] |= 0x01;	//64 - MainLine DHT
	}

	/**
	 * Create a HS message with all given parameters
	 * 
	 * @param length byte[]
	 * @param protocol byte[]
	 * @param reserved byte[]
	 * @param fileID byte[]
	 * @param peerID byte[]
	 */
	protected HandshakeMessage(final byte[] length, final byte[] protocol, final byte[] reserved, final byte[] fileID, final byte[] peerID) {
		super(-1, 0);
		this.length = length;
		this.protocol = protocol;
		this.reserved = reserved;
		this.fileID = fileID;
		this.peerID = peerID;
	}

	/**
	 * Generate the byte array representing the whole message that can then be
	 * transmitted
	 * 
	 * @return byte[]
	 */
	public byte[] generate() {
		return Utils.concat(this.length, Utils.concat(this.protocol, Utils.concat(this.reserved, Utils.concat(this.fileID, this.peerID))));
	}

	/**
	 * Return the infoHash as a byte array
	 * 
	 * @return byte[]
	 */
	public byte[] getFileID() {
		byte[] returnVal;
		returnVal = new byte[fileID.length];
		System.arraycopy(this.fileID, 0, returnVal, 0, this.fileID.length);
		return returnVal;
	}

	/**
	 * @return the length
	 */
	public byte[] getLength() {
		byte[] returnVal;
		returnVal = new byte[length.length];
		System.arraycopy(this.length, 0, returnVal, 0, this.length.length);
		return returnVal;
	}

	/**
	 * Return the peerID as a byte array
	 * 
	 * @return byte[]
	 */
	public byte[] getPeerID() {
		byte[] returnVal;
		returnVal = new byte[peerID.length];
		System.arraycopy(this.peerID, 0, returnVal, 0, this.peerID.length);
		return returnVal;
	}

	/**
	 * @return the protocol
	 */
	public byte[] getProtocol() {
		byte[] returnVal;
		returnVal = new byte[protocol.length];
		System.arraycopy(this.protocol, 0, returnVal, 0, this.protocol.length);
		return returnVal;
	}

	/**
	 * @return the reserved
	 */
	public byte[] getReserved() {
		byte[] returnVal;
		returnVal = new byte[reserved.length];
		System.arraycopy(this.reserved, 0, returnVal, 0, this.reserved.length);
		return returnVal;
	}

	/**
	 * @param fileID the fileID to set
	 */
	public void setFileID(final byte[] fileID) {
		System.arraycopy(fileID, 0, this.fileID, 0, fileID.length);
	}

	/**
	 * @param length the length to set
	 */
	public void setLength(final byte[] length) {
		System.arraycopy(length, 0, this.length, 0, length.length);
	}

	/**
	 * @param peerID the peerID to set
	 */
	public void setPeerID(final byte[] peerID) {
		System.arraycopy(peerID, 0, this.peerID, 0, peerID.length);
	}

	/**
	 * @param protocol the protocol to set
	 */
	public void setProtocol(final byte[] protocol) {
		System.arraycopy(protocol, 0, this.protocol, 0, protocol.length);
	}

	/**
	 * @param reserved the reserved to set
	 */
	public void setReserved(final byte[] reserved) {
		System.arraycopy(reserved, 0, this.reserved, 0, reserved.length);
	}

	/**
	 * Display the message in a readable format
	 * 
	 * @return String
	 */
	public String toString() {
		StringBuffer buffer;
		buffer = new StringBuffer();
		buffer.append(this.length[0]);
		buffer.append('+');
		buffer.append(Arrays.toString(this.protocol));
		buffer.append('+');
		buffer.append(Utils.toString(this.reserved));
		buffer.append('+');
		buffer.append(Utils.toString(this.fileID));
		buffer.append('+');
		buffer.append(Utils.toString(this.peerID));

		return buffer.toString();
	}
}
