/*
 * Java Bittorrent API as its name indicates is a JAVA API that implements the
 * Bittorrent Protocol This project contains two packages: 1. jBittorrentAPI is
 * the "client" part, i.e. it implements all classes needed to publish files,
 * share them and download them. This package also contains example classes on
 * how a developer could create new applications. 2. trackerBT is the "tracker"
 * part, i.e. it implements a all classes needed to run a Bittorrent tracker
 * that coordinates peers exchanges. * Copyright (C) 2007 Baptiste Dubuis,
 * Artificial Intelligence Laboratory, EPFL This file is part of
 * jbittorrentapi-v1.0.zip Java Bittorrent API is free software and a free user
 * study set-up; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version. Java
 * Bittorrent API is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with Java Bittorrent API; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * @version 1.0
 * @author Baptiste Dubuis To contact the author: email:
 * baptiste.dubuis@gmail.com More information about Java Bittorrent API:
 * http://sourceforge.net/projects/bitext/
 */

package net.redlightning.jbittorrent;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import net.redlightning.redtorrent.common.Settings;
import android.util.Log;

/**
 * Thread that can listen for remote peers connection attempts to this client
 * 
 * @author Baptiste Dubuis (baptiste.dubuis@gmail.com)
 * @author Michael Isaacson (michael@redlightning.net) - Android updates
 * @version 11.2.7
 */
public class ConnectionListener extends Thread {
	private static final String TAG = ConnectionListener.class.getSimpleName();
	private ServerSocket socket = null;
	private int connectedPort = -1;
	private final boolean acceptConnection = true;
	private final DownloadManager manager;

	public ConnectionListener(final DownloadManager manager) {
		super("Connection Listener");
		this.manager = manager;
	}

	/**
	 * @return the port this client is listening on
	 */
	public int getConnectedPort() {
		return connectedPort;
	}

	/**
	 * Try to create a server socket for remote peers to connect on within the
	 * specified port range
	 * 
	 * @param minPort The minimal port number this client should listen on
	 * @param maxPort The maximal port number this client should listen on
	 * @return boolean
	 */
	protected boolean connect(final int minPort, final int maxPort) {
		boolean returnVal = false;

		for (int i = minPort; i <= maxPort; i++) {
			try {
				socket = new ServerSocket(i);
				socket.setReuseAddress(true);
				connectedPort = i;
				returnVal = true;
			} catch (final IOException ioe) {
				Log.e(TAG, "Cannot connect: " + ioe.getMessage());
			}
		}
		return returnVal;
	}

	/**
	 * Fire a notification when a connection is accepted
	 */
	@Override
	public void run() {
		byte[] bytes;
		bytes = new byte[0];
		try {
			while (true) {
				synchronized (bytes) {
					if (acceptConnection && manager.getTasks().size() < Settings.getMaxConnections()) {
						fireConnectionAccepted(socket.accept());
						sleep(1000);
					} else {
						Log.d(TAG, "No more connections accepted for the moment...");
						bytes.wait();
					}
				}
			}
		} catch (final IOException ioe) {
			Log.d(TAG, "Error in connection listener: " + ioe.getMessage());
		} catch (final InterruptedException ie) {
			Log.d(TAG, "Error in connection listener: " + ie.getMessage());
		}
	}

	/**
	 * Method used to send message to all object currently listening on this
	 * thread when a new connection has been accepted. It provides the socket
	 * the connection is bound to.
	 * 
	 * @param socket the Socket that is currently connected
	 */
	private void fireConnectionAccepted(final Socket socket) {
		manager.connectionAccepted(socket);
	}
}
