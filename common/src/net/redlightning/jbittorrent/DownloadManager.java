/*
 * Java Bittorrent API as its name indicates is a JAVA API that implements the
 * Bittorrent Protocol This project contains two packages: 1. jBittorrentAPI is
 * the "client" part, i.e. it implements all classes needed to publish files,
 * share them and download them. This package also contains example classes on
 * how a developer could create new applications. 2. trackerBT is the "tracker"
 * part, i.e. it implements a all classes needed to run a Bittorrent tracker
 * that coordinates peers exchanges. * Copyright (C) 2007 Baptiste Dubuis,
 * Artificial Intelligence Laboratory, EPFL This file is part of
 * jbittorrentapi-v1.0.zip Java Bittorrent API is free software and a free user
 * study set-up; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version. Java
 * Bittorrent API is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with Java Bittorrent API; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * @version 1.0
 * @author Baptiste Dubuis To contact the author: email:
 * baptiste.dubuis@gmail.com More information about Java Bittorrent API:
 * http://sourceforge.net/projects/bitext/
 */

package net.redlightning.jbittorrent;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.Socket;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import net.redlightning.redtorrent.common.Settings;
import net.redlightning.redtorrent.common.TorrentService;
import android.util.Log;

/**
 * Object that manages all concurrent downloads. It chooses which piece to
 * request to which peer.
 * 
 * @author Baptiste Dubuis (baptiste.dubuis@gmail.com) - Original
 * @author Michael Isaacson (michael@redlightning.net) - Android port and other enhancements
 * @version 11.11.15
 */
public class DownloadManager {
	private static final String TAG = DownloadManager.class.getSimpleName();
	private transient Map<Integer, RandomAccessFile> outputFiles;
	private transient PeerUpdater updater = null;
	private transient ConnectionListener connListener = null;
	private transient final List<Peer> unchokeList = new LinkedList<Peer>();
	private Map<String, Peer> peerList = new LinkedHashMap<String, Peer>();
	private transient final Map<String, DownloadTask> tasks = new TreeMap<String, DownloadTask>();
	private transient final Map<String, BitSet> peerAvailabilies = new LinkedHashMap<String, BitSet>();
	private transient final Map<String, Peer> unchoken = new LinkedHashMap<String, Peer>();
	private transient final List<Map<Integer, Integer>> offsetList = new ArrayList<Map<Integer, Integer>>();
	private transient long lastUnchoking = 0;
	private transient int optimisticUnchoking = 3;
	private Thread updateThread;
	private TorrentFile torrent = null;
	public boolean allocating = false;
	private boolean active = true;

	/**
	 * @param torrent
	 * @param clientID
	 * @param handle
	 */
	public DownloadManager(final TorrentFile file) {
		torrent = file;
		try {
			torrent.setComplete(new BitSet(torrent.getPieceHashAsBinary().size()));
			torrent.setIsRequested(new BitSet(torrent.getPieceHashAsBinary().size()));
			setOutputFiles(new HashMap<Integer, RandomAccessFile>(torrent.getLength().size()));
			torrent.setLeft(torrent.getTotalLength());
			lastUnchoking = System.currentTimeMillis();
		} catch (NullPointerException e) {
			TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.ERROR, torrent.getTorrentFileName()
					+ ",Unable to start torrent, possibly a currupt file"));
		}
	}

	/**
	 * Add the download task to the list of active (i.e. Handshake is ok) tasks
	 * 
	 * @param id String
	 * @param dt DownloadTask
	 */
	public void addActiveTask(final String id, final DownloadTask dt) {
		synchronized (tasks) {
			tasks.put(id, dt);
		}
	}

	/**
	 * Check the existence of the files specified in the torrent and if
	 * necessary, create them
	 * 
	 * @return int
	 * @todo Should return an integer representing some error message...
	 */
	public int checkTempFiles() {
		String saveas = ""; //torrent.savePath;
		if (torrent.getLength().size() > 1) {
			saveas = torrent.getTorrentFileName().substring(torrent.getTorrentFileName().lastIndexOf("/"));
			saveas = saveas.replace(".torrent", "") + "/";
		}

		if (new File(Settings.getDownloadPath() + saveas).mkdirs()) {
			Log.i(TAG, "Made new folder " + saveas);
		}
		for (int i = 0; i < torrent.getLength().size(); i++) {
			//Log.i(TAG, "Checking: " + torrent.fileNames.get(i));
			if (torrent.getSelectedFiles() == null || torrent.getSelectedFiles().contains(torrent.getFileNames().get(i))) {
				File temp = new File(Settings.getDownloadPath() + saveas + torrent.getFileNames().get(i));
				if (!temp.exists()) {
					try {
						temp.createNewFile();
					} catch (IOException e) {
						Log.e(TAG, "IOE while creating new file: " + e.getMessage());
					}
				}
				try {
					RandomAccessFile rand = new RandomAccessFile(temp, "rw");
					if (torrent.getLength().get(i) > 0) {
						rand.setLength(torrent.getLength().get(i));
					}
					this.getOutputFiles().put(i, new RandomAccessFile(temp, "rw"));
				} catch (IOException ioe) {
					Log.e(TAG, "Could not create temp files");
					ioe.printStackTrace();
				}
			}
		}

		return 0;
	}

	/**
	 * Returns the index of the piece that could be downloaded by the peer in
	 * parameter
	 * 
	 * @param id The id of the peer that wants to download
	 * @return int The index of the piece to request
	 */
	private int choosePiece2Download(final String id) {
		int index = 0;
		ArrayList<Integer> possible = new ArrayList<Integer>(torrent.getPieceHashAsBinary().size());
		for (int i = 0; i < torrent.getPieceHashAsBinary().size(); i++) {
			if ((!this.isPieceRequested(i) || (torrent.getComplete().cardinality() > torrent.getPieceHashAsBinary().size() - 3)) && (!this.isPieceComplete(i))
					&& this.peerAvailabilies.get(id) != null && this.peerAvailabilies.get(id).get(i) && torrent.getInProgressPieces().containsKey(i)) {
				possible.add(i);
			}
		}

		//logger.fine(this.isRequested.cardinality()+" "+this.isComplete.cardinality()+" " + possible.size());
		if (!possible.isEmpty()) {
			Random r = new Random(System.currentTimeMillis());
			index = possible.get(r.nextInt(possible.size()));
			this.setRequested(index, true);
			return index;
		}
		return -1;
	}

	/**
	 * Close all open files
	 */
	public void closeTempFiles() {
		for (int i = 0; i < this.getOutputFiles().size(); i++)
			try {
				if (this.getOutputFiles().get(i) != null) {
					this.getOutputFiles().get(i).close();
				}
			} catch (Exception e) {
				Log.e(TAG, "Fialed to close temp file " + i + " because: " + e.getMessage());
			}
	}

	/**
	 * Connect to a peer if we don't have our max connections yet
	 * 
	 * @param p the peer to connect to
	 */
	private void connect(final Peer p) {
		if (p != null) {
			final DownloadTask dt = new DownloadTask(this, p, true);
			dt.start();
		}
	}

	/**
	 * Called when a new peer connects to the client. Check if it is already
	 * registered in the peer list, and if not, create a new DownloadTask for it
	 * 
	 * @param socket Socket
	 */
	public void connectionAccepted(final Socket socket) {
		synchronized (tasks) {
			final String id = socket.getInetAddress().getHostAddress() + ":" + socket.getPort();
			if (!getTasks().containsKey(id)) {
				final DownloadTask dt = new DownloadTask(this, null, false, socket);
				getPeerList().put(dt.getPeer().toString(), dt.getPeer());
				dt.start();
			}
		}
	}

	/**
	 * @return the map of active download tasks
	 */
	public Map<String, DownloadTask> getTasks() {
		synchronized (tasks) {
			return tasks;
		}
	}

	/**
	 * Compute the bitfield byte array from the isComplete BitSet
	 * 
	 * @return byte[]
	 */
	public synchronized byte[] getBitField() {
		final int l = (int) Math.ceil((double) torrent.getPieceHashAsBinary().size() / 8.0);
		byte[] bitfield = new byte[l];
		for (int i = 0; i < torrent.getPieceHashAsBinary().size(); i++)
			if (torrent.getComplete().get(i)) {
				bitfield[i / 8] |= 1 << (7 - i % 8);
			}
		return bitfield;
	}

	/**
	 * @return get the percent complete for this torrent
	 */
	public synchronized float getCompleted() {
		try {
			return (float) (((float) (100.0)) * ((float) (torrent.getComplete().cardinality())) / ((float) (torrent.getPieceHashAsBinary().size())));
		} catch (Exception e) {
			return 0.00f;
		}
	}

	/**
	 * @param connectionCount the connectionCount to set
	 */
	public synchronized int getConnectionCount() {
		return getTasks().size();
	}

	/**
	 * @return the download rate for all peers in roughly Kbps
	 */
	public synchronized float getDLRate() {
		try {
			float rate = 0.00f;
			List<Peer> l = new LinkedList<Peer>(this.getPeerList().values());
			Iterator<Peer> it = l.iterator();
			while (it.hasNext()) {
				Peer p = (Peer) it.next();
				if (p.getDLRate(false) > 0) {
					rate = rate + p.getDLRate(true);
				}
			}
			return rate;
		} catch (Exception e) {
			return 0.00f;
		}
	}

	/**
	 * @return the offsetList
	 */
	public synchronized List<Map<Integer, Integer>> getOffsetList() {
		return offsetList;
	}

	public Map<Integer, RandomAccessFile> getOutputFiles() {
		return outputFiles;
	}

	/**
	 * @return the peerList
	 */
	public synchronized Map<String, Peer> getPeerList() {
		if (peerList == null) {
			peerList = new LinkedHashMap<String, Peer>();
		}
		return peerList;
	}

	/**
	 * Get a piece block from the existing file(s)
	 * 
	 * @param piece int
	 * @param begin int
	 * @param length int
	 * @return byte[]
	 */
	private synchronized byte[] getPieceBlock(final Piece piece, final int begin, final int length) {
		return Utils.subArray(this.getPieceFromFiles(piece), begin, length);
	}

	/**
	 * Load piece data from the existing files
	 * 
	 * @param piece int
	 * @return byte[]
	 */
	public synchronized byte[] getPieceFromFiles(final Piece piece) {
		byte[] data = new byte[0];

		if (piece.getLength() > 0) {
			data = new byte[piece.getLength()];
			int remainingData = data.length;
			for (Iterator<Integer> it = piece.getFileAndOffset().keySet().iterator(); it.hasNext();) {
				try {
					Integer file = it.next();
					int remaining = ((Integer) torrent.getLength().get(file.intValue())).intValue() - ((Integer) (piece.getFileAndOffset().get(file))).intValue();
					if (this.getOutputFiles().get(file.intValue()) != null) {
						int seekTo = ((Integer) (piece.getFileAndOffset().get(file))).intValue();
						this.getOutputFiles().get(file.intValue()).seek(seekTo);
						this.getOutputFiles().get(file.intValue()).read(data, data.length - remainingData, (remaining < remainingData) ? remaining : remainingData);
					}
					remainingData -= remaining;
				} catch (Exception ioe) {
					if ("Stopped".equals(torrent.getStatus())) {
						stopTrackerUpdate();
					} else {
						torrent.setStatus("Stopped");
						stopTrackerUpdate();
						Log.e(TAG, "getPieceFromFiles: " + ioe.getMessage());
						ioe.printStackTrace();
					}
					break;
				}
			}
		}
		return data;
	}

	/**
	 * @return the torrent
	 */
	public TorrentFile getTorrent() {
		return torrent;
	}

	/**
	 * @return
	 */
	public synchronized float getULRate() {
		try {
			float rate = 0.00f;
			List<Peer> l = new LinkedList<Peer>(this.getPeerList().values());
			Iterator<Peer> it = l.iterator();

			while (it.hasNext()) {
				Peer p = (Peer) it.next();
				if (p.getULRate(false) > 0) {
					rate = rate + p.getULRate(true);
				}
			}
			return rate / (1024 * 10);
		} catch (Exception e) {
			return 0.00f;
		}
	}

	/**
	 * @return the updater
	 */
	public synchronized PeerUpdater getUpdater() {
		return updater;
	}

	/**
	 * Check if the current download is complete
	 * 
	 * @return boolean
	 */
	public synchronized boolean isComplete() {
		return (torrent.getComplete().cardinality() == torrent.getPieceHashAsBinary().size());
	}

	/**
	 * Check if the piece with the given index is complete and verified
	 * 
	 * @param piece The piece index
	 * @return boolean
	 */
	private synchronized boolean isPieceComplete(final int piece) {
		return torrent.getComplete().get(piece);
	}

	/**
	 * Check if the piece with the given index is complete and verified
	 * 
	 * @param piece The piece index
	 * @return boolean
	 */
	private synchronized boolean isPieceComplete(final Piece piece) {
		try {
			return torrent.getComplete().get(piece.getIndex());
		} catch (NullPointerException e) {
			return false;
		}
	}

	/**
	 * Check if the piece with the given index is requested by a peer
	 * 
	 * @param piece The piece index
	 * @return boolean true if the piece is in the completed list and ready to send
	 */
	private synchronized boolean isPieceRequested(final int piece) {
		return torrent.getComplete().get(piece);
	}

	/**
	 * Attempt to gracefully unchoke a peer
	 */
	private synchronized void optimisticUnchoke() {
		if (!unchokeList.isEmpty()) {
			Peer peer = null;
			do {
				peer = (Peer) unchokeList.remove(0);
				synchronized (tasks) {
					if (tasks.get(peer.toString()) != null && tasks.get(peer.toString()).sender != null) {
						tasks.get(peer.toString()).sender.addMessageToQueue(new PeerProtocolMessage(Peer.UNCHOKE));
						peer.setChoked(false);
						unchoken.put(peer.toString(), peer);
						//LOGGER.fine(p + " optimistically unchoken...");
					}
				}
			} while ((peer == null) && (!unchokeList.isEmpty()));
		}
	}

	/**
	 * Update the piece availabilities for a given peer
	 * 
	 * @param peerID peer id in the format of IP address:port (i.e. 192.168.0.1:1234)
	 * @param has the bitSet showing what this peer has
	 */
	public synchronized void peerAvailability(final String peerID, final BitSet has) {
		this.peerAvailabilies.put(peerID, has);
		final BitSet interest = (BitSet) (has.clone());
		interest.andNot(torrent.getComplete());
		synchronized (tasks) {
			if (tasks.get(peerID) != null && tasks.get(peerID).sender != null) {
				if (interest.cardinality() > 0 && !tasks.get(peerID).peer.isInteresting()) {
					try {
						tasks.get(peerID).sender.addMessageToQueue(new PeerProtocolMessage(Peer.INTERESTED, 2));
						tasks.get(peerID).peer.setInteresting(true);
					} catch (NullPointerException npe) {
						Log.e(TAG, "peerAvailability NPE");
					}
				}
			}
		}
	}

	/**
	 * Received when a task is ready to download or upload. In such a case, if
	 * there is a piece that can be downloaded from the corresponding peer, then
	 * request the piece
	 * 
	 * @param peerID peer id in the format of IP address:port (i.e. 192.168.0.1:1234)
	 */
	public synchronized void peerReady(final String peerID) {
		if (System.currentTimeMillis() - this.lastUnchoking > 10000)
			this.unchokePeers();

		int piece2request = this.choosePiece2Download(peerID);
		if (piece2request != -1) {
			Piece piece = torrent.getInProgressPieces().get(piece2request);
			if (piece == null) {
				piece = new Piece(piece2request, (piece2request != torrent.getPieceHashAsBinary().size() - 1) ? torrent.getPieceLength() : ((Long) (torrent.getTotalLength() % torrent.getPieceLength())).intValue(), 16384, offsetList.get(piece2request));
				torrent.getInProgressPieces().put(piece2request, piece);
			}
			try {
				this.tasks.get(peerID).requestPiece(piece);
			} catch (NullPointerException npe) {
				torrent.getInProgressPieces().remove(piece2request);
			}
		}
	}

	/**
	 * Received when a peer request a piece. If the piece is available (which
	 * should always be the case according to Bittorrent protocol) and we are
	 * able and willing to upload, the send the piece to the peer
	 * 
	 * @param peerID String
	 * @param piece int
	 * @param begin int
	 * @param length int
	 */
	public synchronized void peerRequest(final String peerID, final Piece piece, final int begin, final int length) {
		if (piece != null) {
			Log.d(TAG, "Peer: " + peerID + " requests " + piece.getIndex());

			if (this.isPieceComplete(piece)) {
				if (tasks.get(peerID) != null) {
					tasks.get(peerID).sender.addMessageToQueue(new PeerProtocolMessage(Peer.PIECE, Utils.concat(Utils.toArray(piece.getIndex()), Utils.concat(Utils.toArray(begin), this.getPieceBlock(piece, begin, length)))));
					tasks.get(peerID).peer.setULRate(length);
				}
				updater.updateParameters(0, length, "");
			} else {
				try {
					//getTasks().get(peerID).end();
					getTasks().remove(peerID).end();
				} catch (final NullPointerException e) {
					Log.e(TAG, "Exception while requesting from peer: " + e.getMessage());
				}
				getPeerList().remove(peerID);
				unchoken.remove(peerID);
			}
		}
	}

	/**
	 * Received when a piece has been fully downloaded by a task. The piece
	 * might have been corrupted, in which case the manager will request it
	 * again later. If it has been successfully downloaded and verified, the
	 * piece status is set to 'complete', a 'HAVE' message is sent to all
	 * connected peers and the piece is saved into the corresponding file(s)
	 * 
	 * @param peerID String
	 * @param piece int
	 * @param complete boolean
	 */
	public synchronized void pieceCompleted(final String peerID, final Piece piece, final boolean complete) {
		//Log.d(TAG, "Peer " + peerID + " completed piece " + piece.getIndex());
		torrent.getIsRequested().clear(piece.getIndex());
		if (complete && !isPieceComplete(piece)) {
			updater.updateParameters(torrent.getPieceLength(), 0, "");
			torrent.getComplete().set(piece.getIndex(), complete);
			//float totaldl = (float) (((float) (100.0)) * ((float) (torrent.getComplete().cardinality())) / ((float) (torrent.getPieceHashAsBinary().size())));

			for (Iterator<String> it = this.tasks.keySet().iterator(); it.hasNext();)
				try {
					this.tasks.get(it.next()).sender.addMessageToQueue(new PeerProtocolMessage(Peer.HAVE, Utils.toArray(piece.getIndex()), 1));
				} catch (NullPointerException npe) {}
			this.savePiece(piece);
			//this.getPieceBlock(piece, 0, 15000);	//TODO can this be safely removed?

			torrent.completePiece(piece.getIndex());
			TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.UPDATE, torrent.getTorrentFileName()));

			updateProgress();
		} 

		if (torrent.getComplete().cardinality() == torrent.getPieceHashAsBinary().size()) {
			//Log.i(TAG, "Task completed");
			this.notify();
		}
	}

	/**
	 * Set the status of the piece to requested or not
	 * 
	 * @param i int
	 * @param requested boolean
	 */
	public synchronized void pieceRequested(final int i, final boolean requested) {
		torrent.getIsRequested().set(i, requested);
	}

	/**
	 * @param piece Save a piece in the corresponding file(s)
	 */
	private synchronized void savePiece(final Piece piece) {
		//int remaining = torrent.pieceList.get(piece).getLength();
		byte[] data = piece.data();
		int remainingData = data.length;
		for (Iterator<Integer> it = piece.getFileAndOffset().keySet().iterator(); it.hasNext();) {
			try {
				Integer file = (Integer) (it.next());
				int remaining = ((Integer) torrent.getLength().get(file.intValue())).intValue() - ((Integer) (piece.getFileAndOffset().get(file))).intValue();
				if (this.getOutputFiles().get(file.intValue()) != null) {
					this.getOutputFiles().get(file.intValue()).seek(((Integer) (piece.getFileAndOffset().get(file))).intValue());
					this.getOutputFiles().get(file.intValue()).write(data, data.length - remainingData, (remaining < remainingData) ? remaining : remainingData);
				}

				remainingData -= remaining;
			} catch (IOException ioe) {
				Log.e(TAG, "savePeice: " + ioe.getMessage());
			}
		}
		torrent.getInProgressPieces().remove(piece.getIndex());
	}

	/**
	 * @param outputFiles the map of random access files used to store downloaded content
	 */
	public synchronized void setOutputFiles(final Map<Integer, RandomAccessFile> outputFiles) {
		this.outputFiles = outputFiles;
	}

	/**
	 * Mark a piece as requested or not according to the parameters
	 * 
	 * @param piece The index of the piece to be updated
	 * @param is True if the piece is now requested, false otherwise
	 */
	private synchronized void setRequested(final int piece, final boolean is) {
		torrent.getIsRequested().set(piece, is);
	}

	/**
	 * @param torrent the torrent to set
	 */
	public synchronized void setTorrent(final TorrentFile torrent) {
		this.torrent = torrent;
	}

	/**
	 * Create the ConnectionListener to accept incoming connection from peers
	 * 
	 * @param minPort The minimal port number this client should listen on
	 * @param maxPort The maximal port number this client should listen on
	 * @return True if the listening process is started, false else
	 * @todo Should it really be here? Better create it in the implementation
	 */
	public synchronized boolean startListening(final int minPort, final int maxPort) {
		connListener = new ConnectionListener(this);
		if (connListener.connect(minPort, maxPort)) {
			return true;
		} else {
			Log.e(TAG, "Could not create listening socket!");
			return false;
		}
	}

	/**
	 * Create and start the peer updater to retrieve new peers sharing the file
	 */
	public synchronized void startTrackerUpdate() {
		this.updater = new PeerUpdater(this);
		if (connListener == null) {
			startListening(Settings.getLowPort(), Settings.getHighPort());
		}
		this.updater.setListeningPort(this.connListener.getConnectedPort());
		this.updateThread = new Thread(new DownloadThreadGroup(), updater);
		this.updateThread.start();
	}

	/**
	 * Stop accepting incoming connections
	 */
	public synchronized void stopListening() {
		this.connListener = null;
	}

	/**
	 * Stop the tracker updates
	 */
	public synchronized void stopTrackerUpdate() {
		TorrentService.getHandler().dispatchMessage(TorrentService.getHandler().obtainMessage(TorrentService.STOP, torrent.getTorrentFileName()));
		setActive(false);

		if (updater != null) {
			updater.setConnected(false);
		}
	}

	/**
	 * 
	 * @param peerID
	 * @param reason
	 */
	public synchronized void taskCompleted(String peerID, int reason) {
		taskCompleted(peerID, null, reason);
	}

	/**
	 * Removes a task and peer after the task sends a completion message.
	 * Completion can be caused by an error (bad request, ...) or simply by the
	 * end of the connection
	 * 
	 * @param id Task identity
	 * @param reason Reason of the completion
	 */
	public synchronized void taskCompleted(final String id, final String message, final int reason) {
		switch (reason) {
			case DownloadTask.REFUSED:
				//Log.e(TAG, "Connection refused by host " + id + " - " + message);
				this.peerAvailabilies.remove(id);
				this.getPeerList().remove(id);
				break;
			case DownloadTask.MALFORMED_MESSAGE:
				//Log.e(TAG, "Malformed message from " + id + ". Task ended...");
				this.peerAvailabilies.remove(id);
				this.getPeerList().remove(id);
				break;
			case DownloadTask.UNKNOWN_HOST:
				//Log.e(TAG, "Connection could not be established to " + id + ". Host unknown...");
				this.peerAvailabilies.remove(id);
				this.getPeerList().remove(id);
			default:
				break;
		}
		try {
			getTasks().remove(id).end();
		} catch (final NullPointerException e) {

		}
	}

	/**
	 * @param piece
	 * @return
	 */
	public synchronized boolean testComplete(final Piece piece) {
		boolean complete = false;
		piece.setBlock(0, this.getPieceFromFiles(piece));

		//TODO this is typically only for Magnet links where we can't get the piece hashes.  Do we get them from PEX? 
		if (torrent.getPieceHashAsBinary().size() == 0) {
			complete = true;
		} else {
			complete = piece.verify((byte[]) torrent.getPieceHashAsBinary().get(piece.getIndex()));
		}
		return complete;
	}

	/**
	 * Choose which of the connected peers should be unchoked and authorized to
	 * upload from this client. A peer gets unchoked if it is not interested, or
	 * if it is interested and has one of the 5 highest download rate among the
	 * interested peers. \r\n Every 3 times this method is called, calls the
	 * optimisticUnchoke method, which unchoke a peer no matter its download
	 * rate, in a try to find a better source
	 */
	public synchronized void unchokePeers() {
		int nbDownloaders = 0;
		this.unchoken.clear();

		final List<Peer> peersAsList = new LinkedList<Peer>(peerList.values());

		for (Iterator<Peer> it = peersAsList.iterator(); it.hasNext();) {
			final Peer peer = (Peer) it.next();
			if (nbDownloaders < 5 && getTasks().get(peer.toString()) != null) {
				if (!peer.isInterested()) {
					unchoken.put(peer.toString(), peer);
					if (peer.isChoked() && getTasks().get(peer.toString()) != null && getTasks().get(peer.toString()).sender != null) {
						getTasks().get(peer.toString()).sender.addMessageToQueue(new PeerProtocolMessage(Peer.UNCHOKE));
					}
					peer.setChoked(false);

					while (unchokeList.remove(peer)) {
						;
					}
				} else if (peer.isChoked()) {
					unchoken.put(peer.toString(), peer);
					getTasks().get(peer.toString()).sender.addMessageToQueue(new PeerProtocolMessage(Peer.UNCHOKE));
					peer.setChoked(false);
					while (unchokeList.remove(peer))
						;
					nbDownloaders++;
				}

			} else {
				if (!peer.isChoked()) {
					try {
						getTasks().get(peer.toString()).sender.addMessageToQueue(new PeerProtocolMessage(Peer.CHOKE));
						peer.setChoked(true);
					} catch (NullPointerException e) {
						//Log.e(TAG, "Failed to unchoke peer, NPE.");
					}
				}
				if (!unchokeList.contains(peer)) {
					unchokeList.add(peer);
				}
			}
		}

		lastUnchoking = System.currentTimeMillis();
		if (optimisticUnchoking-- == 0) {
			optimisticUnchoke();
			optimisticUnchoking = 3;
		}

		for (int p = 0; p < peersAsList.size(); p++) {
			Peer peer = peersAsList.get(p);
			if (!getTasks().containsKey(peer.toString())) {
				connect(peer);
			}
		}
	}

	/**
	 * Update the progress of each piece
	 */
	public synchronized void updateProgress() {
		synchronized (getTorrent().getIncompletePieces()) {
			final Iterator<Integer> itr = getTorrent().getIncompletePieces().keySet().iterator();
			while (itr.hasNext()) {
				final Integer index = itr.next();
				try {
					if (index < getOffsetList().size()) {
						final Piece piece = new Piece(index, (index != torrent.getPieceHashAsBinary().size() - 1) ? torrent.getPieceLength() : ((Long) (torrent.getTotalLength() % torrent.getPieceLength())).intValue(), 16384, getOffsetList().get(index));
						getTorrent().getInProgressPieces().put(index, piece);
					} else {
						//TODO put in overflow queue to be reprocessed when allocation it done?
					}
				} catch (IndexOutOfBoundsException e) {
					Log.e(TAG, "Unable to add piece to In Progress list, " + index + " is out of bounds");
				}
			}
			unchokePeers();
		}
	}

	/**
	 * @return true if this torrent is active, false if not
	 */
	public synchronized boolean isActive() {
		return active;
	}

	/**
	 * @param active true if this torrent is active, false if not
	 */
	public synchronized void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * @param b true if this torrent is currently allocating, false if not
	 */
	public synchronized void setAllocating(boolean b) {
		allocating = b;
	}
}
