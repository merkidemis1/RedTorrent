/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.jbittorrent;

import junit.framework.TestCase;

/**
 * @author Michael Isaacson
 *
 */
public class TorrentProcessorTest extends TestCase {

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#TorrentProcessor(net.redlightning.jbittorrent.TorrentFile)}.
	 */
	
	public void testTorrentProcessorTorrentFile() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#TorrentProcessor()}.
	 */
	
	public void testTorrentProcessor() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#parseTorrent(java.lang.String)}.
	 */
	
	public void testParseTorrentString() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#parseTorrent(java.io.File)}.
	 */
	
	public void testParseTorrentFile() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#getTorrentFile(java.util.Map)}.
	 */
	
	public void testGetTorrentFile() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#setTorrent(net.redlightning.jbittorrent.TorrentFile)}.
	 */
	
	public void testSetTorrent() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#setTorrentData(java.lang.String, int, java.lang.String, java.lang.String, java.lang.String)}.
	 */
	
	public void testSetTorrentDataStringIntStringStringString() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#setTorrentData(java.lang.String, int, java.lang.String, java.lang.String, java.lang.String, java.util.List)}.
	 */
	
	public void testSetTorrentDataStringIntStringStringStringListOfString() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#setAnnounceURL(java.lang.String)}.
	 */
	
	public void testSetAnnounceURL() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#setPieceLength(int)}.
	 */
	
	public void testSetPieceLength() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#setName(java.lang.String)}.
	 */
	
	public void testSetName() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#setComment(java.lang.String)}.
	 */
	
	public void testSetComment() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#setCreator(java.lang.String)}.
	 */
	
	public void testSetCreator() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#setCreationDate(long)}.
	 */
	
	public void testSetCreationDate() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#setEncoding(java.lang.String)}.
	 */
	
	public void testSetEncoding() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#addFiles(java.util.List)}.
	 */
	
	public void testAddFilesList() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#addFile(java.io.File)}.
	 */
	
	public void testAddFileFile() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#addFile(java.lang.String)}.
	 */
	
	public void testAddFileString() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#addFiles(java.lang.Object[])}.
	 */
	
	public void testAddFilesObjectArray() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#generatePieceHashes(net.redlightning.jbittorrent.TorrentFile)}.
	 */
	
	public void testGeneratePieceHashesTorrentFile() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#generatePieceHashes()}.
	 */
	
	public void testGeneratePieceHashes() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#generateTorrent(net.redlightning.jbittorrent.TorrentFile)}.
	 */
	
	public void testGenerateTorrentTorrentFile() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#generateTorrent()}.
	 */
	
	public void testGenerateTorrent() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.TorrentProcessor#getTorrent()}.
	 */
	
	public void testGetTorrent() {
		fail("Not yet implemented"); // TODO
	}

}
