/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.jbittorrent;

import junit.framework.TestCase;

/**
 * @author Michael Isaacson
 *
 */
public class HandshakeMessageTest extends TestCase {

	/**
	 * Test method for {@link net.redlightning.jbittorrent.HandshakeMessage#generate()}.
	 */
	
	public void testGenerate() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.HandshakeMessage#HandshakeMessage()}.
	 */
	
	public void testHandshakeMessage() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.HandshakeMessage#HandshakeMessage(byte[], byte[])}.
	 */
	
	public void testHandshakeMessageByteArrayByteArray() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.HandshakeMessage#HandshakeMessage(byte[], byte[], byte[], byte[], byte[])}.
	 */
	
	public void testHandshakeMessageByteArrayByteArrayByteArrayByteArrayByteArray() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.HandshakeMessage#getFileID()}.
	 */
	
	public void testGetFileID() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.HandshakeMessage#getLength()}.
	 */
	
	public void testGetLength() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.HandshakeMessage#getPeerID()}.
	 */
	
	public void testGetPeerID() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.HandshakeMessage#getProtocol()}.
	 */
	
	public void testGetProtocol() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.HandshakeMessage#getReserved()}.
	 */
	
	public void testGetReserved() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.HandshakeMessage#setFileID(byte[])}.
	 */
	
	public void testSetFileID() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.HandshakeMessage#setLength(byte[])}.
	 */
	
	public void testSetLength() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.HandshakeMessage#setPeerID(byte[])}.
	 */
	
	public void testSetPeerID() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.HandshakeMessage#setProtocol(byte[])}.
	 */
	
	public void testSetProtocol() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.HandshakeMessage#setReserved(byte[])}.
	 */
	
	public void testSetReserved() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.HandshakeMessage#toString()}.
	 */
	
	public void testToString() {
		fail("Not yet implemented"); // TODO
	}

}
