/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.jbittorrent;

import junit.framework.TestCase;


/**
 * @author Michael Isaacson
 *
 */
public class UtilsTest extends TestCase {

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#byteToHex(byte)}.
	 */
	
	public void testByteToHex() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#bytesToHex(byte[])}.
	 */
	
	public void testBytesToHex() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#toHexChar(int)}.
	 */
	
	public void testToHexChar() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#toURLString(byte[])}.
	 */
	
	public void testByteArrayToURLString() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#toString(byte[])}.
	 */
	
	public void testByteArrayToByteString() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#toArray(int)}.
	 */
	
	public void testIntToByteArray() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#toArray(long)}.
	 */
	
	public void testLongToByteArray() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#toLong(byte[])}.
	 */
	
	public void testByteArrayToLong() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#toInt(byte[])}.
	 */
	
	public void testByteArrayToInt() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#toUnsignedInt(byte)}.
	 */
	
	public void testByteToUnsignedInt() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#intToByteArray4(int)}.
	 */
	
	public void testIntToByteArray4() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#hash(java.nio.ByteBuffer)}.
	 */
	
	public void testHashByteBuffer() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#hash(byte[])}.
	 */
	
	public void testHashByteArray() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#generateID()}.
	 */
	
	public void testGenerateID() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#concat2(byte[], byte[])}.
	 */
	
	public void testConcat2() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#concat(byte[], byte[])}.
	 */
	
	public void testConcatByteArrayByteArray() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#concat(byte[], byte)}.
	 */
	
	public void testConcatByteArrayByte() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#unsignedIntToLong(byte[])}.
	 */
	
	public void testUnsignedIntToLong() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#toBitArray(byte[])}.
	 */
	
	public void testByteArray2BitArray() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#subArray(byte[], int, int)}.
	 */
	
	public void testSubArray() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#compareBytes(byte[], byte[])}.
	 */
	
	public void testBytesCompare() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#copy(byte[], byte[])}.
	 */
	
	public void testCopy() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Utils#toByteArray(java.util.BitSet)}.
	 */
	
	public void testToByteArray() {
		fail("Not yet implemented"); // TODO
	}

}
