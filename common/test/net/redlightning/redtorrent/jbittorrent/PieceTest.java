/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.jbittorrent;

import junit.framework.TestCase;

/**
 * @author Michael Isaacson
 * 
 */
public class PieceTest extends TestCase {

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Piece#Piece(int, int, int)}.
	 */

	public void testPieceIntIntInt() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Piece#Piece(int, int, int, java.util.Map)}.
	 */

	public void testPieceIntIntIntMapOfIntegerInteger() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Piece#Piece()}.
	 */

	public void testPiece() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Piece#clearData()}.
	 */

	public void testClearData() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Piece#setFileAndOffset(int, int)}.
	 */

	public void testSetFileAndOffset() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Piece#getFileAndOffset()}.
	 */

	public void testGetFileAndOffset() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Piece#getIndex()}.
	 */

	public void testGetIndex() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Piece#getLength()}.
	 */

	public void testGetLength() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Piece#setBlock(int, byte[])}.
	 */

	public void testSetBlock() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Piece#data()}.
	 */

	public void testData() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Piece#verify(byte[])}.
	 */

	public void testVerify() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Piece#toString()}.
	 */

	public void testToString() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Piece#getPieceBlock()}.
	 */

	public void testGetPieceBlock() {
		fail("Not yet implemented"); // TODO
	}

}
