/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.jbittorrent;

import junit.framework.TestCase;


/**
 * @author Michael Isaacson
 *
 */
public class DownloadManagerTest extends TestCase {

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#DownloadManager(net.redlightning.jbittorrent.TorrentFile, byte[], android.os.Handler, java.util.List)}.
	 */
	
	public void testDownloadManagerTorrentFileByteArrayHandlerListOfString() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#DownloadManager(net.redlightning.jbittorrent.TorrentFile, byte[], android.os.Handler)}.
	 */
	
	public void testDownloadManagerTorrentFileByteArrayHandler() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#updateProgress()}.
	 */
	
	public void testUpdateProgress() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#testComplete(net.redlightning.jbittorrent.Piece)}.
	 */
	
	public void testTestComplete() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#startTrackerUpdate()}.
	 */
	
	public void testStartTrackerUpdate() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#stopTrackerUpdate()}.
	 */
	
	public void testStopTrackerUpdate() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#startListening(int, int)}.
	 */
	
	public void testStartListening() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#stopListening()}.
	 */
	
	public void testStopListening() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#closeTempFiles()}.
	 */
	
	public void testCloseTempFiles() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#checkTempFiles()}.
	 */
	
	public void testCheckTempFiles() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#savePiece(net.redlightning.jbittorrent.Piece)}.
	 */
	
	public void testSavePiece() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#isComplete()}.
	 */
	
	public void testIsComplete() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#cardinalityR()}.
	 */
	
	public void testCardinalityR() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#getPiece(int)}.
	 */
	
	public void testGetPiece() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#isPieceComplete(net.redlightning.jbittorrent.Piece)}.
	 */
	
	public void testIsPieceCompletePiece() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#isPieceComplete(int)}.
	 */
	
	public void testIsPieceCompleteInt() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#isPieceRequested(int)}.
	 */
	
	public void testIsPieceRequested() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#setComplete(int, boolean)}.
	 */
	
	public void testSetComplete() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#setRequested(int, boolean)}.
	 */
	
	public void testSetRequested() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#requestedBits()}.
	 */
	
	public void testRequestedBits() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#taskCompleted(java.lang.String, int)}.
	 */
	
	public void testTaskCompleted() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#pieceCompleted(java.lang.String, net.redlightning.jbittorrent.Piece, boolean)}.
	 */
	
	public void testPieceCompleted() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#pieceRequested(int, boolean)}.
	 */
	
	public void testPieceRequested() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#unchokePeers()}.
	 */
	
	public void testUnchokePeers() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#peerReady(java.lang.String)}.
	 */
	
	public void testPeerReady() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#peerRequest(java.lang.String, net.redlightning.jbittorrent.Piece, int, int)}.
	 */
	
	public void testPeerRequest() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#getPieceFromFiles(net.redlightning.jbittorrent.Piece)}.
	 */
	
	public void testGetPieceFromFiles() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#getPieceBlock(net.redlightning.jbittorrent.Piece, int, int)}.
	 */
	
	public void testGetPieceBlock() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#peerAvailability(java.lang.String, java.util.BitSet)}.
	 */
	
	public void testPeerAvailability() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#connect(net.redlightning.jbittorrent.Peer)}.
	 */
	
	public void testConnect() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#getConnectionCount()}.
	 */
	
	public void testGetConnectionCount() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#setConnectionCount(int)}.
	 */
	
	public void testSetConnectionCount() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#disconnect(net.redlightning.jbittorrent.Peer)}.
	 */
	
	public void testDisconnect() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#updatePeerList(java.util.Map)}.
	 */
	
	public void testUpdatePeerList() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#updateFailed(int, java.lang.String)}.
	 */
	
	public void testUpdateFailed() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#addActiveTask(java.lang.String, net.redlightning.jbittorrent.DownloadTask)}.
	 */
	
	public void testAddActiveTask() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#connectionAccepted(java.net.Socket)}.
	 */
	
	public void testConnectionAccepted() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#getBitField()}.
	 */
	
	public void testGetBitField() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#getCompleted()}.
	 */
	
	public void testGetCompleted() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#getDLRate()}.
	 */
	
	public void testGetDLRate() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#getULRate()}.
	 */
	
	public void testGetULRate() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#getTorrent()}.
	 */
	
	public void testGetTorrent() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#setTorrent(net.redlightning.jbittorrent.TorrentFile)}.
	 */
	
	public void testSetTorrent() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#setMaxPieces(int)}.
	 */
	
	public void testSetMaxPieces() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#getMaxPieces()}.
	 */
	
	public void testGetMaxPieces() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#getUpdater()}.
	 */
	
	public void testGetUpdater() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadManager#getPeerList()}.
	 */
	
	public void testGetPeerList() {
		fail("Not yet implemented"); // TODO
	}

}
