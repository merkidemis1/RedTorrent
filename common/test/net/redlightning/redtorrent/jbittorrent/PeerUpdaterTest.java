/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.jbittorrent;

import junit.framework.TestCase;


/**
 * @author Michael Isaacson
 *
 */
public class PeerUpdaterTest extends TestCase {

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#run()}.
	 */
	
	public void testRun() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#PeerUpdater(byte[], net.redlightning.jbittorrent.TorrentFile, long)}.
	 */
	
	public void testPeerUpdater() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#setListeningPort(int)}.
	 */
	
	public void testSetListeningPort() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#getInterval()}.
	 */
	
	public void testGetInterval() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#getMinInterval()}.
	 */
	
	public void testGetMinInterval() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#getDownloaded()}.
	 */
	
	public void testGetDownloaded() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#getUploaded()}.
	 */
	
	public void testGetUploaded() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#getLeft()}.
	 */
	
	public void testGetLeft() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#getEvent()}.
	 */
	
	public void testGetEvent() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#setInterval(int)}.
	 */
	
	public void testSetInterval() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#setMinInterval(int)}.
	 */
	
	public void testSetMinInterval() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#setDownloaded(long)}.
	 */
	
	public void testSetDownloaded() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#setUploaded(long)}.
	 */
	
	public void testSetUploaded() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#setLeft(long)}.
	 */
	
	public void testSetLeft() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#setEvent(java.lang.String)}.
	 */
	
	public void testSetEvent() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#getList()}.
	 */
	
	public void testGetList() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#getPeerList()}.
	 */
	
	public void testGetPeerList() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#updateParameters(int, int, java.lang.String)}.
	 */
	
	public void testUpdateParameters() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#processResponse(java.util.Map)}.
	 */
	
	public void testProcessResponse() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#contactTracker(byte[], long, long, long, java.lang.String)}.
	 */
	
	public void testContactTracker() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#end()}.
	 */
	
	public void testEnd() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#addPeerUpdateListener(net.redlightning.jbittorrent.interfaces.PeerUpdateListener)}.
	 */
	
	public void testAddPeerUpdateListener() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#removePeerUpdateListener(net.redlightning.jbittorrent.interfaces.PeerUpdateListener)}.
	 */
	
	public void testRemovePeerUpdateListener() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#getPeerUpdateListeners()}.
	 */
	
	public void testGetPeerUpdateListeners() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#fireUpdatePeerList(java.util.LinkedHashMap)}.
	 */
	
	public void testFireUpdatePeerList() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#fireUpdateFailed(int, java.lang.String)}.
	 */
	
	public void testFireUpdateFailed() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerUpdater#getFailCount()}.
	 */
	
	public void testGetFailCount() {
		fail("Not yet implemented"); // TODO
	}

}
