/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.jbittorrent;

import junit.framework.TestCase;


/**
 * @author Michael Isaacson
 *
 */
public class DownloadTaskTest extends TestCase {

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadTask#finalize()}.
	 */
	
	public void testFinalize() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadTask#run()}.
	 */
	
	public void testRun() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadTask#DownloadTask(net.redlightning.jbittorrent.TorrentFile, net.redlightning.jbittorrent.Peer, byte[], boolean, byte[], java.net.Socket)}.
	 */
	
	public void testDownloadTaskTorrentFilePeerByteArrayBooleanByteArraySocket() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadTask#DownloadTask(net.redlightning.jbittorrent.TorrentFile, net.redlightning.jbittorrent.Peer, byte[], boolean, byte[])}.
	 */
	
	public void testDownloadTaskTorrentFilePeerByteArrayBooleanByteArray() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadTask#initConnection()}.
	 */
	
	public void testInitConnection() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadTask#getPeer()}.
	 */
	
	public void testGetPeer() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadTask#requestPiece(net.redlightning.jbittorrent.Piece)}.
	 */
	
	public void testRequestPiece() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadTask#checkDownloaded()}.
	 */
	
	public void testCheckDownloaded() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadTask#connectionClosed()}.
	 */
	
	public void testConnectionClosed() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadTask#keepAliveSent()}.
	 */
	
	public void testKeepAliveSent() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadTask#messageReceived(net.redlightning.jbittorrent.AbstractMessage)}.
	 */
	
	public void testMessageReceived() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadTask#addDTListener(net.redlightning.jbittorrent.DTListener)}.
	 */
	
	public void testAddDTListener() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadTask#removeDTListener(net.redlightning.jbittorrent.DTListener)}.
	 */
	
	public void testRemoveDTListener() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.DownloadTask#end()}.
	 */
	
	public void testEnd() {
		fail("Not yet implemented"); // TODO
	}

}
