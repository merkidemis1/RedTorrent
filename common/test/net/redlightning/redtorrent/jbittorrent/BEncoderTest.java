/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.jbittorrent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.redlightning.jbittorrent.BEncoder;
import junit.framework.TestCase;

/**
 * Test methods for {@link net.redlightning.jbittorrent.BEncoder}
 * 
 * @author Michael Isaacson
 * @version 12.6.28
 */
public class BEncoderTest extends TestCase {
	/**
	 * Test method for {@link net.redlightning.jbittorrent.BEncoder#encode(java.util.Map)}.
	 */
	public void testEncodeMap() {
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("Hello", "World");
		map.put("Integer", new Integer(1000));
		map.put("Long", new Long(1234567890));
		try {
			assertEquals("d5:Hello5:World7:Integeri1000e4:Longi1234567890ee", new String(BEncoder.encode(map)));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.BEncoder#listsAreIdentical(java.util.List, java.util.List)}.
	 */
	public void testListsAreIdentical() {
		final List<Integer> intList1 = new ArrayList<Integer>();
		final List<Integer> intList2 = new ArrayList<Integer>();
		final List<Long> longList1 = new ArrayList<Long>();
		final List<Long> longList2 = new ArrayList<Long>();
		final List<Float> floatList1 = new ArrayList<Float>();
		final List<Float> floatList2 = new ArrayList<Float>();
		final List<String> strList1 = new ArrayList<String>();
		final List<String> strList2 = new ArrayList<String>();
		
		intList1.add(new Integer(5));
		intList1.add(new Integer(1000));
		intList1.add(new Integer(500));
		intList1.add(new Integer(10));
		intList2.add(new Integer(5));
		intList2.add(new Integer(1000));
		intList2.add(new Integer(500));
		intList2.add(new Integer(10));
		assertTrue(BEncoder.listsAreIdentical(intList1, intList2));
		
		longList1.add(new Long(5));
		longList1.add(new Long(1000));
		longList1.add(new Long(500));
		longList1.add(new Long(1430987435450l));
		longList2.add(new Long(5));
		longList2.add(new Long(1000));
		longList2.add(new Long(500));
		longList2.add(new Long(1430987435450l));
		assertTrue(BEncoder.listsAreIdentical(longList1, longList2));
		
		floatList1.add(new Float(5.320948709324));
		floatList1.add(new Float(1000.8));
		floatList1.add(new Float(500.0));
		floatList1.add(new Float(10.2094));
		floatList2.add(new Float(5.320948709324));
		floatList2.add(new Float(1000.8));
		floatList2.add(new Float(500.0));
		floatList2.add(new Float(10.2094));
		assertTrue(BEncoder.listsAreIdentical(floatList1, floatList2));
		
		strList1.add("This");
		strList1.add("is");
		strList1.add("a");
		strList1.add("test.!@#$%^&*()\n\r\t");
		strList2.add("This");
		strList2.add("is");
		strList2.add("a");
		strList2.add("test.!@#$%^&*()\n\r\t");
		assertTrue(BEncoder.listsAreIdentical(strList1, strList2));
	
		assertFalse(BEncoder.listsAreIdentical(strList1, intList1));
		assertFalse(BEncoder.listsAreIdentical(strList1, longList1));
		assertFalse(BEncoder.listsAreIdentical(strList1, floatList1));
		assertFalse(BEncoder.listsAreIdentical(floatList1, intList1));
		assertFalse(BEncoder.listsAreIdentical(floatList1, longList1));
		assertFalse(BEncoder.listsAreIdentical(longList1, intList1));
		assertFalse(BEncoder.listsAreIdentical(floatList1, longList1));
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.BEncoder#mapsAreIdentical(java.util.Map, java.util.Map)}.
	 */
	public void testMapsAreIdentical() {
		//Map<?,?>
	}

	public void testObjectsAreIdentical() {
		assertTrue(BEncoder.objectsAreIdentical(new String("test"), new String("test")));
		assertFalse(BEncoder.objectsAreIdentical(new String("test"), new String("TEST")));
		assertTrue(BEncoder.objectsAreIdentical(new Integer(7), new Integer(7)));
		assertFalse(BEncoder.objectsAreIdentical(new Integer(7), new Integer(9000)));
		assertTrue(BEncoder.objectsAreIdentical(new Long(7), new Long(7)));
		assertFalse(BEncoder.objectsAreIdentical(new Long(7), new Long(9000)));
		assertTrue(BEncoder.objectsAreIdentical(new Float(7.43209345), new Float(7.43209345)));
		assertFalse(BEncoder.objectsAreIdentical(new Float(7.43209345), new Float(4975.93275)));
		assertTrue(BEncoder.objectsAreIdentical(new byte[] { 29, 12, 32, 42, 2, 1 }, new byte[] { 29, 12, 32, 42, 2, 1 }));
		assertFalse(BEncoder.objectsAreIdentical(new byte[] { 29, 12, 32, 42, 2, 1 }, new byte[] { 29, 12, 32, 42, 2, 8 }));
		//TODO ByteBuffer
	}
}