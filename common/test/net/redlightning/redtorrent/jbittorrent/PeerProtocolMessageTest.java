/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.jbittorrent;

import junit.framework.TestCase;


/**
 * @author Michael Isaacson
 *
 */
public class PeerProtocolMessageTest extends TestCase {

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerProtocolMessage#generate()}.
	 */
	
	public void testGenerate() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerProtocolMessage#PeerProtocolMessage()}.
	 */
	
	public void testPeerProtocolMessage() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerProtocolMessage#PeerProtocolMessage(int, int)}.
	 */
	
	public void testPeerProtocolMessageIntInt() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerProtocolMessage#PeerProtocolMessage(int)}.
	 */
	
	public void testPeerProtocolMessageInt() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerProtocolMessage#PeerProtocolMessage(int, byte[], int)}.
	 */
	
	public void testPeerProtocolMessageIntByteArrayInt() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerProtocolMessage#PeerProtocolMessage(int, byte[])}.
	 */
	
	public void testPeerProtocolMessageIntByteArray() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerProtocolMessage#getLength()}.
	 */
	
	public void testGetLength() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerProtocolMessage#getID()}.
	 */
	
	public void testGetID() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerProtocolMessage#getPayload()}.
	 */
	
	public void testGetPayload() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerProtocolMessage#setLength(byte[])}.
	 */
	
	public void testSetLength() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerProtocolMessage#setID(int)}.
	 */
	
	public void testSetID() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerProtocolMessage#setPayload(byte[])}.
	 */
	
	public void testSetPayload() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerProtocolMessage#setData(int)}.
	 */
	
	public void testSetDataInt() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerProtocolMessage#setData(int, byte[])}.
	 */
	
	public void testSetDataIntByteArray() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerProtocolMessage#setData(byte[], byte, byte[])}.
	 */
	
	public void testSetDataByteArrayByteByteArray() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.PeerProtocolMessage#toString()}.
	 */
	
	public void testToString() {
		fail("Not yet implemented"); // TODO
	}

}
