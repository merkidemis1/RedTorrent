/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.jbittorrent;

import junit.framework.TestCase;


/**
 * @author Michael Isaacson
 *
 */
public class PeerTest extends TestCase {

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#hashCode()}.
	 */
	
	public void testHashCode() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#Peer()}.
	 */
	
	public void testPeer() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#Peer(java.lang.String, java.lang.String, int)}.
	 */
	
	public void testPeerStringStringInt() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#resetDL()}.
	 */
	
	public void testResetDL() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#resetUL()}.
	 */
	
	public void testResetUL() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#getDLRate(boolean)}.
	 */
	
	public void testGetDLRate() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#getULRate(boolean)}.
	 */
	
	public void testGetULRate() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#getDL()}.
	 */
	
	public void testGetDL() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#getUL()}.
	 */
	
	public void testGetUL() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#setDLRate(int)}.
	 */
	
	public void testSetDLRate() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#setULRate(int)}.
	 */
	
	public void testSetULRate() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#getID()}.
	 */
	
	public void testGetID() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#getIP()}.
	 */
	
	public void testGetIP() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#getPort()}.
	 */
	
	public void testGetPort() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#getHasPiece()}.
	 */
	
	public void testGetHasPiece() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#setID(java.lang.String)}.
	 */
	
	public void testSetID() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#setIP(java.lang.String)}.
	 */
	
	public void testSetIP() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#setPort(int)}.
	 */
	
	public void testSetPort() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#isInterested()}.
	 */
	
	public void testIsInterested() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#isChoked()}.
	 */
	
	public void testIsChoked() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#isInteresting()}.
	 */
	
	public void testIsInteresting() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#isChoking()}.
	 */
	
	public void testIsChoking() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#setInterested(boolean)}.
	 */
	
	public void testSetInterested() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#setChoked(boolean)}.
	 */
	
	public void testSetChoked() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#setInteresting(boolean)}.
	 */
	
	public void testSetInteresting() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#setChoking(boolean)}.
	 */
	
	public void testSetChoking() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#setHasPiece(byte[])}.
	 */
	
	public void testSetHasPieceByteArray() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#setHasPiece(int, boolean)}.
	 */
	
	public void testSetHasPieceIntBoolean() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#isConnected()}.
	 */
	
	public void testIsConnected() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#setConnected(boolean)}.
	 */
	
	public void testSetConnected() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#equals(java.lang.Object)}.
	 */
	
	public void testEqualsObject() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.jbittorrent.Peer#toString()}.
	 */
	
	public void testToString() {
		fail("Not yet implemented"); // TODO
	}

}
