/**
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent;

import java.util.Iterator;
import net.redlightning.jbittorrent.Utils;
import net.redlightning.redtorrent.common.DownloadListAdapter;
import net.redlightning.redtorrent.common.MessageHandler;
import net.redlightning.redtorrent.common.Settings;
import net.redlightning.redtorrent.common.TorrentActivity;
import net.redlightning.redtorrent.common.TorrentFilePickerDialog;
import net.redlightning.redtorrent.common.TorrentService;
import net.redlightning.redtorrent.search.SearchResultListAdapter;
import net.redlightning.redtorrent.search.TorrentSearch;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Main activity for Red Torrent
 * 
 * @author Michael Isaacson
 * @version 12.6.29
 */
public class RedTorrent extends TorrentActivity {
	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(final Bundle bundle) {
		super.onCreate(bundle);
		setDefaultKeyMode(DEFAULT_KEYS_SEARCH_LOCAL);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		Utils.version = getVersionName(this.getApplicationContext());
		Utils.name = getString(R.string.app_name);
		setContentView(R.layout.main);

		if (findViewById(R.id.memory) != null) {
			TorrentActivity.updateMemory(RedTorrent.this, (TextView) findViewById(R.id.memory), (TextView) findViewById(R.id.sdcard), null);
		}

		((ImageView) findViewById(R.id.div)).setImageDrawable(TorrentActivity.GRADIENT);
		((ImageView) findViewById(R.id.div2)).setImageDrawable(TorrentActivity.GRADIENT);
		((LinearLayout) findViewById(R.id.main_layout)).setBackgroundDrawable(TorrentActivity.BACK_GRADIENT);

		final ImageView addImage = (ImageView) findViewById(R.id.addImage);
		addImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View view) {
				startActivityForResult(new Intent(RedTorrent.this, DirectoryBrowser.class), FILE_BROWSER);
			}
		});

		final ImageView searchImage = (ImageView) findViewById(R.id.searchImage);
		searchImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View view) {
				onSearchRequested();
			}
		});

		final ImageView menuImage = (ImageView) findViewById(R.id.menuImage);
		final Activity context = this;
		menuImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View view) {
				context.openOptionsMenu();
			}
		});
		
		handler = new MessageHandler(RedTorrent.this, R.id.memory, R.id.sdcard);
		//Start the service
		startService(new Intent(getBaseContext(), RedService.class));
		TorrentService.setUiHandler(handler);
		downloaders = TorrentService.getDownloaderList();

		// Generate the ListView
		DownloadListAdapter.setViews(R.layout.list_item, R.id.text, R.id.speed, R.id.status, R.id.allocation, R.id.peers, R.id.error_message, R.id.progress);
		setAdapter(new DownloadListAdapter(this, R.layout.list_item, downloaders));
		setListAdapter(getAdapter());

		//Set up search controls
		SearchResultListAdapter.initialize(R.layout.search_result_item, R.id.search_results_list, R.id.torrentName, R.id.size, R.id.peers);
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onMenuItemSelected(int, android.view.MenuItem)
	 */
	@Override
	public boolean onMenuItemSelected(final int itemNum, final MenuItem item) {
		switch (item.getItemId()) {
			case MENU_EXIT:
				TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.QUIT));
				stopService(new Intent(getBaseContext(), RedService.class));
				onDestroy();
				finish();
				break;
			case MENU_SETTINGS:
				final Intent intent = new Intent(this, SettingsActivity.class);
				startActivity(intent);
				break;
			case MENU_OPEN:
				startActivityForResult(new Intent(this, DirectoryBrowser.class), FILE_BROWSER);
				break;
			case MENU_SEARCH:
				onSearchRequested();
				break;
			case MENU_STOP_ALL:
				final Iterator<String> keys = TorrentService.getDownloaders().keySet().iterator();
				while (keys.hasNext()) {
					TorrentService.getDownloaders().get(keys.next()).stopDownloading();
				}
				getAdapter().notifyDataSetChanged();
				Toast.makeText(this, R.string.allStopped, Toast.LENGTH_SHORT).show();
				break;
			default:
				break;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onPrepareOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onPrepareOptionsMenu(final Menu menu) {
		menu.clear();
		menu.add(Menu.NONE, MENU_OPEN, 0, R.string.addTorrent).setIcon(android.R.drawable.ic_menu_add);
		menu.add(Menu.NONE, MENU_SEARCH, 1, R.string.search).setIcon(android.R.drawable.ic_menu_search);
		menu.add(Menu.NONE, MENU_STOP_ALL, 2, R.string.stopAll).setIcon(android.R.drawable.ic_menu_close_clear_cancel);
		menu.add(Menu.NONE, MENU_SETTINGS, 3, R.string.settings).setIcon(android.R.drawable.ic_menu_preferences);
		menu.add(Menu.NONE, MENU_EXIT, 4, R.string.quit).setIcon(android.R.drawable.ic_menu_delete);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		super.onResume();

		if (getIntent().getData() != null) {
			if (getIntent().getData().toString().startsWith("magnet:")) {
				TorrentService.handleMagnet(getIntent());
			} else {
				pickerDialog = new TorrentFilePickerDialog(this, getIntent().getData().toString().replace("file://", "")).build();
				pickerDialog.show();
				getIntent().setData(null);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onSearchRequested()
	 */
	@Override
	public boolean onSearchRequested() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		final View layout = inflater.inflate(R.layout.search, (ViewGroup) findViewById(R.id.searchDialog));

		final Spinner providers = (Spinner) layout.findViewById(R.id.searchProvider);
		final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.search_providers, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		providers.setAdapter(adapter);
		providers.setSelection(Settings.getSearchProvider());
		
		final TorrentSearch searchListener = new TorrentSearch(this, R.layout.search_results, (EditText) layout.findViewById(R.id.searchInput), providers);

		builder.setTitle(R.string.search);
		builder.setIcon(android.R.drawable.ic_menu_search);
		builder.setView(layout);
		builder.setPositiveButton(R.string.search, searchListener);
		builder.setNegativeButton(R.string.cancel, null);
		builder.create().show();
		return false;
	}
}