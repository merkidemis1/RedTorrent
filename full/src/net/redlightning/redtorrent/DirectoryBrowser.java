/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

// TODO handle back button correctly

/**
 * @author Niall Kader
 * @author Michael Isaacson
 * @see www.remwebdevelopment.com/dev/a34/Directory-Browser-Application.html
 */
public class DirectoryBrowser extends ListActivity {
	private transient List<String> items = null;

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(final Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.browser);
		getFiles(new File(Environment.getExternalStorageDirectory().getPath()).listFiles());
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.ListActivity#onListItemClick(android.widget.ListView,
	 * android.view.View, int, long)
	 */
	@Override
	protected void onListItemClick(final ListView listView, final View view, final int position, final long selected) {
		if ((int) selected == 0) {
			getFiles(new File(Environment.getExternalStorageDirectory().getPath()).listFiles());
		} else {
			final File file = new File(items.get((int) selected));
			if (file.isDirectory()) {
				getFiles(file.listFiles());
			} else {
				Intent data;
				data = new Intent();
				data.putExtra("file", file.getAbsolutePath());
				if (getParent() == null) {
					setResult(Activity.RESULT_OK, data);
				} else {
					getParent().setResult(Activity.RESULT_OK, data);
				}
				finish();
			}
		}
	}

	/**
	 * @param files the list of files to add to the ListAdapter
	 */
	private void getFiles(final File[] files) {
		ArrayAdapter<String> fileList;
		List<String> torrentItems;
		
		torrentItems = new ArrayList<String>(); 
		items = new ArrayList<String>();
		items.add(getString(R.string.gotoRoot));

		if (files != null && files.length > 0) {
			for (File file : files) {
				if (file.isDirectory() ) {
					items.add(file.getPath());
				} else if(file.getName().endsWith(".torrent")) {
					torrentItems.add(file.getPath());
				}
			}
			
		}
		Collections.sort(items);
		Collections.sort(torrentItems);
		items.addAll(torrentItems);
		fileList = new ArrayAdapter<String>(this, R.layout.file_list_item, items);
		setListAdapter(fileList);
	}
}
