/**
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.Toast;

// TODO handle back button correctly

/**
 * @author Niall Kader (original)
 * @author Michael Isaacson (revisions)
 * @version 12.8.14
 * @see www.remwebdevelopment.com/dev/a34/Directory-Browser-Application.html
 */
public class SavePathBrowser extends ListActivity {
	private static final String TAG = SavePathBrowser.class.getSimpleName();
	private transient List<String> items = null;

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(final Bundle icicle) {
		super.onCreate(icicle);
		ListView listView;
		setContentView(R.layout.browser);
		getFiles(new File(Environment.getExternalStorageDirectory().getPath()).listFiles());

		// ListActivity has a ListView, which you can get with:
		listView = getListView();

		// Then you can create a listener like so:
		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			public boolean onItemLongClick(final AdapterView<?> parent, final View view, final int pos, final long ident) {
				onLongListItemClick(view, pos, ident);
				return true;
			}
		});
		Toast.makeText(this, R.string.tapSelect, Toast.LENGTH_SHORT).show();
	}

	// You then create your handler method:
	protected void onLongListItemClick(final View view, final int position, final long ident) {
		Log.i(TAG, "onLongListItemClick id=" + ident + " position=" + position);
		final Intent data = new Intent();
		data.putExtra("path", items.get(position).replace("/mnt", ""));
		setResult(Activity.RESULT_OK, data);
		Toast.makeText(this, getString(R.string.pathSet) + items.get(position).replace("/mnt", ""), Toast.LENGTH_SHORT).show();
		this.onDestroy();
		this.finish();
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.ListActivity#onListItemClick(android.widget.ListView,
	 * android.view.View, int, long)
	 */
	@Override
	protected void onListItemClick(final ListView listView, final View view, final int position, final long selected) {
		if ((int) selected == 0) {
			getFiles(new File(Environment.getExternalStorageDirectory().getPath()).listFiles());
		} else {
			final File file = new File(items.get((int) selected));
			if (file.isDirectory()) {
				getFiles(file.listFiles());
			} else {
				Intent data;
				data = new Intent();
				data.putExtra("file", file.getAbsolutePath());
				if (getParent() == null) {
					setResult(Activity.RESULT_OK, data);
				} else {
					getParent().setResult(Activity.RESULT_OK, data);
				}
				finish();
			}
		}
	}

	/**
	 * @param files the list of files to add to the ListAdapter
	 */
	private void getFiles(final File[] files) {
		ArrayAdapter<String> fileList;
		items = new ArrayList<String>();
		items.add(getString(R.string.gotoRoot));

		if (files != null && files.length > 0) {
			for (File file : files) {
				if (file.isDirectory()) {
					items.add(file.getPath());
				}
			}
		}
		Collections.sort(items);
		fileList = new ArrayAdapter<String>(this, R.layout.file_list_item, items);
		setListAdapter(fileList);
	}
}
