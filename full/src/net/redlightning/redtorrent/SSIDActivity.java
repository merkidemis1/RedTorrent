/**
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent;

import java.util.List;
import net.redlightning.redtorrent.common.SSIDListAdapter;
import net.redlightning.redtorrent.common.Settings;
import net.redlightning.redtorrent.common.TorrentActivity;
import net.redlightning.redtorrent.common.TorrentService;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;

/**
 * @author Michael Isaacson
 * @version 12.6.27
 */
public class SSIDActivity extends ListActivity {
	public static SSIDListAdapter adapter;
	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(final Bundle icicle) {
		super.onCreate(icicle);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.networks);
		
		((ImageView) findViewById(R.id.div)).setImageDrawable(TorrentActivity.GRADIENT);
		((ImageView) findViewById(R.id.div2)).setImageDrawable(TorrentActivity.GRADIENT);
		
		final List<String> rows = Settings.getWifiNetworkSSIDs();
		
		adapter = new SSIDListAdapter(this, R.layout.network_item, rows);
		SSIDListAdapter.setViews(R.layout.network_item, R.layout.networks, R.id.ssidName, R.id.ssidRemove);
		final ImageView addButton = (ImageView) findViewById(R.id.addSSIDImage);
		final EditText ssidField = (EditText) findViewById(R.id.ssidInput);

		addButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View view) {
				if (ssidField.getText() != null && ssidField.getText().toString().length() > 0) {
					//adapter.add(ssidField.getText().toString());
					
					Settings.getWifiNetworkSSIDs().add(ssidField.getText().toString());
					adapter.notifyDataSetChanged();
					TorrentService.saveSettings();
					ssidField.setText(null);
				}
			}
		});
		setListAdapter(adapter);
	}
}