package net.redlightning.redtorrent.test.common;

import net.redlightning.redtorrent.common.Downloader;
import net.redlightning.redtorrent.common.TorrentActivity;
import android.os.Environment;
import android.test.ActivityInstrumentationTestCase2;

/**
 * @author Michael Isaacson
 * @version 11.1.6
 */
public class DownloaderTest extends ActivityInstrumentationTestCase2<TorrentActivity> {
	private static final String TAG = DownloaderTest.class.getSimpleName();
	private TorrentActivity mActivity;
	/**
	 * Constructor
	 */
	public DownloaderTest() {
		super("net.redlightning.redtorrent.RedTorrent", TorrentActivity.class);
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.test.ActivityInstrumentationTestCase2#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mActivity = this.getActivity();
		//mActivity.clearTorrents();
	}
	
	/**
	 * Test method for
	 * {@link net.redlightning.redtorrent.common.Downloader#run()}.
	 */
	public void testRun() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for
	 * {@link net.redlightning.redtorrent.common.Downloader#setHandler(android.os.Handler)}
	 */
	public void testSetHandler() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for
	 * {@link net.redlightning.redtorrent.common.Downloader#Downloader(java.lang.String, java.lang.String, android.os.Handler, java.util.List)}
	 */
	public void testDownloader() {
		Downloader downloader = new Downloader(Environment.getExternalStorageDirectory().getPath() + "/valid.torrent", Environment.getExternalStorageDirectory().getPath() + "/download", null, null);
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for
	 * {@link net.redlightning.redtorrent.common.Downloader#stopDownloading()}.
	 */
	public void testStopDownloading() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for
	 * {@link net.redlightning.redtorrent.common.Downloader#getManager()}.
	 */
	public void testGetManager() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for
	 * {@link net.redlightning.redtorrent.common.Downloader#getManager(android.os.Handler)}
	 */
	public void testGetManagerHandler() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for
	 * {@link net.redlightning.redtorrent.common.Downloader#toString()}.
	 */
	public void testToString() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for
	 * {@link net.redlightning.redtorrent.common.Downloader#getRate()}.
	 */
	public void testGetRate() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for
	 * {@link net.redlightning.redtorrent.common.Downloader#getPercentDone()}.
	 */
	public void testGetPercentDone() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for
	 * {@link net.redlightning.redtorrent.common.Downloader#update(java.lang.String)}
	 */
	public void testUpdate() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for
	 * {@link net.redlightning.redtorrent.common.Downloader#getTorrent()}.
	 */
	public void testGetTorrent() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for
	 * {@link net.redlightning.redtorrent.common.Downloader#setTorrent(net.redlightning.jbittorrent.TorrentFile)}
	 * .
	 */
	public void testSetTorrent() {
		fail("Not yet implemented"); // TODO
	}
}
