/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.test;

import java.net.URLDecoder;
import net.redlightning.redtorrent.RedTorrent;
import net.redlightning.redtorrent.common.TorrentService;
import android.content.Intent;
import android.net.Uri;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

/**
 * UI tests for RedTorrent and TorrentActivity
 * 
 * @author Michael Isaacson
 * @version 11.12.14
 */
public class MagnetTest extends ActivityInstrumentationTestCase2<RedTorrent> {
	private static final String TAG = MagnetTest.class.getSimpleName();
	//private Solo robot;

	/**
	 * Constructor
	 * 
	 * @param pkg the full package name
	 * @param activityClass the class under test that extends Activity
	 */
	public MagnetTest(final String pkg, final Class<RedTorrent> activityClass) {
		super(pkg, activityClass);
	}

	/**
	 * Default constructor
	 */
	public MagnetTest() {
		super("net.redlightning.redtorrent", RedTorrent.class);
	}

	/*
	 * (non-Javadoc)
	 * @see android.test.ActivityInstrumentationTestCase2#setUp()
	 */
	@Override
	public void setUp() throws Exception {
		super.setUp();
		//robot = new Solo(getInstrumentation(), getActivity());
		while (RedTorrent.getAdapter().getCount() > 0) {
			TorrentService.removeTorrent(this.getActivity(), 0);
		}
	}


	/**
	 * Test method for {@link net.redlightning.redtorrent.common.TorrentActivity#addTorrent(java.lang.String)}
	 */
	public void testHandleMagnet() {
		final Intent intent = new Intent();
		String decoded = URLDecoder.decode("magnet:?xt=urn:btih:bbb6db69965af769f664b6636e7914f8735141b3&dn=Ubuntu-12.04-desktop-i386.iso&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80");
		Log.i(TAG, decoded);
		intent.setData(Uri.parse(decoded));
		
		TorrentService.handleMagnet(intent);
	}
}