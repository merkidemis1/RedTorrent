/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.test;

import java.util.ArrayList;
import java.util.List;
import com.jayway.android.robotium.solo.Solo;
import net.redlightning.redtorrent.RedTorrent;
import net.redlightning.redtorrent.common.TorrentActivity;
import net.redlightning.redtorrent.common.TorrentService;
import android.app.Dialog;
import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;

/**
 * UI tests for RedTorrent and TorrentActivity
 * 
 * @author Michael Isaacson
 * @version 11.12.14
 */
public class RedTorrentTest extends ActivityInstrumentationTestCase2<RedTorrent> {
	private Solo robot;

	/**
	 * Constructor
	 * 
	 * @param pkg the full package name
	 * @param activityClass the class under test that extends Activity
	 */
	public RedTorrentTest(final String pkg, final Class<RedTorrent> activityClass) {
		super(pkg, activityClass);
	}

	/**
	 * Default constructor
	 */
	public RedTorrentTest() {
		super("net.redlightning.redtorrent", RedTorrent.class);
	}

	/*
	 * (non-Javadoc)
	 * @see android.test.ActivityInstrumentationTestCase2#setUp()
	 */
	@Override
	public void setUp() throws Exception {
		super.setUp();
		robot = new Solo(getInstrumentation(), getActivity());
		while (RedTorrent.getAdapter().getCount() > 0) {
			TorrentService.removeTorrent(this.getActivity(), 0);
		}
	}

	/**
	 * Test of net.redlightning.redtorrent.RedTorrent.onCreate(Bundle)
	 */
	public void testOnCreate() {
		assertNotNull(RedTorrent.getAdapter());
		assertNotNull(RedTorrent.downloaders);
	}

	/**
	 * Test of net.redlightning.redtorrent.RedTorrent.onResume()
	 */
	public void testOnResume() {
		fail("Not yet implemented");
	}

	/**
	 * Test of net.redlightning.redtorrent.RedTorrent.onMenuItemSelected
	 */
	public void testOnMenuItemSelected() {
		robot.clickOnMenuItem("Add Torrent");
		robot.sleep(5000);
		assertEquals("DirectoryBrowser", robot.getCurrentActivity().getLocalClassName());
		robot.goBack();

		robot.clickOnMenuItem("Search");
		//TODO assert that the search box is showing
		robot.sleep(2000);
		robot.goBack();
		//TODO check to see if the search edit box is still visible
		robot.sleep(2000);
		robot.goBack();

		robot.clickOnMenuItem("Stop All");
		robot.sleep(2000);

		robot.clickOnMenuItem("Settings");
		robot.sleep(5000);
		assertEquals("SettingsActivity", robot.getCurrentActivity().getLocalClassName());
		robot.goBack();
		if ("SettingsActivity".equals(robot.getCurrentActivity().getLocalClassName())) {
			robot.goBack();
		}
		robot.clickOnMenuItem("Quit");
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.common.TorrentActivity#loadSettings(android.content.Context)}
	 */
	public void testLoadSettings() {
		TorrentService.loadSettings(getActivity());
		assertNotNull(TorrentService.settings);
		assertNotNull(TorrentService.settings.getString("downloadPath", "/sdcard/download/"));
		assertNotNull(TorrentService.settings.getBoolean("wifiOnly", false));
		assertNotNull(TorrentService.settings.getBoolean("wifiResume", false));
		assertNotNull(TorrentService.settings.getInt("maxActive", 3));
		assertNotNull(TorrentService.settings.getInt("maxConnections", 500));
		assertNotNull(TorrentService.settings.getInt("maxPieces", 30));
		assertNotNull(TorrentService.settings.getInt("lowPort", 6881));
		assertNotNull(TorrentService.settings.getInt("highPort", 6889));
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.common.TorrentActivity#addTorrent(java.lang.String)}
	 */
	public void testAddTorrent() {
		TorrentService.addTorrent(this.getActivity(), "/sdcard/valid.torrent", null, null);
		assertEquals(1, RedTorrent.getAdapter().getCount());
		assertEquals(1, TorrentService.getDownloaders().size());
		TorrentService.addTorrent(this.getActivity(), "/sdcard/valid_gzipped.torrent", null, null);
		assertEquals(2, RedTorrent.getAdapter().getCount());
		assertEquals(2, TorrentService.getDownloaders().size());
		TorrentService.removeTorrent(this.getActivity(), 0);
		assertEquals(1, RedTorrent.getAdapter().getCount());
		assertEquals(1, TorrentService.getDownloaders().size());
		TorrentService.removeTorrent(this.getActivity(), 0);
		assertEquals(0, RedTorrent.getAdapter().getCount());
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.common.TorrentActivity#addTorrent(java.lang.String, java.util.List)}
	 */
	public void testAddTorrentWithSelectedFiles() {
		List<String> fileList = new ArrayList<String>(1);

		fileList.add("Azureus4.5.1.0.jar");
		assertEquals(1, fileList.size());

		TorrentService.addTorrent(this.getActivity(), "/sdcard/valid.torrent", fileList, null);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		assertEquals(1, RedTorrent.getAdapter().getCount());
		assertEquals(1, TorrentService.getDownloaders().size());
		assertEquals(1, RedTorrent.getAdapter().getItem(0).getTorrent().getFileNames().size());
		assertEquals(1, RedTorrent.getAdapter().getItem(0).getTorrent().getSelectedFiles().size());
		TorrentService.removeTorrent(getActivity(), 0);
		assertEquals(0, RedTorrent.getAdapter().getCount());
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.common.TorrentActivity#getAdapter()}.
	 */
	public void testGetAdapter() {
		assertEquals(0, TorrentActivity.getAdapter().getCount());
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.common.TorrentActivity#saveSettings()}
	 */
	public void testSaveSettings() {
		assertTrue(TorrentService.saveSettings());
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.common.TorrentActivity#onActivityResult(int, int, android.content.Intent)}
	 */
	public void testOnActivityResult() {
		final Intent intent = new Intent();
		intent.putExtra("file", "/sdcard/valid.torrent");

		//Test the negative button
		getActivity().onActivityResult(TorrentActivity.FILE_BROWSER, TorrentActivity.RESULT_OK, intent);
		assertNotNull(getActivity().pickerDialog);
		assertTrue(getActivity().pickerDialog.isShowing());
		getActivity().pickerDialog.getButton(Dialog.BUTTON_NEGATIVE).performClick();
		//assertFalse(getActivity().pickerDialog.isShowing());
		assertEquals(0, RedTorrent.getAdapter().getCount());

		//Test the positive button
		getActivity().onActivityResult(TorrentActivity.FILE_BROWSER, TorrentActivity.RESULT_OK, intent);
		assertNotNull(getActivity().pickerDialog);
		assertTrue(getActivity().pickerDialog.isShowing());
		getActivity().pickerDialog.getButton(Dialog.BUTTON_POSITIVE).performClick();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//assertFalse(getActivity().pickerDialog.isShowing());
		assertEquals(1, RedTorrent.getAdapter().getCount());
		TorrentService.removeTorrent(this.getActivity(), 0);
		assertEquals(0, RedTorrent.getAdapter().getCount());
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.common.TorrentActivity#removeTorrent(java.lang.String)}
	 */
	public void testRemoveTorrent() {
		TorrentService.addTorrent(this.getActivity(), "/sdcard/valid.torrent", null, null);
		assertEquals(1, TorrentService.getDownloaders().size());
		TorrentService.removeTorrent(this.getActivity(), 0);
		assertEquals(0, RedTorrent.getAdapter().getCount());
		//clearAssets();
	}

	/**
	 * Test method for
	 * {@link net.redlightning.redtorrent.common.TorrentActivity#updateMemory(android.content.Context, android.widget.TextView, android.widget.TextView)}
	 */
	public void testUpdateMemory() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.common.TorrentActivity#onDestroy()}.
	 */
	public void testOnDestroy() {
		getActivity().onDestroy();
		assertNull(TorrentService.uiHandler);
	}
}
