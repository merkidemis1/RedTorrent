/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.test;

import net.redlightning.redtorrent.DirectoryBrowser;
import android.test.ActivityInstrumentationTestCase2;

/**
 * @author Michael Isaacson
 * @version 10.12.29
 */
public class DirectoryBrowserTest extends ActivityInstrumentationTestCase2<DirectoryBrowser> {
	/**
	 * 
	 */
	public DirectoryBrowserTest() {
		super(DirectoryBrowser.class.getSimpleName(), DirectoryBrowser.class);
	}

	/**
	 * Test method for
	 * {@link net.redlightning.redtorrent.DirectoryBrowser#onCreate(android.os.Bundle)}
	 * .
	 */
	public void testOnCreateBundle() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for
	 * {@link net.redlightning.redtorrent.DirectoryBrowser#onListItemClick(android.widget.ListView, android.view.View, int, long)}
	 * .
	 */
	public void testOnListItemClickListViewViewIntLong() {
		fail("Not yet implemented"); // TODO
	}
}
