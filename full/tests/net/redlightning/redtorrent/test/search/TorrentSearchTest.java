/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.test.search;

import junit.framework.TestCase;

/**
 * @author Michael Isaacson
 * @version 12.6.28
 */
public class TorrentSearchTest extends TestCase {

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.search.TorrentSearch#TorrentSearch(net.redlightning.redtorrent.common.TorrentActivity, android.view.View, int, android.widget.EditText)}.
	 */
	public void testTorrentSearch() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.search.TorrentSearch#onClick(android.view.View)}.
	 */
	public void testOnClick() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.search.TorrentSearch#buildSearchResultList(net.redlightning.json.JSONArray)}.
	 */
	public void testBuildSearchResultList() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.search.TorrentSearch#onEditorAction(android.widget.TextView, int, android.view.KeyEvent)}.
	 */
	public void testOnEditorAction() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.search.TorrentSearch#getProgressDialog()}.
	 */
	public void testGetProgressDialog() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.search.TorrentSearch#setProgressDialog(android.app.ProgressDialog)}.
	 */
	public void testSetProgressDialog() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.search.TorrentSearch#getResultsDialog()}.
	 */
	public void testGetResultsDialog() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.search.TorrentSearch#setResultsDialog(android.app.AlertDialog)}.
	 */
	public void testSetResultsDialog() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.search.TorrentSearch#getContext()}.
	 */
	public void testGetContext() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.search.TorrentSearch#getInput()}.
	 */
	public void testGetInput() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.search.TorrentSearch#getSearchBoxLayout()}.
	 */
	public void testGetSearchBoxLayout() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.search.TorrentSearch#getResultListLayout()}.
	 */
	public void testGetResultListLayout() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.search.TorrentSearch#getHandler()}.
	 */
	public void testGetHandler() {
		fail("Not yet implemented");
	}

}
