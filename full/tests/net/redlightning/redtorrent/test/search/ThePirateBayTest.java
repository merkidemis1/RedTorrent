/**
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.test.search;

import com.jayway.android.robotium.solo.Solo;
import net.redlightning.redtorrent.RedTorrent;
import net.redlightning.redtorrent.search.ThePirateBay;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

/**
 * Tests searching for torrents on ThePirateBay.org
 * 
 * @author Michael Isaacson
 * @version 11.12.20
 */
public class ThePirateBayTest extends ActivityInstrumentationTestCase2<RedTorrent> {
	private static final String TAG = ThePirateBayTest.class.getSimpleName();
	final ThePirateBay bay = new ThePirateBay("Ubuntu", null);
	private Solo robot;

	/**
	 * Constructor
	 * 
	 * @param pkg the full package name
	 * @param activityClass the class under test that extends Activity
	 */
	public ThePirateBayTest(String pkg, Class<RedTorrent> activityClass) {
		super(pkg, activityClass);
	}

	/**
	 * Default constructor
	 */
	public ThePirateBayTest() {
		super("net.redlightning.redtorrent", RedTorrent.class);
	}

	/*
	 * (non-Javadoc)
	 * @see android.test.AndroidTestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		robot = new Solo(getInstrumentation(), getActivity());
	}

	//	/**
	//	 * Test method for {@link net.redlightning.redtorrent.search.ThePirateBay#run()}.
	//	 */
	//	public void testRun() {
	//		fail("Not yet implemented");
	//	}
	//
	//	/**
	//	 * Test method for {@link net.redlightning.redtorrent.search.ThePirateBay#ThePirateBay(java.lang.String, android.os.Handler)}.
	//	 */
	//	public void testThePirateBay() {
	//		fail("Not yet implemented");
	//	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.search.ThePirateBay#search()}.
	 */
	public void testSearch() {
		Log.e(TAG, "Starting search test");
		assertNotNull(bay.search());
		robot.sleep(500);
	}
}