/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.test;

import android.test.suitebuilder.TestSuiteBuilder;
import junit.framework.TestSuite;

/**
 * Builds and executes all the JUnit tests for Red Torrent
 * 
 * @author Michael Isaacson
 * @version 11.12.19
 */
public class FullTest extends TestSuite {
	public static TestSuite suite() {
		return new TestSuiteBuilder(RedTorrentTest.class).includeAllPackagesUnderHere().build();
	}
}