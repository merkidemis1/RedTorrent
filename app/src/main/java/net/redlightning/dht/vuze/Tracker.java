/*
 * This file is part of mlDHT. mlDHT is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or (at your option) any later version. mlDHT is distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details. You should have received a copy of
 * the GNU General Public License along with mlDHT. If not, see
 * <http://www.gnu.org/licenses/>.
 */
package net.redlightning.dht.vuze;

import java.util.*;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import net.redlightning.dht.kad.*;
import net.redlightning.dht.kad.DHT.DHTtype;
//import net.redlightning.dht.kad.tasks.AnnounceTask;
import net.redlightning.dht.kad.tasks.PeerLookupTask;
import net.redlightning.dht.kad.tasks.Task;
import net.redlightning.dht.kad.tasks.TaskListener;
import net.redlightning.jbittorrent.udp.DHTHandler;
import net.redlightning.redtorrent.common.Downloader;
import net.redlightning.redtorrent.common.Settings;
import net.redlightning.redtorrent.common.TorrentService; // import
// org.gudy.azureus2.plugins.download.Download;
// import org.gudy.azureus2.plugins.download.DownloadAnnounceResult;
// import org.gudy.azureus2.plugins.download.DownloadAnnounceResultPeer;
// import org.gudy.azureus2.plugins.download.DownloadAttributeListener;
// import org.gudy.azureus2.plugins.download.DownloadException;
// import org.gudy.azureus2.plugins.download.DownloadListener;
// import org.gudy.azureus2.plugins.download.DownloadManagerListener;
// import org.gudy.azureus2.plugins.download.DownloadScrapeResult;
// import org.gudy.azureus2.plugins.download.DownloadTrackerListener;
import android.util.Log;

/**
 * @author Damokles
 * @author Michael Isaacson (Android port)
 * @version 11.4.12
 */
public class Tracker {
	private static final String TAG = Tracker.class.getSimpleName();
	private static final int MAX_CONCURRENT_ANNOUNCES = 8;
	private static final int MAX_CONCURRENT_SCRAPES = 1;
	private static final int TRACKER_UPDATE_INTERVAL = 30 * 1000;
	private static final int SHORT_DELAY = 60 * 1000;
	private static final int MIN_ANNOUNCE_INTERVAL = 5 * 60 * 1000;
	private static final int MAX_ANNOUNCE_INTERVAL = 20 * 60 * 1000;
	private static final int MIN_SCRAPE_INTERVAL = 20 * 60 * 1000;
	private static final int MAX_SCRAPE_INTERVAL = 10 * 60 * 1000;
	public static final String PEER_SOURCE_NAME = "DHT"; // DownloadAnnounceResultPeer.PEERSOURCE_DHT;
	private List<Downloader> currentAnnounces = new LinkedList<>();
	private List<Downloader> currentScrapes = new LinkedList<>();
	//private MlDHTPlugin plugin;
	private boolean running;
	private Random random = new Random();
	private ScheduledFuture<?> timer;
	private TorrentAttribute networks;
	private TorrentAttribute peerSources;
	private ListenerBundle listener;
	private Map<Downloader, TrackedTorrent> trackedTorrents = new HashMap<>();
	private Queue<TrackedTorrent> scrapeQueue = new DelayQueue<>();
	private Queue<TrackedTorrent> announceQueue = new DelayQueue<>();

	public Tracker() {
		listener = new ListenerBundle(this);
		//this.plugin = plugin;
		//ta_networks = plugin.getPluginInterface().getTorrentManager().getAttribute(TorrentAttribute.TA_NETWORKS);
		//ta_peer_sources = plugin.getPluginInterface().getTorrentManager().getAttribute(TorrentAttribute.TA_PEER_SOURCES);
	}

	public void start() {
		if (running) {
			return;
		}
		Log.i(TAG, "Tracker: starting...");
		timer = DHT.getScheduler().scheduleAtFixedRate(new Runnable() {
			public void run() {
				checkQueues();
			}
		}, 100 * 1000, TRACKER_UPDATE_INTERVAL, TimeUnit.MILLISECONDS);
//		Downloader[] downloads = (Downloader[]) TorrentService.getDownloaderList().toArray();
//		
//		for(Downloader dl: downloads) {
//			dl.addListener(listener);
//		}

		running = true;
	}

	public void stop() {
		if (!running) {
			return;
		}
		Log.i(TAG, "Tracker: stopping...");
		if (timer != null) {
			timer.cancel(false);
		}
		announceQueue.clear();
		trackedTorrents.clear();
		//		plugin.getPluginInterface().getDownloadManager().removeListener(listener);
		Downloader[] downloads = (Downloader[]) TorrentService.getDownloaderList().toArray();
		
//		for(Downloader dl: downloads) {
//			dl.removeListener(listener);
//		}

		running = false;
	}

	private void announceDownload(final Downloader dl) {
		if (running) {
			if (dl.getTorrent() == null) {
				return;
			}

			if (dl.getTorrent().isPrivate()) {
				Log.i(TAG, "Announce for [" + dl.getName() + "] forbidden because Torrent is private.");
				return;
			}
			if (trackedTorrents.containsKey(dl)) {
				if (trackedTorrents.get(dl).isAnnouncing()) {
					Log.i(TAG, "Announce for [" + dl.getName() + "] was denied since there is already one running.");
					return;
				}
			}
			Log.i(TAG, "DHT Starting Announce for " + dl.getName());

			final long startTime = System.currentTimeMillis();
			final TrackedTorrent tor = trackedTorrents.get(dl);

			if (tor != null) {
				tor.setAnnouncing(true);
				tor.setLastAnnounceStart(startTime);
			}

			final boolean scrapeOnly = dl.getCurrentState() == Downloader.ST_QUEUED;

			new TaskListener() {
				Set<PeerAddressDBItem> items = new HashSet<PeerAddressDBItem>();
				ScrapeResponseHandler handler = new ScrapeResponseHandler();
				AtomicInteger pendingCount = new AtomicInteger();

				{ // initializer
					(scrapeOnly ? currentScrapes : currentAnnounces).add(dl);

					for (DHTtype type : DHTtype.values()) {
						DHT dht = DHTHandler.dhts.get(type);
						PeerLookupTask lookupTask = dht.createPeerLookup(dl.getTorrent().getInfoHashAsBinary());
						if (lookupTask != null) {
							pendingCount.incrementAndGet();
							lookupTask.setScrapeHandler(handler);
							lookupTask.setScrapeOnly(scrapeOnly);
							lookupTask.addListener(this);
							lookupTask.setInfo(dl.getName());
							lookupTask.setNoSeeds(dl.isComplete());
							dht.getTaskManager().addTask(lookupTask);
						}
					}
				}

				@Override
				public void finished(Task t) {
					Log.d(TAG, "DHT Task done: " + t.getClass().getSimpleName());
					if (t instanceof PeerLookupTask) {
						PeerLookupTask peerLookup = (PeerLookupTask) t;
						synchronized (items) {
							items.addAll(peerLookup.getReturnedItems());
						}

						// if we're not just scraping the torrent... send announces
						if (!scrapeOnly)
							t.getRPC().getDHT().announce(peerLookup, dl.isComplete(), Settings.getLowPort());

						if (pendingCount.decrementAndGet() > 0)
							return;
						allFinished(peerLookup.getInfoHash().getHash());
					}
				}

				private void allFinished(byte[] hash) {
					handler.process();

					currentAnnounces.remove(dl);
					currentScrapes.remove(dl);

					if (tor != null) {
						tor.setAnnouncing(false);
					}

					// schedule the next announce (will be ignored if there is one pending)
					scheduleTorrent(dl, false);

					if (!scrapeOnly && items.size() > 0) {
						DHTAnnounceResult res = new DHTAnnounceResult(dl, items, tor != null ? (int) tor.getDelay(TimeUnit.SECONDS) : 0);
						res.setScrapePeers(handler.getScrapedPeers());
						res.setScrapeSeeds(handler.getScrapedSeeds());
						dl.setAnnounceResult(res);
					}

					if (scrapeOnly && (handler.getScrapedPeers() > 0 || handler.getScrapedSeeds() > 0)) {
						DHTScrapeResult res = new DHTScrapeResult(dl, handler.getScrapedSeeds(), handler.getScrapedPeers());
						res.setScrapeStartTime(startTime);
						dl.setScrapeResult(res);
					}
					
					//TODO Populate DownloadManager's peer list with these new peers
					//TODO Also add peers from the Tracker to the DHT
					Log.i(TAG, "DHT Announce finished for " + dl.getName() + " found " + items.size() + " Peers.");
				}

			};
		}
	}

	private void scheduleTorrent(Downloader dl, boolean shortDelay) {
		if (!running) {
			return;
		}

		if (trackedTorrents.containsKey(dl)) {
			TrackedTorrent t = trackedTorrents.get(dl);

			Queue<TrackedTorrent> targetQueue;
			int delay;

			if (t.scrapeOnly()) {
				targetQueue = scrapeQueue;
				announceQueue.remove(t);
				delay = shortDelay ? (SHORT_DELAY + random.nextInt(SHORT_DELAY)) : (MIN_SCRAPE_INTERVAL + random.nextInt(MAX_SCRAPE_INTERVAL));
			} else {
				targetQueue = announceQueue;
				scrapeQueue.remove(t);
				delay = shortDelay ? (SHORT_DELAY + random.nextInt(SHORT_DELAY)) : (MIN_ANNOUNCE_INTERVAL + random.nextInt(MAX_ANNOUNCE_INTERVAL));
			}

			if (targetQueue.contains(t)) {
				if (shortDelay)
					targetQueue.remove(t);
				else
					return; // still queued, no need to announce
			}
			t.setDelay(delay);

			Log.i(TAG, "Tracker: scheduled " + (t.scrapeOnly() ? "scrape" : "announce") + " in " + t.getDelay(TimeUnit.SECONDS) + "sec for: " + dl.getName());
			targetQueue.add(t);
		}
	}

	private void checkQueues() {
		if (!running) {
			return;
		}
		Log.d(TAG, "Checking DHT Queues");
		TrackedTorrent t;
		Downloader dl;

		while (currentAnnounces.size() < MAX_CONCURRENT_ANNOUNCES && (t = announceQueue.poll()) != null) {
			dl = t.getDownload();
			if (trackedTorrents.containsKey(dl) && trackedTorrents.get(dl).isAnnouncing())
				scheduleTorrent(dl, false);
			else
				announceDownload(dl);
		}

		while (currentScrapes.size() < MAX_CONCURRENT_SCRAPES && (t = scrapeQueue.poll()) != null) {
			dl = t.getDownload();
			if (trackedTorrents.containsKey(dl) && trackedTorrents.get(dl).isAnnouncing())
				scheduleTorrent(dl, false);
			else
				announceDownload(dl);
		}
	}

	void checkDownload(Downloader dl) {
		if (!running || dl.getTorrent() == null || dl.getTorrent().isPrivate())
			return;
//
//		String[] sources = dl.getListAttribute(ta_peer_sources);
//		String[] networks = dl.getListAttribute(ta_networks);
//
//		boolean ok = false;
//
//		if (networks != null) {
//			for (int i = 0; i < networks.length; i++) {
//				if (networks[i].equalsIgnoreCase("Public")) {
//					ok = true;
//					break;
//				}
//			}
//		}
//
//		if (!ok) {
//			removeTrackedTorrent(dl, "Network is not public anymore");
//			return;
//		}
//
//		ok = false;
//
//		for (int i = 0; i < sources.length; i++) {
//			if (sources[i].equalsIgnoreCase(PEER_SOURCE_NAME)) {
//				ok = true;
//				break;
//			}
//		}
//
//		if (!ok) {
//			removeTrackedTorrent(dl, "Peer source was disabled");
//			return;
//		}

		TrackedTorrent tor = trackedTorrents.get(dl);

		if (dl.getCurrentState() == Downloader.ST_DOWNLOADING || dl.getCurrentState() == Downloader.ST_SEEDING) {

			//			//only act as backup tracker
			//			if (plugin.getPluginInterface().getPluginconfig().getPluginBooleanParameter("backupOnly")) {
			//				DownloadAnnounceResult result = dl.getLastAnnounceResult();
			//
			//				if (result == null || result.getResponseType() == DownloadAnnounceResult.RT_ERROR) {
			//					addTrackedTorrent(dl, "BackupTracker");
			//				} else {
			//					removeTrackedTorrent(dl, "BackupTracker no longer needed");
			//				}
			//
			//				return;
			//			}
			//
			//			addTrackedTorrent(dl, "Normal");
			//
			//		} else 
			if (dl.getCurrentState() == Downloader.ST_QUEUED) {
				DHTScrapeResult scrResult = dl.getScrapeResult();

				if (scrResult.getResponseType() == DHTScrapeResult.RT_ERROR || (tor != null && scrResult.getScrapeStartTime() == tor.getLastAnnounceStart())) {
					// faulty states or our own scrape => let's scrape again 
					addTrackedTorrent(dl, "BackupScraper");
				} else {
					// scrape seems fine => no need for DHT scrape
					removeTrackedTorrent(dl, "BackupScraper no longer needed");
				}

			}
		} else {
			removeTrackedTorrent(dl, "Has stopped Downloading/Seeding");
		}
	}

	private void addTrackedTorrent(Downloader dl, String reason) {
		if (!trackedTorrents.containsKey(dl)) {
			Log.i(TAG, "Tracker: starting to track Torrent reason: " + reason + ", Torrent; " + dl.getName());
			trackedTorrents.put(dl, new TrackedTorrent(dl));
			scheduleTorrent(dl, true);
		}
	}

	void removeTrackedTorrent(Downloader dl, String reason) {
		if (trackedTorrents.containsKey(dl)) {
			Log.i(TAG, "Tracker: stop tracking of Torrent reason: " + reason + ", Torrent; " + dl.getName());
			TrackedTorrent tracked = trackedTorrents.get(dl);

			announceQueue.remove(tracked);
			scrapeQueue.remove(tracked);
			trackedTorrents.remove(dl);
		}
	}
//
//	public List<TrackedTorrent> getTrackedTorrentList() {
//		return new ArrayList<TrackedTorrent>(trackedTorrents.values());
//	}
//
//	/**
//	 * @param networks the networks to set
//	 */
//	public void setNetworks(TorrentAttribute networks) {
//		this.networks = networks;
//	}

	/**
	 * @return the networks
	 */
	public TorrentAttribute getNetworks() {
		return networks;
	}

	public TorrentAttribute getPeerSources() {
		return peerSources;
	}

	public void setPeerSources(TorrentAttribute peerSources) {
		this.peerSources = peerSources;
	}

//	/**
//	 * @param peerSources the peerSources to set
//	 */
//	public void setPeerSources(TorrentAttribute peerSources) {
//		this.peerSources = peerSources;
//	}
//
//	/**
//	 * @return the peerSources
//	 */
//	private TorrentAttribute getPeerSources() {
//		return peerSources;
//	}
}
