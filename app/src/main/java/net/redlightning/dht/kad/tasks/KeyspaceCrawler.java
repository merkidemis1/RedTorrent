/*
 * This file is part of mlDHT. mlDHT is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or (at your option) any later version. mlDHT is distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details. You should have received a copy of
 * the GNU General Public License along with mlDHT. If not, see
 * <http://www.gnu.org/licenses/>.
 */
package net.redlightning.dht.kad.tasks;

import java.net.InetSocketAddress;
import java.util.HashSet;
import java.util.Set;
import net.redlightning.dht.kad.*;
import net.redlightning.dht.kad.DHT.DHTtype;
import net.redlightning.dht.kad.Node.RoutingTableEntry;
import net.redlightning.dht.kad.messages.FindNodeRequest;
import net.redlightning.dht.kad.messages.FindNodeResponse;
import net.redlightning.dht.kad.messages.MessageBase;
import net.redlightning.dht.kad.messages.MessageBase.Method;
import net.redlightning.dht.kad.messages.MessageBase.Type;
import net.redlightning.dht.kad.utils.PackUtil;

/**
 * @author The 8472
 */
public class KeyspaceCrawler extends Task {
	Set<InetSocketAddress> responded = new HashSet<InetSocketAddress>();

	/**
	 * Constructor
	 * @param rpc
	 * @param node
	 */
	public KeyspaceCrawler(RPCServerBase rpc, Node node) {
		super(Key.createRandomKey(), rpc, node);
		setInfo("Exhaustive Keyspace Crawl");
	}

	/*
	 * (non-Javadoc)
	 * @see net.redlightning.dht.kad.tasks.Task#update()
	 */
	@Override
	synchronized void update() {
		// go over the todo list and send find node calls
		// until we have nothing left
		synchronized (todo) {

			while (todo.size() > 0 && canDoRequest()) {
				KBucketEntry e = todo.first();
				todo.remove(e);
				// only send a findNode if we haven't allready visited the node
				if (!visited.contains(e)) {
					// send a findNode to the node
					FindNodeRequest fnr;

					fnr = new FindNodeRequest(Key.createRandomKey());
					fnr.setWant4(rpc.getDHT().getType() == DHTtype.IPV4_DHT || DHT.getDHT(DHTtype.IPV4_DHT).getNode().getNumEntriesInRoutingTable() < DHTConstants.BOOTSTRAP_IF_LESS_THAN_X_PEERS);
					fnr.setWant6(rpc.getDHT().getType() == DHTtype.IPV6_DHT || DHT.getDHT(DHTtype.IPV6_DHT).getNode().getNumEntriesInRoutingTable() < DHTConstants.BOOTSTRAP_IF_LESS_THAN_X_PEERS);
					fnr.setDestination(e.getAddress());
					rpcCall(fnr, e.getID());

					if (canDoRequest()) {
						fnr = new FindNodeRequest(e.getID());
						fnr.setWant4(rpc.getDHT().getType() == DHTtype.IPV4_DHT || DHT.getDHT(DHTtype.IPV4_DHT).getNode().getNumEntriesInRoutingTable() < DHTConstants.BOOTSTRAP_IF_LESS_THAN_X_PEERS);
						fnr.setWant6(rpc.getDHT().getType() == DHTtype.IPV6_DHT || DHT.getDHT(DHTtype.IPV6_DHT).getNode().getNumEntriesInRoutingTable() < DHTConstants.BOOTSTRAP_IF_LESS_THAN_X_PEERS);
						fnr.setDestination(e.getAddress());
						rpcCall(fnr, e.getID());
					}

					synchronized (visited) {
						visited.add(e);
					}
				}
				// remove the entry from the todo list
			}
		}

		if (todo.size() == 0 && getNumOutstandingRequests() == 0 && !isFinished()) {
			done();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see net.redlightning.dht.kad.tasks.Task#callFinished(net.redlightning.dht.kad.RPCCallBase, net.redlightning.dht.kad.messages.MessageBase)
	 */
	@Override
	void callFinished(RPCCallBase c, MessageBase rsp) {
		if (isFinished()) {
			return;
		}

		// check the response and see if it is a good one
		if (rsp.getMethod() == Method.FIND_NODE && rsp.getType() == Type.RSP_MSG) {

			FindNodeResponse fnr = (FindNodeResponse) rsp;

			responded.add(fnr.getOrigin());

			for (DHTtype type : DHTtype.values()) {
				byte[] nodes = fnr.getNodes(type);
				if (nodes == null)
					continue;
				int nval = nodes.length / type.NODES_ENTRY_LENGTH;
				if (type == rpc.getDHT().getType()) {
					synchronized (todo) {
						for (int i = 0; i < nval; i++) {
							// add node to todo list
							KBucketEntry e = PackUtil.UnpackBucketEntry(nodes, i * type.NODES_ENTRY_LENGTH, type);
							if (!node.allLocalIDs().contains(e.getID()) && !todo.contains(e) && !visited.contains(e)) {
								todo.add(e);
							}
						}
					}
				} else {
					for (int i = 0; i < nval; i++) {
						KBucketEntry e = PackUtil.UnpackBucketEntry(nodes, i * type.NODES_ENTRY_LENGTH, type);
						DHT.getDHT(type).addDHTNode(e.getAddress().getAddress().getHostAddress(), e.getAddress().getPort());
					}
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see net.redlightning.dht.kad.tasks.Task#canDoRequest()
	 */
	@Override
	boolean canDoRequest() {
		return getNumOutstandingRequestsExcludingStalled() < DHTConstants.MAX_CONCURRENT_REQUESTS * 5;
	}

	/*
	 * (non-Javadoc)
	 * @see net.redlightning.dht.kad.tasks.Task#kill()
	 */
	@Override
	public void kill() {
	// do nothing to evade safeties
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * net.redlightning.dht.kad.tasks.Task#callTimeout(net.redlightning.dht.
	 * kad.RPCCallBase)
	 */
	@Override
	void callTimeout(RPCCallBase c) {

	}

	/*
	 * (non-Javadoc)
	 * @see lbms.plugins.mldht.kad.Task#start()
	 */
	@Override
	public void start() {
		//int added = 0;

		// delay the filling of the todo list until we actually start the task
		for (RoutingTableEntry bucket : node.getBuckets()) {
			for (KBucketEntry e : bucket.getBucket().getEntries()) {
				if (!e.isBad()) {
					todo.add(e);
					//added++;
				}
			}
		}
		super.start();
	}

	/*
	 * (non-Javadoc)
	 * @see net.redlightning.dht.kad.tasks.Task#done()
	 */
	@Override
	protected void done() {
		super.done();
		System.out.println("crawled " + visited.size() + " nodes, seen " + responded.size());
	}
}
