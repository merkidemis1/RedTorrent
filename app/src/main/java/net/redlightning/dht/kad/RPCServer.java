/*
 * This file is part of mlDHT. mlDHT is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or (at your option) any later version. mlDHT is distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details. You should have received a copy of
 * the GNU General Public License along with mlDHT. If not, see
 * <http://www.gnu.org/licenses/>.
 */
package net.redlightning.dht.kad;

import java.io.IOException;
import java.net.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import android.util.Log;
import net.redlightning.dht.kad.messages.MessageBase;
import net.redlightning.dht.kad.messages.MessageDecoder;
import net.redlightning.dht.kad.messages.PingRequest;
import net.redlightning.dht.kad.messages.MessageBase.Type;
import net.redlightning.dht.kad.utils.AddressUtils;
import net.redlightning.dht.kad.utils.ByteWrapper;
import net.redlightning.dht.kad.utils.ResponseTimeoutFilter;
import net.redlightning.dht.kad.utils.ThreadLocalUtils;
import net.redlightning.jbittorrent.BDecoder;

/**
 * @author The_8472, Damokles
 * @author Michael Isaacson (Android Port)
 */
public class RPCServer implements Runnable, RPCServerBase {
	private static final String TAG = RPCServer.class.getSimpleName();
	static Map<InetAddress, RPCServer> interfacesInUse = new HashMap<InetAddress, RPCServer>();
	private BDecoder decoder = new BDecoder(); // we only decode in the listening thread, so reused the decoder
	private DatagramSocket sock;
	private DHT dh_table;
	private ConcurrentMap<ByteWrapper, RPCCallBase> calls;
	private Queue<RPCCallBase> call_queue;
	private volatile boolean running;
	private Thread thread;
	private int numReceived;
	private int numSent;
	private int port;
	private RPCStats stats;
	private ResponseTimeoutFilter timeoutFilter;

	private Key derivedId;

	public RPCServer(DHT dh_table, int port, RPCStats stats) {
		this.port = port;
		this.dh_table = dh_table;
		timeoutFilter = new ResponseTimeoutFilter();
		calls = new ConcurrentHashMap<ByteWrapper, RPCCallBase>(80, 0.75f, 3);
		call_queue = new ConcurrentLinkedQueue<RPCCallBase>();
		this.stats = stats;

		start();
	}

	public DHT getDHT() {
		return dh_table;
	}

	private boolean createSocket() {
		if (sock != null) {
			sock.close();
		}

		synchronized (interfacesInUse) {
			interfacesInUse.values().remove(this);

			InetAddress addr = null;

			try {
				LinkedList<InetAddress> addrs = AddressUtils.getAvailableAddrs(dh_table.getConfig().isMultiHoming(), dh_table.getType().PREFERRED_ADDRESS_TYPE);
				addrs.removeAll(interfacesInUse.keySet());
				addr = addrs.getFirst();

				timeoutFilter.reset();

				if (addr == null)
					throw new NullPointerException("valid address expected");

				sock = new DatagramSocket(null);
				sock.setReuseAddress(true);
				sock.bind(new InetSocketAddress(addr, port));

				interfacesInUse.put(addr, this);
				Log.d(TAG, "Inteface Count: " + interfacesInUse.size());
				return true;
			} catch (Exception e) {
				if (sock != null) {
					sock.close();
				}
				Log.e(TAG, "Error creating Socket");
				destroy();
				return false;
			}
		}

	}

	public int getPort() {
		return port;
	}

	/**
	 * @return external addess, if known (only ipv6 for now)
	 */
	public InetAddress getPublicAddress() {
		if (sock.getLocalAddress() instanceof Inet6Address && !sock.getLocalAddress().isAnyLocalAddress())
			return sock.getLocalAddress();
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	/*
	 * (non-Javadoc)
	 * @see lbms.plugins.mldht.kad.RPCServerBase#run()
	 */
	public void run() {

		int delay = 1;

		byte[] buffer = new byte[DHTConstants.RECEIVE_BUFFER_SIZE];

		while (running) {
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

			try {
				if (sock.isClosed()) { // don't try to receive on a closed socket, attempt to create a new one instead.
					Thread.sleep(delay * 100);
					if (delay < 256)
						delay <<= 1;
					if (createSocket())
						continue;
					else
						break;
				}

				sock.receive(packet);
			} catch (Exception e) {
				if (running) {
					Log.e(TAG, e.getMessage());
					sock.close();
				}
				continue;
			}

			try {
				handlePacket(packet);
				if (delay > 1)
					delay--;
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
				if (running) {
					Log.e(TAG, e.getMessage());
				}
			}

		}
		// we fell out of the loop, make sure everything is cleaned up
		destroy();
		Log.i(TAG, "Stopped RPC Server");
	}

	public Key getDerivedID() {
		return derivedId;
	}

	/*
	 * (non-Javadoc)
	 * @see lbms.plugins.mldht.kad.RPCServerBase#start()
	 */
	public void start() {
		if (!createSocket())
			return;

		running = true;

		Log.i(TAG, "Starting RPC Server");

		// reserve an ID
		derivedId = dh_table.getNode().registerServer(this);

		// make ourselves known once everything is ready
		dh_table.addServer(this);

		// start thread after registering so the DHT can handle incoming packets properly
		thread = new Thread(this, "mlDHT RPC Thread " + dh_table.getType());
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.setDaemon(true);
		thread.start();

	}

	/*
	 * (non-Javadoc)
	 * @see lbms.plugins.mldht.kad.RPCServerBase#stop()
	 */
	public void destroy() {
		if (running) {
			Log.i(TAG, "Stopping RPC Server");
		}
		running = false;
		dh_table.removeServer(this);
		dh_table.getNode().removeServer(this);
		synchronized (interfacesInUse) {
			interfacesInUse.values().remove(this);
		}
		if (sock != null)
			sock.close();
		thread = null;

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * lbms.plugins.mldht.kad.RPCServerBase#doCall(lbms.plugins.mldht.kad.messages
	 * .MessageBase)
	 */
	public RPCCall doCall(MessageBase msg) {

		RPCCall c = new RPCCall(this, msg);

		while (true) {

			if (calls.size() >= DHTConstants.MAX_ACTIVE_CALLS) {
				//Log.i(TAG, "Queueing RPC call, no slots available at the moment");
				call_queue.add(c);
				break;
			}
			short mtid = (short) ThreadLocalUtils.getThreadLocalRandom().nextInt();
			if (calls.putIfAbsent(new ByteWrapper(mtid), c) == null) {
				dispatchCall(c, mtid);
				break;
			}
		}

		return c;
	}

	private final RPCCallListener rpcListener = new RPCCallListener() {
		public void onTimeout(RPCCallBase c) {
			ByteWrapper w = new ByteWrapper(c.getRequest().getMTID());
			stats.addTimeoutMessageToCount(c.getRequest());
			calls.remove(w);
			dh_table.timeout(c);
			doQueuedCalls();
		}

		public void onStall(RPCCallBase c) {}

		public void onResponse(RPCCallBase c, MessageBase rsp) {}
	};

	private void dispatchCall(RPCCallBase call, short mtid) {
		MessageBase msg = call.getRequest();
		msg.setMTID(mtid);
		sendMessage(msg);
		call.addListener(rpcListener);
		timeoutFilter.registerCall(call);
		call.start();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * net.redlightning.dht.kad.RPCServerBase#ping(java.net.InetSocketAddress)
	 */
	public void ping(InetSocketAddress addr) {
		PingRequest pr = new PingRequest();
		pr.setID(derivedId);
		pr.setDestination(addr);
		doCall(pr);
	}

	/*
	 * (non-Javadoc)
	 * @see net.redlightning.dht.kad.RPCServerBase#findCall(byte[])
	 */
	public RPCCallBase findCall(byte[] mtid) {
		return calls.get(new ByteWrapper(mtid));
	}

	/**
	 * @return the number of active calls
	 */
	public int getNumActiveRPCCalls() {
		return calls.size();
	}

	/**
	 * @return the numReceived
	 */
	public int getNumReceived() {
		return numReceived;
	}

	/**
	 * @return the numSent
	 */
	public int getNumSent() {
		return numSent;
	}

	/*
	 * (non-Javadoc)
	 * @see net.redlightning.dht.kad.RPCServerBase#getStats()
	 */
	public RPCStats getStats() {
		return stats;
	}

	/**
	 * @param p
	 */
	@SuppressWarnings("unchecked")
	private void handlePacket(DatagramPacket p) {
		numReceived++;
		stats.addReceivedBytes(p.getLength() + dh_table.getType().HEADER_LENGTH);
		// ignore port 0, can't respond to them anyway and responses to requests from port 0 will be useless too
		if (p.getPort() == 0) {
			return;
		}

		Map<String, Object> bedata = decoder.decodeByteArray(p.getData());
		MessageBase msg = MessageDecoder.parseMessage(bedata, this);

		if (msg != null) {
			//Log.d(TAG, "RPC received message [" + p.getAddress().getHostAddress() + "] " + msg.toString());

			stats.addReceivedMessageToCount(msg);
			msg.setOrigin(new InetSocketAddress(p.getAddress(), p.getPort()));
			msg.setServer(this);
			msg.apply(dh_table);
			// erase an existing call
			if (msg.getType() == Type.RSP_MSG && calls.containsKey(new ByteWrapper(msg.getMTID()))) {
				RPCCallBase c = calls.get(new ByteWrapper(msg.getMTID()));
				if (c.getRequest().getDestination().equals(msg.getOrigin())) {
					// delete the call, but first notify it of the response
					c.response(msg);
					calls.remove(new ByteWrapper(msg.getMTID()));
					doQueuedCalls();
				} else {
					Log.i(TAG, "Response source (" + msg.getOrigin() + ") mismatches request destination (" + c.getRequest().getDestination() + "); ignoring response");
				}
			}
		} else {
			try {
				Log.d(TAG, "RPC received message [" + p.getAddress().getHostAddress() + "] Decode failed msg was:" + new String(p.getData(), 0, p.getLength(), "UTF-8"));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * net.redlightning.dht.kad.RPCServerBase#sendMessage(net.redlightning.dht
	 * .kad.messages.MessageBase)
	 */
	public void sendMessage(MessageBase msg) {
		try {
			if (msg.getID() == null)
				msg.setID(getDerivedID());
			stats.addSentMessageToCount(msg);
			send(msg.getDestination(), msg.encode());
			//Log.d(TAG, "RPC send Message: [" + msg.getDestination().getAddress().getHostAddress() + "] " + msg.toString());
		} catch (IOException e) {
			System.out.print(sock.getLocalAddress() + " -> " + msg.getDestination() + " ");
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public ResponseTimeoutFilter getTimeoutFilter() {
		return timeoutFilter;
	}

	/**
	 * @param addr
	 * @param msg
	 * @throws IOException
	 */
	private void send(InetSocketAddress addr, byte[] msg) throws IOException {
		if (!sock.isClosed()) {
			DatagramPacket p = new DatagramPacket(msg, msg.length);
			p.setSocketAddress(addr);
			try {
				sock.send(p);
			} catch (BindException e) {
				if (NetworkInterface.getByInetAddress(sock.getLocalAddress()) == null) {
					createSocket();
					sock.send(p);
				} else {
					throw e;
				}
			}
			stats.addSentBytes(msg.length + dh_table.getType().HEADER_LENGTH);
			numSent++;
		}
	}

	/**
	 * 
	 */
	private void doQueuedCalls() {
		while (call_queue.peek() != null && calls.size() < DHTConstants.MAX_ACTIVE_CALLS) {
			RPCCallBase c;

			if ((c = call_queue.poll()) == null)
				return;

			short mtid = 0;
			do {
				mtid = (short) ThreadLocalUtils.getThreadLocalRandom().nextInt();
			} while (calls.putIfAbsent(new ByteWrapper(mtid), c) != null);

			dispatchCall(c, mtid);
		}
	}
}
