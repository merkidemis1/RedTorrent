/*
 * This file is part of mlDHT. mlDHT is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or (at your option) any later version. mlDHT is distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details. You should have received a copy of
 * the GNU General Public License along with mlDHT. If not, see
 * <http://www.gnu.org/licenses/>.
 */
package net.redlightning.dht;

import java.io.File;

public class DHTConfiguration {
	private boolean routerBootstrap = true;
	private boolean persistingID = true;
	private boolean multiHoming = true;
	private File nodeCachePath = new File("/sdcard/redtorrent.dht");
	private int listeningPort = 6881;

	/**
	 * @param routerBootstrap the routerBootstrap to set
	 */
	public synchronized void setRouterBootstrap(final boolean routerBootstrap) {
		this.routerBootstrap = routerBootstrap;
	}

	/**
	 * @param persistingID the persistingID to set
	 */
	public synchronized void setPersistingID(final boolean persistingID) {
		this.persistingID = persistingID;
	}

	/**
	 * @param multiHoming the multiHoming to set
	 */
	public synchronized void setMultiHoming(final boolean multiHoming) {
		this.multiHoming = multiHoming;
	}

	/**
	 * @param nodeCachePath the nodeCachePath to set
	 */
	public synchronized void setNodeCachePath(final File nodeCachePath) {
		this.nodeCachePath = nodeCachePath;
	}

	/**
	 * @param listeningPort the listeningPort to set
	 */
	public synchronized void setListeningPort(final int listeningPort) {
		this.listeningPort = listeningPort;
	}

	public boolean isRouterBootstrap() {
		return routerBootstrap;
	}

	public boolean isPersistingID() {
		return persistingID;
	}

	public File getNodeCachePath() {
		return nodeCachePath;
	}

	public int getListeningPort() {
		return listeningPort;
	}

	public boolean isMultiHoming() {
		return multiHoming;
	}
}
