/*
 * Java Bittorrent API as its name indicates is a JAVA API that implements the
 * Bittorrent Protocol This project contains two packages: 1. jBittorrentAPI is
 * the "client" part, i.e. it implements all classes needed to publish files,
 * share them and download them. This package also contains example classes on
 * how a developer could create new applications. 2. trackerBT is the "tracker"
 * part, i.e. it implements a all classes needed to run a Bittorrent tracker
 * that coordinates peers exchanges. * Copyright (C) 2007 Baptiste Dubuis,
 * Artificial Intelligence Laboratory, EPFL This file is part of
 * jbittorrentapi-v1.0.zip Java Bittorrent API is free software and a free user
 * study set-up; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version. Java
 * Bittorrent API is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with Java Bittorrent API; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * @version 1.0
 * @author Baptiste Dubuis To contact the author: email:
 * baptiste.dubuis@gmail.com More information about Java Bittorrent API:
 * http://sourceforge.net/projects/bitext/
 */

package net.redlightning.jbittorrent;

import java.net.UnknownHostException;
import java.io.InputStream;
import java.io.IOException;
import java.io.BufferedInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import android.util.Log;
import net.redlightning.jbittorrent.udp.UDPTracker;
import net.redlightning.redtorrent.common.TorrentService;

/**
 * Class providing methods to enable communication between the client and a
 * tracker. Provide method to decode and parse tracker response.
 * 
 * @author Baptiste Dubuis (baptiste.dubuis@gmail.com)
 * @author Michael Isaacson (michael@redlightning.net)
 * @version 11.11.15
 */
public class PeerUpdater extends Thread {
	private static final String TAG = PeerUpdater.class.getSimpleName();
	private transient TorrentFile torrent;
	private transient DownloadManager manager;
	private String event = "&event=started";
	private transient int listeningPort = 6881;
	private int interval = 150;
	private boolean connected = false;

	/**
	 * Constructor
	 * 
	 * @param manager the DownloadManager handling the downloading of this torrent
	 */
	protected PeerUpdater(final DownloadManager manager) {
		super("Peer Updater");
		this.manager = manager;
		this.torrent = manager.getTorrent();
		this.setDaemon(true);
	}

	/**
	 * Contact the tracker according to the HTTP/HTTPS tracker protocol and
	 * using the information in the TorrentFile.
	 * 
	 * @param id byte[]
	 * @param t TorrentFile
	 * @param dl long
	 * @param ul long
	 * @param left long
	 * @param event String
	 * @return A Map containing the decoded tracker response
	 */
	@SuppressWarnings("rawtypes")
	private Map contactTracker(final int trackerID, final String event) {
		URLConnection uc = null;
		InputStream inputStream = null;
		BufferedInputStream bis = null;
		URL source = null;

		if (!torrent.getStatus().equalsIgnoreCase("Tracker error")) {
			TorrentService.getHandler().dispatchMessage(TorrentService.getHandler().obtainMessage(TorrentService.CONNECTING, torrent.getTorrentFileName()));
			try {
				//Log.w(TAG, "Tracker: " + torrent.getAnnounceURLs().get(trackerID));
				if (torrent.getAnnounceURLs().get(trackerID).startsWith("udp")) {
					//Handle UDP trackers
					String trackerURL = torrent.getAnnounceURLs().get(trackerID);
					String port = trackerURL.substring(trackerURL.lastIndexOf(":") + 1);
					if (port.contains("/")) {
						port = port.substring(0, port.indexOf('/'));
					}
					trackerURL = trackerURL.substring(6, trackerURL.lastIndexOf(":"));

					final UDPTracker client = new UDPTracker(torrent);
					final Map<String, String> response = client.getTrackerInfo(trackerURL, Integer.parseInt(port));

					if (response != null && manager.isActive()) {
						torrent.setErrorMessage(null);
						torrent.setStatus("Connecting to Peers...");
						setConnected(true);
						TorrentService.getHandler().dispatchMessage(TorrentService.getHandler().obtainMessage(TorrentService.DOWNLOADING, torrent.getTorrentFileName()));
						Log.i(TAG, "Connected to " + torrent.getAnnounceURLs().get(trackerID).toString() + "(" + response.size() + ")");
					} else if (response == null && manager.isActive()) {
						TorrentService.getHandler().dispatchMessage(TorrentService.getHandler().obtainMessage(TorrentService.ERROR, torrent.getTorrentFileName()
								+ ",Tracker " + (trackerID + 1) + "/" + torrent.getAnnounceURLs().size() + " - " + torrent.getAnnounceURLs().get(trackerID)
								+ " is unreachable"));
						Log.w(TAG, "Tracker unreachable... Retrying " + "due to : Connection Timeout");
					}
					return response;
				} else if (torrent.getAnnounceURLs().get(trackerID).startsWith("http")) {
					//Handle HTTP trackers
					source = new URL(torrent.getAnnounceURLs().get(trackerID) + "?info_hash=" + torrent.getInfoHashAsURL() + "&peer_id="
							+ Utils.toURLString(torrent.getPeerID()) + "&port=" + this.listeningPort + "&downloaded=" + torrent.getDownloaded() + "&uploaded="
							+ torrent.getUploaded() + "&left=" + torrent.getLeft() + "&compact=1" + event);
					uc = source.openConnection();
					inputStream = uc.getInputStream();
					bis = new BufferedInputStream(inputStream);

					// Decode the tracker bencoded response
					final Map response = BDecoder.decode(bis);
					bis.close();
					inputStream.close();
					if (manager.isActive()) {
						torrent.setStatus("Connecting to Peers...");
						setConnected(true);
						TorrentService.getHandler().dispatchMessage(TorrentService.getHandler().obtainMessage(TorrentService.DOWNLOADING, torrent.getTorrentFileName()));
						Log.i(TAG, "Connected to " + source.toString());
					}
					return response;
				}
			} catch (final MalformedURLException murle) {
				Log.e(TAG, "Tracker URL is not valid... Check if your data is correct and try again");
				TorrentService.getHandler().dispatchMessage(TorrentService.getHandler().obtainMessage(TorrentService.ERROR, torrent.getTorrentFileName() + ",Tracker "
						+ (trackerID + 1) + "/" + torrent.getAnnounceURLs().size() + " - " + torrent.getAnnounceURLs().get(trackerID) + " is not a valid URL"));
			} catch (final UnknownHostException uhe) {
				Log.w(TAG, "Tracker not available... Retrying...");
				TorrentService.getHandler().dispatchMessage(TorrentService.getHandler().obtainMessage(TorrentService.ERROR, torrent.getTorrentFileName() + ",Tracker "
						+ (trackerID + 1) + "/" + torrent.getAnnounceURLs().size() + " - " + torrent.getAnnounceURLs().get(trackerID) + " is not available"));
			} catch (final IOException ioe) {
				TorrentService.getHandler().dispatchMessage(TorrentService.getHandler().obtainMessage(TorrentService.ERROR, torrent.getTorrentFileName() + ",Tracker "
						+ (trackerID + 1) + "/" + torrent.getAnnounceURLs().size() + " - " + torrent.getAnnounceURLs().get(trackerID) + " is unreachable"));
				Log.w(TAG, "Tracker unreachable... Retrying due to : " + ioe.getMessage());
			} catch (final Exception e) {
				e.printStackTrace();
				Log.e(TAG, "Tracker URL: " + torrent.getAnnounceURLs().get(trackerID));
				Log.e(TAG, "Internal error: " + e.getMessage());
			}
		}
		return null;
	}

	/**
	 * @return int the current event of the client
	 */
	public synchronized String getEvent() {
		return this.event;
	}

	/**
	 * @return int the last interval for updates received from the tracker
	 */
	public synchronized int getInterval() {
		return this.interval;
	}

	/**
	 * @return true if the peer is connected, false if not
	 */
	public synchronized boolean isConnected() {
		return connected;
	}

	/**
	 * Process the map representing The tracker response, which should contain
	 * either an error message or the peers list and other information such as
	 * the interval before next update, aso
	 * 
	 * @param response The tracker response as a Map
	 * @return LinkedHashMap A HashMap containing the peers and their ID as keys
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private synchronized LinkedHashMap<String, Peer> processResponse(Map response) {
		LinkedHashMap<String, Peer> peerMap = null;

		if (response != null && manager.isActive()) {
			if (response.containsKey("udp")) {
				return processUDPResponse(response);
			} else if (response.containsKey("failure reason")) {
				Log.w(TAG, "The tracker returns the following error message:" + "\t'" + new String((byte[]) response.get("failure reason")) + "'");
				return null;
			} else {
				if (((Long) response.get("interval")).intValue() < interval) {
					interval = ((Long) response.get("interval")).intValue();
				} else {
					interval *= 2;
				}

				final Object peers = response.get("peers");
				final ArrayList peerList = new ArrayList();
				peerMap = new LinkedHashMap<String, Peer>();
				if (peers instanceof List) {
					peerList.addAll((List) peers);
					if (peerList != null && peerList.size() > 0) {
						for (int i = 0; i < peerList.size(); i++) {
							final String ipAddress = new String((byte[]) ((Map) (peerList.get(i))).get("ip"));
							final int port = ((Long) ((Map) (peerList.get(i))).get("port")).intValue();
							final Peer peer = new Peer(ipAddress, port);
							peerMap.put(peer.toString(), peer);
							Log.i(TAG, "Adding peer " + peer.toString());
						}
					}
				} else if (peers instanceof byte[]) {
					final byte[] p = ((byte[]) peers);
					for (int i = 0; i < p.length; i += 6) {
						final String ip = Utils.toUnsignedInt(p[i]) + "." + Utils.toUnsignedInt(p[i + 1]) + "." + Utils.toUnsignedInt(p[i + 2]) + "."
								+ Utils.toUnsignedInt(p[i + 3]);
						int port = Utils.toInt(Utils.subArray(p, i + 4, 2));
						if (port != -1) {
							final Peer peer = new Peer(ip, port);
							peerMap.put(peer.toString(), peer);
							Log.i(TAG, "Adding peer " + peer.toString());
						}
					}
				}
			}

			//Log.i(TAG, "Found " + peerMap.size() + " peers for " + torrent.getTorrentFileName());
			return peerMap;
		} else {
			//Log.i(TAG, "No peers found for " + torrent.getTorrentFileName());
			return null;
		}
	}

	/**
	 * Process the map representing the response from the tracker. Should be a
	 * list of seeders and leechers, as well as some additional info
	 * 
	 * @param response The tracker response as a Map
	 * @return LinkedHashMap A HashMap containing the peers and their ID as keys
	 */
	private synchronized LinkedHashMap<String, Peer> processUDPResponse(Map<String, String> response) {
		final LinkedHashMap<String, Peer> peerList = new LinkedHashMap<String, Peer>();
		if (response != null) {
			final Iterator<String> itr = response.keySet().iterator();
			while (itr.hasNext()) {
				final String key = itr.next();
				if ("interval".equals(key)) {
					interval = Integer.parseInt(response.get(key));
				} else if ("seeders".equals(key)) {
					//TODO Populate Seeders display
				} else if ("leechers".equals(key)) {
					//TODO Populate Leechers display
				} else if (!"udp".equals(key)) {
					final Peer peer = new Peer(key, Integer.parseInt(response.get(key)));
					peerList.put(peer.toString(), peer);
					Log.i(TAG, "Adding peer " + peer.toString());
				}
			}
			return peerList;
		} else {
			return null;
		}
	}

	/**
	 * Thread method that regularly contact the tracker and process its response
	 */
	public void run() {
		setName("Peer Updater");

		final byte[] b = new byte[0];

		while (manager.isActive()) {
			synchronized (b) {
				//Log.i(TAG, "Contacting " + torrent.getAnnounceURLs().size() + " trackers...");
				for (int tracker = 0; tracker < torrent.getAnnounceURLs().size(); tracker++) {
					final LinkedHashMap<String, Peer> peerList = processResponse(contactTracker(tracker, event));
					if (peerList != null) {
						manager.getPeerList().putAll(peerList);
					}
				}
				try {
					for (int i = interval; i > 0; i--) {
						torrent.setNote(" (Tracker refresh: " + i + " secs)");
						b.wait(1000);
					}
				} catch (InterruptedException ie) {
					Log.i(TAG, "Tracker update thread interrupted");
				}
				torrent.setNote(null);
			}
		}
	}

	/**
	 * @param connected true if the peer is connected, false if not
	 */
	public synchronized void setConnected(boolean connected) {
		this.connected = connected;
	}

	/**
	 * @param event the current state of the client
	 */
	public synchronized void setEvent(String event) {
		this.event = event;
	}

	/**
	 * @param interval the interval between tracker update
	 */
	public synchronized void setInterval(int interval) {
		this.interval = interval;
	}

	/**
	 * @param port
	 */
	public synchronized void setListeningPort(int port) {
		this.listeningPort = port;
	}

	/**
	 * Update the parameters for future tracker communication
	 * 
	 * @param dl the number of bytes downloaded
	 * @param ul the number of bytes uploaded
	 * @param event current peer event
	 */
	protected synchronized void updateParameters(int dl, int ul, String event) {
		torrent.setDownloaded(torrent.getDownloaded() + dl);
		torrent.setUploaded(torrent.getUploaded() + ul);
		torrent.setLeft(torrent.getLeft() - dl);
		setEvent(event);
	}
}
