/******************************************************************************
 * Copyright (c) 1998,99 by Mindbright Technology AB, Stockholm, Sweden.
 * www.mindbright.se, info@mindbright.se This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 */
package net.redlightning.jbittorrent;

/**
 * Encrypt and decrypt a string using the RC4 cipher
 * 
 * @author Mindbright Technology (Original implementation)
 * @author Clarence Ho (RC4 methods)
 * @author Michael Isaacson (michael@redlightning.net)
 * @version 11.3.3
 */
public final class RC4 {
	char[] dh_prime = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xC9, 0x0F, 0xDA, 0xA2, 0x21, 0x68, 0xC2, 0x34, 0xC4, 0xC6, 0x62, 0x8B, 0x80, 0xDC, 0x1C, 0xD1, 0x29, 0x02, 0x4E, 0x08, 0x8A, 0x67, 0xCC, 0x74, 0x02, 0x0B, 0xBE, 0xA6, 0x3B, 0x13, 0x9B, 0x22, 0x51, 0x4A, 0x08, 0x79, 0x8E, 0x34, 0x04, 0xDD, 0xEF, 0x95, 0x19, 0xB3, 0xCD, 0x3A, 0x43, 0x1B, 0x30, 0x2B, 0x0A, 0x6D, 0xF2, 0x5F, 0x14, 0x37, 0x4F, 0xE1, 0x35, 0x6D, 0x6D, 0x51, 0xC2, 0x45, 0xE4, 0x85, 0xB5, 0x76, 0x62, 0x5E, 0x7E, 0xC6, 0xF4, 0x4C, 0x42, 0xE9, 0xA6, 0x3A, 0x36, 0x21, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 0x05, 0x63 };

	private int x;
	private int y;
	private byte[] state = new byte[256];

	/**
	 * @return
	 */
	final int arcfour_byte() {
		int x;
		int y;
		int sx, sy;

		x = (this.x + 1) & 0xff;
		sx = (int) state[x];
		y = (sx + this.y) & 0xff;
		sy = (int) state[y];
		this.x = x;
		this.y = y;
		state[y] = (byte) (sx & 0xff);
		state[x] = (byte) (sy & 0xff);
		return (int) state[((sx + sy) & 0xff)];
	}

	/**
	 * @param src
	 * @param srcOff
	 * @param dest
	 * @param destOff
	 * @param len
	 */
	public synchronized void encrypt(byte[] src, int srcOff, byte[] dest, int destOff, int len) {
		int end = srcOff + len;
		for (int si = srcOff, di = destOff; si < end; si++, di++) {
			dest[di] = (byte) (((int) src[si] ^ arcfour_byte()) & 0xff);
		}
	}

	/**
	 * @param src
	 * @param srcOff
	 * @param dest
	 * @param destOff
	 * @param len
	 */
	public void decrypt(byte[] src, int srcOff, byte[] dest, int destOff, int len) {
		encrypt(src, srcOff, dest, destOff, len);
	}

	/**
	 * @param key
	 */
	public void setKey(byte[] key) {
		int t, u;
		int keyindex = 0;
		int stateindex = 0;

		for (int counter = 0; counter < 256; counter++) {
			state[counter] = (byte) counter;
		}

		for (int counter = 0; counter < 256; counter++) {
			t = (int) state[counter];
			stateindex = (stateindex + key[keyindex] + t) & 0xff;
			u = (int) state[stateindex];
			state[stateindex] = (byte) (t & 0xff);
			state[counter] = (byte) (u & 0xff);
			if (++keyindex >= key.length) {
				keyindex = 0;
			}
		}
	}

	/**
	 * Initializes the class with a string key. The length of a normal key
	 * should be between 1 and 2048 bits. But this method doens't check the
	 * length at all.
	 * 
	 * @param key the encryption/decryption key
	 */
	public RC4(String key) throws NullPointerException {
		this(key.getBytes());
	}

	/**
	 * Initializes the class with a byte array key. The length of a normal key
	 * should be between 1 and 2048 bits. But this method doens't check the
	 * length at all.
	 * 
	 * @param key the encryption/decryption key
	 */
	public RC4(byte[] key) throws NullPointerException {
		for (int counter = 0; counter < 256; counter++) {
			state[counter] = (byte) counter;
		}

		x = 0;
		y = 0;

		int index1 = 0;
		int index2 = 0;

		byte tmp;

		if (key == null || key.length == 0) {
			throw new NullPointerException();
		}

		for (int i = 0; i < 256; i++) {
			index2 = ((key[index1] & 0xff) + (state[i] & 0xff) + index2) & 0xff;
			tmp = state[i];
			state[i] = state[index2];
			state[index2] = tmp;
			index1 = (index1 + 1) % key.length;
		}

	}

	/**
	 * RC4 encryption/decryption.
	 * 
	 * @param data the data to be encrypted/decrypted
	 * @return the result of the encryption/decryption
	 */
	public byte[] rc4(String data) {
		if (data == null) {
			return null;
		}

		return this.rc4(data.getBytes());
	}

	/**
	 * RC4 encryption/decryption.
	 * 
	 * @param buffer the data to be encrypted/decrypted
	 * @return the result of the encryption/decryption
	 */
	public byte[] rc4(byte[] buffer) {
		int xorIndex;
		byte tmp;

		if (buffer == null) {
			return null;
		}

		byte[] result = new byte[buffer.length];

		for (int i = 0; i < buffer.length; i++) {

			x = (x + 1) & 0xff;
			y = ((state[x] & 0xff) + y) & 0xff;

			tmp = state[x];
			state[x] = state[y];
			state[y] = tmp;

			xorIndex = ((state[x] & 0xff) + (state[y] & 0xff)) & 0xff;
			result[i] = (byte) (buffer[i] ^ state[xorIndex]);
		}

		return result;
	}
}
