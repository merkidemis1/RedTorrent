/**
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.jbittorrent.udp;

import java.net.*;
import java.util.Hashtable;
import java.util.Map;
import java.util.Random;
import android.util.Log;
import net.redlightning.jbittorrent.TorrentFile;
import net.redlightning.jbittorrent.Utils;
import net.redlightning.redtorrent.common.Settings;

/**
 * Handles interacting with UDP BitTorrent Trackers
 * 
 * @author Michael Isaacson
 * @version 11.9.29
 */
public class UDPTracker {
	public static final String TAG = UDPTracker.class.getSimpleName();
	private final static int MAX_NUM_WANT = 50;
	public static final int CONNECT = 0;
	public static final int ANNOUNCE = 1;
	public static final int SCRAPE = 2;
	public static final int ERROR = 3;
	public static final int NONE = 0;
	public static final int COMPLETED = 1;
	public static final int STARTED = 2;
	public static final int STOPPED = 3;
	public static final int INT_MAX = 2147483647;
	public int port = 80;
	public int buffer_size = 1024;
	public byte buffer[] = new byte[buffer_size];
	public long connectionID = 0x41727101980L;
	public int action = 0;
	public InetAddress host;
	private Random generator = new Random();
	private TorrentFile torrent;

	/**
	 * Constructor
	 * 
	 * @param file the torrent we'll be working with and storing data into
	 */
	public UDPTracker(final TorrentFile file) {
		torrent = file;
	}

	/**
	 * @return a map of data containing peer ip address, interval, seeder and
	 *         leecher counts
	 */
	public Map<String, String> announce(final int event) {
		Log.i(TAG, "Announcing");
		//Request (98 byte)
		//	0       64-bit integer  connection_id
		//	8       32-bit integer  action          1 // announce
		//	12      32-bit integer  transactionID
		//	16      20-byte string  info_hash
		//	36      20-byte string  peer_id
		//	56      64-bit integer  downloaded
		//	64      64-bit integer  left
		//	72      64-bit integer  uploaded
		//	80      32-bit integer  event           0 // 0: none; 1: completed; 2: started; 3: stopped
		//	84      32-bit integer  IP address      0 // default
		//	88      32-bit integer  key
		//	92      32-bit integer  num_want        -1 // default
		//	96      16-bit integer  port

		//	Do not announce again until interval seconds have passed or an event has occurred.

		//	Response:
		//	0           32-bit integer  action          1 // announce
		//	4           32-bit integer  transactionID
		//	8           32-bit integer  interval
		//	12          32-bit integer  leechers
		//	16          32-bit integer  seeders
		//	20 + 6 * n  32-bit integer  IP address
		//	24 + 6 * n  16-bit integer  TCP port
		//	20 + 6 * N

		final int transactionID = getGenerator().nextInt(INT_MAX);
		Map<String, String> info = new Hashtable<String, String>();
		//Log.i(TAG, "Announce");
		setBuffer(Utils.concat(Utils.toArray(getConnectionID()), Utils.toArray(ANNOUNCE)));
		setBuffer(Utils.concat(getBuffer(), Utils.toArray(transactionID)));
		setBuffer(Utils.concat(getBuffer(), getTorrent().getInfoHashAsBinary()));
		setBuffer(Utils.concat(getBuffer(), getTorrent().getPeerID()));
		setBuffer(Utils.concat(getBuffer(), Utils.toArray(getTorrent().getDownloaded())));
		setBuffer(Utils.concat(getBuffer(), Utils.toArray(getTorrent().getLeft())));
		setBuffer(Utils.concat(getBuffer(), Utils.toArray(getTorrent().getUploaded())));
		setBuffer(Utils.concat(getBuffer(), Utils.toArray(event))); //event
		setBuffer(Utils.concat(getBuffer(), Utils.toArray(0))); //IP address
		setBuffer(Utils.concat(getBuffer(), Utils.toArray(getGenerator().nextInt(INT_MAX)))); //key
		setBuffer(Utils.concat(getBuffer(), Utils.toArray(MAX_NUM_WANT))); //num want
		setBuffer(Utils.concat(getBuffer(), Utils.toArray(Settings.getLowPort())));

		//Log.e(TAG, "Setting up UDP Response buffer (10000)...");
		UDP udp = new UDP();
		final byte[] responseData = udp.getData(getHost(), getPort(), getBuffer(), 10000);
		//Log.e(TAG, "Buffer set");

		boolean goodResponse = false;
		if (responseData != null) {
			for (final byte element : responseData) {
				if (element != 0) {
					goodResponse = true;
				}
			}
		}

		if (goodResponse) {
			try {
				final int resAction = Utils.toInt(Utils.subArray(responseData, 0, 4));
				final int resTrans = Utils.toInt(Utils.subArray(responseData, 4, 4));
				final int interval = Utils.toInt(Utils.subArray(responseData, 8, 4));
				final int leechers = Utils.toInt(Utils.subArray(responseData, 12, 4));
				final int seeders = Utils.toInt(Utils.subArray(responseData, 16, 4));

				if ((resAction != ERROR) && (resTrans == transactionID) && (resAction == ANNOUNCE)) {
					//Log.i(TAG, "ANNOUNCE: A:" + resAction + " T: " + resTrans + " (" + transactionID + ") I: " + interval + " P: " + seeders + "/" + leechers);
					info.put("udp", "tracker");
					info.put("seeders", String.valueOf(seeders));
					info.put("leechers", String.valueOf(leechers));
					info.put("interval", String.valueOf(interval));
					try {
						for (int peer = 0; peer < (leechers + seeders); peer++) {
							final String ip = Utils.intToIp(Utils.toInt(Utils.subArray(responseData, (20 + (6 * peer)), 4)));
							final int port = Utils.toChar(Utils.subArray(responseData, (24 + (6 * peer)), 2));

							if (port != 0) {
								info.put(ip, String.valueOf(port));
							}
						}
					} catch (final ArrayIndexOutOfBoundsException e) {
						Log.w(TAG, "Too many peers returned, some were dropped");
					}
				} else if (resAction == ERROR) {
					error(responseData);
					info = null;
				} else {
					getTorrent().setErrorMessage("Unable to announce, invalid request");
					//Log.i(TAG, "ANNOUNCE-E: A:" + resAction + " T: " + resTrans + " (" + transactionID + ") I: " + interval + " P: " + seeders + "/" + leechers);
					info = null;
				}
			} catch (final Exception e) {
				getTorrent().setErrorMessage("Unable to announce with tracker " + host);
				Log.e(TAG, "ANNOUCE-EX: " + e.getClass().getSimpleName() + " - " + e.getMessage());
				info = null;
			}
		}

		return info;
	}

	/**
	 * Perform the Connect action
	 * 
	 * @return true if a successful connection was established and we received a
	 *         new connection id
	 */
	public boolean connect() {
		//Log.e(TAG, "Connecting");
		//Request (16 byte):
		//0		64-bit integer  connection_id   0x41727101980
		//8		32-bit integer  action          0 // connect
		//12	32-bit integer  transactionID	Random

		//Response (16 byte):
		//	0       32-bit integer  action          0 // connect
		//	4       32-bit integer  transactionID
		//	8       64-bit integer  connection_id
		boolean connected = false;
		final int transactionID = getGenerator().nextInt(INT_MAX);
		//Log.e(TAG, "Buffer 1");
		setBuffer(Utils.concat(Utils.toArray(getConnectionID()), Utils.toArray(CONNECT)));
		//Log.e(TAG, "Buffer 2");
		setBuffer(Utils.concat(getBuffer(), Utils.toArray(transactionID)));
		//Log.e(TAG, "Getting connect response");
		//Log.e(TAG, "Request: " + Utils.toString(getBuffer()));
		UDP udp = new UDP();
		final byte[] responseData = udp.getData(getHost(), getPort(),getBuffer(), 32);

		boolean found = false;
		if (responseData == null) {
			connected = false;
			//Log.e(TAG, "Connect Response data is null");
		} else {
			for (final byte element : responseData) {
				if (element != 0) {
					found = true;
					break;
				}
			}

			if (found) {
				try {
					final int resAction = Utils.toInt(Utils.subArray(responseData, 0, 4));
					final int resTrans = Utils.toInt(Utils.subArray(responseData, 4, 4));
					final long resConn = Utils.toLong(Utils.subArray(responseData, 8, 8));

					if ((resAction != ERROR) && (resTrans == transactionID)) {
						setConnectionID(resConn);
						connected = true;
					} else if (resAction == ERROR) {
						error(responseData);
						connected = false;
					} else {
						//Log.i(TAG, "CONNECT E: A:" + resAction + " T: " + resTrans + " (" + transactionID + ") C: " + resConn);
						connected = false;
					}
				} catch (final Exception e) {
					getTorrent().setErrorMessage("Unable to connect to tracker");
					Log.e(TAG, e.getMessage());
					connected = false;
				}
			} else {
				Log.e(TAG, "Connect Repsonse data has null characters");
			}
		}
		return connected;
	}

	/**
	 * @return the error message pulled from the given byte array
	 */
	public String error(final byte[] data) {
		//	0       32-bit integer  action          3 // error
		//	4       32-bit integer  transactionID
		//	8       string  message
		//Log.e(TAG, Utils.toString(Utils.subArray(data, 8, 4)));
		getTorrent().setErrorMessage(Utils.toString(Utils.subArray(data, 8, 4)));
		return Utils.toString(Utils.subArray(data, 8, 4));
	}

	/**
	 * @return the action
	 */
	public synchronized int getAction() {
		return action;
	}

	/**
	 * @return the buffer
	 */
	public synchronized byte[] getBuffer() {
		return buffer;
	}

	/**
	 * @return the connectionID
	 */
	public synchronized long getConnectionID() {
		return connectionID;
	}

	/**
	 * @return the generator
	 */
	public synchronized Random getGenerator() {
		return generator;
	}

	/**
	 * @return the host
	 */
	public synchronized InetAddress getHost() {
		return host;
	}

	/**
	 * @return the port
	 */
	public synchronized int getPort() {
		return port;
	}

	/**
	 * @return the torrent
	 */
	public synchronized TorrentFile getTorrent() {
		return torrent;
	}

	/**
	 * @param hostURI
	 * @param portNum
	 * @return
	 */
	public Map<String, String> getTrackerInfo(final String hostURI) {
		String trackerURL = hostURI;
		String port = trackerURL.substring(trackerURL.lastIndexOf(':') + 1);
		
		if(port.contains("/")) {
			port = port.substring(0, port.indexOf('/'));
		}
		int portNum = Integer.valueOf(port);
		trackerURL = trackerURL.substring(6, trackerURL.lastIndexOf(':'));
		return getTrackerInfo(trackerURL, portNum, STARTED);
	}
	
	/**
	 * @param hostURI
	 * @param portNum
	 * @return
	 */
	public Map<String, String> getTrackerInfo(final String hostURI, final int portNum) {
		return getTrackerInfo(hostURI, portNum, STARTED);
	}

	/**
	 * @param hostURI the URL of the host (minus any paths or protocol info, ie:
	 *            torrent.host.com)
	 * @param portNum the port number to interact with
	 * @param event the action to announce (0: none; 1: completed; 2: started;
	 *            3: stopped)
	 */
	public Map<String, String> getTrackerInfo(final String hostURI, final int portNum, final int event) {
		setPort(portNum);
		try {
			setHost(InetAddress.getByName(hostURI));
		} catch (final UnknownHostException e) {
			getTorrent().setErrorMessage("Unknown Host: " + hostURI + ":" + portNum);
			Log.e(TAG, e.getMessage());
			return null;
		}
		if(!host.isLoopbackAddress() && connect()) {
			return announce(event);
		} else {
			return null;
		}
	}

	/**
	 * @return
	 */
	public boolean scrape() {
		//  Request (16 + 20 * N) where N is the number of 20 byte info_hashes
		//	0               64-bit integer  connection_id
		//	8               32-bit integer  action          2 // scrape
		//	12              32-bit integer  transactionID
		//	16 + 20 * n     20-byte string  info_hash
		//
		//	Response (8 + 12 * N) where N is the number of 20 byte info_hashes
		//	0           32-bit integer  action          2 // scrape
		//	4           32-bit integer  transactionID
		//	8 + 12 * n  32-bit integer  seeders
		//	12 + 12 * n 32-bit integer  completed
		//	16 + 12 * n 32-bit integer  leechers
		boolean connected = false;
		final int transactionID = getGenerator().nextInt(INT_MAX);

		setBuffer(Utils.concat(Utils.toArray(getConnectionID()), Utils.toArray(SCRAPE)));
		setBuffer(Utils.concat(getBuffer(), Utils.toArray(transactionID)));
		setBuffer(Utils.concat(getBuffer(), getTorrent().getInfoHashAsBinary()));

		//Log.e(TAG, "Setting up UDP Response buffer (8192)...");
		UDP udp = new UDP();
		final byte[] responseData = udp.getData(getHost(), getPort(), getBuffer(), 8192);
		//Log.e(TAG, "Buffer set");

		boolean found = false;
		for (final byte element : responseData) {
			if (element != 0) {
				found = true;
			}
		}

		if (found) {
			try {
				final int resAction = Utils.toInt(Utils.subArray(responseData, 0, 4));
				final int resTrans = Utils.toInt(Utils.subArray(responseData, 4, 4));
				//int seeders = Utils.toInt(Utils.subArray(responseData, 8, 4));
				//int completed = Utils.toInt(Utils.subArray(responseData, 12, 4));
				//int leechers = Utils.toInt(Utils.subArray(responseData, 16, 4));

				if ((resAction != ERROR) && (resTrans == transactionID) && (resAction == SCRAPE)) {
					//Log.i(TAG, "RES: A:" + resAction + " T: " + resTrans + " (" + transactionID + ") P: " + seeders + "/" + leechers + " C: " + completed);
					connected = true;
				} else if (resAction == ERROR) {
					error(responseData);
				} else {
					getTorrent().setErrorMessage("Unable to scape data, invalid info hash?");
					//Log.i(TAG, "RES-E: A:" + resAction + " T: " + resTrans + " (" + transactionID + ") P: " + seeders + "/" + leechers + " C: " + completed);
				}
			} catch (final Exception e) {
				getTorrent().setErrorMessage("Unable to scrape data");
				Log.e(TAG, e.getMessage());
			}
		}

		return connected;
	}

	/**
	 * @param action the action to set
	 */
	public synchronized void setAction(final int action) {
		this.action = action;
	}

	/**
	 * @param buffer the buffer to set
	 */
	public synchronized void setBuffer(final byte[] buffer) {
		this.buffer = buffer;
	}

	/**
	 * @param connectionID the connectionID to set
	 */
	public synchronized void setConnectionID(final long connectionID) {
		this.connectionID = connectionID;
	}

	/**
	 * @param generator the generator to set
	 */
	public synchronized void setGenerator(final Random generator) {
		this.generator = generator;
	}

	/**
	 * @param host the host to set
	 */
	public synchronized void setHost(final InetAddress host) {
		this.host = host;
	}

	/**
	 * @param port the port to set
	 */
	public synchronized void setPort(final int port) {
		this.port = port;
	}

	/**
	 * @param torrent the torrent to set
	 */
	public synchronized void setTorrent(final TorrentFile torrent) {
		this.torrent = torrent;
	}
}