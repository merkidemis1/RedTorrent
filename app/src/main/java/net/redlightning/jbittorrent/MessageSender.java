/*
 * Java Bittorrent API as its name indicates is a JAVA API that implements the
 * Bittorrent Protocol This project contains two packages: 1. jBittorrentAPI is
 * the "client" part, i.e. it implements all classes needed to publish files,
 * share them and download them. This package also contains example classes on
 * how a developer could create new applications. 2. trackerBT is the "tracker"
 * part, i.e. it implements a all classes needed to run a Bittorrent tracker
 * that coordinates peers exchanges. * Copyright (C) 2007 Baptiste Dubuis,
 * Artificial Intelligence Laboratory, EPFL This file is part of
 * jbittorrentapi-v1.0.zip Java Bittorrent API is free software and a free user
 * study set-up; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version. Java
 * Bittorrent API is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with Java Bittorrent API; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * @version 1.0
 * @author Baptiste Dubuis To contact the author: email:
 * baptiste.dubuis@gmail.com More information about Java Bittorrent API:
 * http://sourceforge.net/projects/bitext/
 */

package net.redlightning.jbittorrent;

import java.io.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.LinkedBlockingQueue;
import java.io.OutputStream;
import net.redlightning.jbittorrent.interfaces.AbstractMessage;

/**
 * Thread created to send message to the remote peer. Hold a queue for outgoing
 * messages
 * 
 * @author Baptiste Dubuis (baptiste.dubuis@gmail.com)
 * @author Michael Isaacson (michael@redlightning.net)
 * @version 0.1
 */
public class MessageSender extends Thread {
	private OutputStream output = null;
	private LinkedBlockingQueue<AbstractMessage> outgoingMessage = new LinkedBlockingQueue<AbstractMessage>();
	private boolean running = true;
	private final DownloadTask task;

	/**
	 * 
	 * @param id
	 * @param stream
	 */
	protected MessageSender(final DownloadTask task, final String id, final OutputStream stream) {
		super("MessageSender");
		output = stream;
		this.task = task;
	}

	/**
	 * Called when the connection to the peer has been closed. Advertise the
	 * DownloadTask that there is no more connection to the remote peer.
	 */
	private void fireConnectionClosed() {
		task.connectionClosed();
	}

	/**
	 * Called when a keep-alive message has been sent. This happens about every
	 * 2 minutes if there has not been any other messages sent to avoid the
	 * connection to be closed by the remote peer
	 */
	private void fireKeepAliveSent() {
		task.keepAliveSent();
	}

	/**
	 * Puts the message in parameter in the queue, waiting to be sent
	 * 
	 * @param m Message
	 */
	protected synchronized void addMessageToQueue(final AbstractMessage m) {
		outgoingMessage.add(m);
	}

	/**
	 * Sends messages from the queue. While the queue is not empty, polls
	 * message from it and sends it to the remote peer. If the queue is empty
	 * for more than 2 minutes, a keep-alive message is sent and the
	 * DownloadTask is advertised
	 */
	@Override
	public void run() {
		AbstractMessage out = null;
		final byte[] keep = new PeerProtocolMessage(Peer.KEEP_ALIVE).generate();
		try {
			while (isRunning()) {
				if ((outgoingMessage != null) && (output != null)) {
					out = outgoingMessage.poll(120000, TimeUnit.MILLISECONDS);
				}
				if (out != null) {
					output.write(out.generate());
					out = null;
				} else if (isRunning()) {
					output.write(keep);
					fireKeepAliveSent();
				}
			}
		} catch (final InterruptedException ie) {
			fireConnectionClosed();
		} catch (final IOException ioe) {
			fireConnectionClosed();
		} catch (final Exception e) {
			fireConnectionClosed();
		}

		try {
			output.close();
			notify();
		} catch (final Exception e) {}
	}

	/**
	 * Sets the 'run' variable to false, causing the thread to stop on its next
	 * loop.
	 */
	protected void stopThread() {
		setRunning(false);
	}
	
	private synchronized boolean isRunning() {
		return running;
	}
	
	private synchronized void setRunning(boolean run) {
		this. running = run;
	}
}