/*
 * Java Bittorrent API as its name indicates is a JAVA API that implements the
 * Bittorrent Protocol This project contains two packages: 1. jBittorrentAPI is
 * the "client" part, i.e. it implements all classes needed to publish files,
 * share them and download them. This package also contains example classes on
 * how a developer could create new applications. 2. trackerBT is the "tracker"
 * part, i.e. it implements a all classes needed to run a Bittorrent tracker
 * that coordinates peers exchanges. * Copyright (C) 2007 Baptiste Dubuis,
 * Artificial Intelligence Laboratory, EPFL This file is part of
 * jbittorrentapi-v1.0.zip Java Bittorrent API is free software and a free user
 * study set-up; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version. Java
 * Bittorrent API is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with Java Bittorrent API; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * @version 1.0
 * @author Baptiste Dubuis To contact the author: email:
 * baptiste.dubuis@gmail.com More information about Java Bittorrent API:
 * http://sourceforge.net/projects/bitext/
 */

package net.redlightning.jbittorrent;

import java.io.*;
import java.util.*;
import java.net.*;
import android.util.Log;
import net.redlightning.jbittorrent.interfaces.AbstractMessage;
import net.redlightning.jbittorrent.interfaces.IncomingListener;
import net.redlightning.jbittorrent.interfaces.OutgoingListener;

/**
 * Class representing a task that downloads pieces from a remote peer
 * 
 * @author Baptiste Dubuis (baptiste.dubuis@gmail.com)
 * @author Michael Isaacson (michael@redlightning.net)
 * @version 11.11.15
 */
public class DownloadTask extends Thread implements IncomingListener, OutgoingListener {
	private static final String TAG = DownloadTask.class.getSimpleName();
	private static final int IDLE = 0;
	private static final int WAIT_HS = 1;
	private static final int WAIT_BFORHAVE = 2;
	private static final int WAIT_UNCHOKE = 4;
	private static final int READY_2_DL = 5;
	private static final int DOWNLOADING = 6;
	private static final int WAIT_BLOCK = 7;
	protected static final int UNKNOWN_HOST = 1;
	protected static final int REFUSED = 2;
	private static final int BAD_HANDSHAKE = 3;
	protected static final int MALFORMED_MESSAGE = 4;
	private static final int TIMEOUT = 5;
	private transient int state = IDLE;
	private transient boolean running = true;
	protected transient Peer peer;
	private transient boolean initiate;
	private transient Piece downloadPiece = null;
	private transient int offset = 0;
	private transient Socket peerConnection = null;
	private transient OutputStream output = null;
	private transient InputStream input = null;
	protected transient MessageSender sender = null;
	private transient MessageReceiver receiver = null;
	private transient long updateTime = 0;
	private transient long lmrt = 0;
	private transient final DownloadManager manager;
	private transient List<Integer> pendingRequest;

	/**
	 * Start the downloading process from the remote peer in parameter
	 * 
	 * @param peer The peer to connect to
	 * @param fileID The file to be downloaded
	 * @param myID The id of the current client
	 * @param init True if this client initiate the connection, false otherwise
	 * @param socket Only set if this client receive a connection request from
	 *            the remote peer
	 * @param bitfield The piece currently owned by this client
	 */
	protected DownloadTask(final DownloadManager manager, final Peer remotePeer, final boolean init, final Socket socket) {
		super("Download Task");
		initiate = init;
		this.manager = manager;
		pendingRequest = new LinkedList<Integer>();

		if (socket == null) {
			peer = remotePeer;
		} else {
			try {
				peerConnection = socket;
				final String peerIP = peerConnection.getInetAddress().getHostAddress();
				final int peerPort = peerConnection.getPort();

				input = peerConnection.getInputStream();
				output = peerConnection.getOutputStream();
				peer = new Peer(peerIP, peerPort);
			} catch (final IOException ioe) {
				Log.e(TAG, "Unable to establish download task connection: " + ioe.getMessage());
			}
		}
		manager.addActiveTask(peer.toString(), this);
	}

	/**
	 * Start the downloading process from the remote peer in parameter
	 * 
	 * @param peer The peer to connect to
	 * @param fileID The file to be downloaded
	 * @param myID The id of the current client
	 * @param init True if this client initiate the connection, false otherwise
	 * @param bitfield The pieces currently owned by this client
	 */
	protected DownloadTask(DownloadManager manager, final Peer peer, final boolean init) {
		this(manager, peer, init, null);
	}

	/**
	 * Inits the connection to the remote peer. Also init the message sender and
	 * receiver. If necessary, starts the handshake with the peer
	 * 
	 * @throws UnknownHostException If the remote peer is unknown
	 * @throws IOException If the connection to the remote peer fails (reset,
	 *             ...)
	 */
	private void initConnection() throws UnknownHostException, IOException {
		if ((peerConnection == null) && !peer.isConnected()) {
			peerConnection = new Socket(peer.getIP(), peer.getPort());
			peerConnection.setReuseAddress(true);
			output = peerConnection.getOutputStream();
			input = peerConnection.getInputStream();
			peer.setConnected(true);
		}

		sender = new MessageSender(this, peer.toString(), output);
		sender.start();
		receiver = new MessageReceiver(this, peer.toString(), input);
		receiver.start();

		if (initiate) {
			try {
				sender.addMessageToQueue(new HandshakeMessage(manager.getTorrent().getInfoHashAsBinary(), manager.getTorrent().getPeerID()));
				changeState(WAIT_HS);
			} catch (final NullPointerException npe) {
				if (manager.getTorrent() == null) {
					Log.e(TAG, "initConnection - torrent is null");
				} else if (manager.getTorrent().getInfoHashAsBinary() == null) {
					Log.e(TAG, "initConnection - infohashasbinary is null");
				} else if (manager.getTorrent().getPeerID() == null) {
					Log.e(TAG, "initConnection - peerID is null");
				} else if (sender == null) {
					Log.e(TAG, "initConnection - sender is null");
				} else {
					Log.e(TAG, npe.getMessage());
				}
			}
		} else {
			changeState(WAIT_BFORHAVE);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		byte[] lock = new byte[1];
		try {
			initConnection();

			/**
			 * Wait for the task to end, i.e. the peer to return to IDLE state
			 */
			synchronized (lock) {
				while (isRunning()) {
					lock.wait(100);
				}
			}

			if (input != null) {
				input.close();
			}
			if (output != null) {
				output.close();
			}
			if (peerConnection != null) {
				peerConnection.close();
			}
		} catch (final UnknownHostException e) {
			fireTaskCompleted(peer.toString(), e.getMessage(), UNKNOWN_HOST);
		} catch (final IOException e) {
			fireTaskCompleted(peer.toString(), e.getMessage(), REFUSED);
		} catch (final InterruptedException e) {
			Log.d(TAG, "Connection with " + peer.toString() + " interrupted: " + e.getMessage());
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (peerConnection != null) {
				try {
					peerConnection.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		try {
			manager.getTasks().remove(this);
			manager.getTasks().remove(toString());
		} catch (Exception e) {

		}
	}

	/**
	 * Clear the piece currently downloading
	 */
	private synchronized void clear() {
		if (downloadPiece != null) {
			firePieceRequested(downloadPiece.getIndex(), false);
			downloadPiece = null;
		}
	}

	/**
	 * @return Returns this task's peer object
	 */
	public synchronized Peer getPeer() {
		return peer;

	}

	/**
	 * Request a peer to the peer
	 * 
	 * @param piece The piece to be requested to the peer
	 */
	protected synchronized void requestPiece(final Piece piece) {
		downloadPiece = piece;
		if (state == READY_2_DL) {
			changeState(DOWNLOADING);
		}

	}

	/**
	 * Fired when the connection to the remote peer has been closed. This method
	 * clear this task data and send a message to the DownloadManager, informing
	 * it that this peer connection has been closed, resulting in the deletion
	 * of this task
	 */
	@Override
	public synchronized void connectionClosed() {
		clear();
		fireTaskCompleted(peer.toString(), null, REFUSED);
	}

	/**
	 * Fired when a keep-alive message has been sent by the MessageSender. If at
	 * the time the keep-alive was sent, this peer has not received any message
	 * from the remote peer since more that 3 minutes, the remote peer is
	 * considered as dead, and we close the connection, then inform the
	 * DownloadManager that this connection timed out... Otherwise, inform the
	 * DownloadManager that this task is still alive and has not been used for a
	 * long time...
	 */
	@Override
	public synchronized void keepAliveSent() {
		if ((System.currentTimeMillis() - lmrt) > 180000) {
			clear();
			fireTaskCompleted(peer.toString(), null, TIMEOUT);
			return;
		}
		firePeerReady(peer.toString());
	}

	/**
	 * According to the message type, change the state of the task (peer) and
	 * take the necessary actions
	 * 
	 * @param m Message
	 */
	@Override
	public synchronized void messageReceived(final AbstractMessage mess) {
		AbstractMessage message;
		message = mess;

		if (message == null) {
			fireTaskCompleted(peer.toString(), null, MALFORMED_MESSAGE);
			return;
		}

		lmrt = System.currentTimeMillis();

		if (message.getType() == Peer.HANDSHAKE) {
			HandshakeMessage handshake = (HandshakeMessage) message;

			// Check that the requested file is the one this client is sharing
			if (Utils.compareBytes(handshake.getFileID(), manager.getTorrent().getInfoHashAsBinary())) {
				if (!initiate) { // If not already done, send handshake message
					//peer.setID(new String(handshake.getPeerID()));
					sender.addMessageToQueue(new HandshakeMessage(manager.getTorrent().getInfoHashAsBinary(), manager.getTorrent().getPeerID()));
				}

				sender.addMessageToQueue(new PeerProtocolMessage(Peer.BITFIELD, manager.getBitField()));

				changeState(WAIT_BFORHAVE);
			} else {
				fireTaskCompleted(peer.toString(), null, BAD_HANDSHAKE);
			}
			handshake = null;
		} else {
			PeerProtocolMessage peerMessage = (PeerProtocolMessage) message;
			switch (peerMessage.getType()) {
				case Peer.KEEP_ALIVE:
					// Nothing to do, just keep the connection open
					break;
				case Peer.CHOKE:
					// Change the choking state to true, meaning remote peer will not accept any request message from this client
					peer.setChoking(true);
					break;
				case Peer.UNCHOKE:
					// Change the choking state to false, meaning this client now accepts request messages from this client. If this task was already downloading a piece, then continue. Otherwise, advertise DownloadManager that it is ready to do so
					peer.setChoking(false);
					if (downloadPiece == null) {
						changeState(READY_2_DL);
					} else {
						changeState(DOWNLOADING);
					}
					break;
				case Peer.INTERESTED:
					// Change the interested state of the remote peer to true, meaning this peer will start downloading from this client if it is unchoked
					peer.setInterested(true);
					break;
				case Peer.NOT_INTERESTED:
					// Change the interested state of the remote peer to true, meaning this peer will not start downloading from this client if it is unchoked
					peer.setInterested(false);
					break;
				case Peer.HAVE:
					// Update the peer piece list with the piece described in this message and advertise DownloadManager of the change
					peer.setHasPiece(Utils.toInt(peerMessage.getPayload()), true);
					firePeerAvailability(peer.toString(), peer.getHasPiece());
					break;
				case Peer.BITFIELD:
					// Update the peer piece list with the piece described in this message and advertise DownloadManager of the change
					peer.setHasPiece(peerMessage.getPayload());
					firePeerAvailability(peer.toString(), peer.getHasPiece());
					changeState(WAIT_UNCHOKE);
					break;
				case Peer.REQUEST:
					// If the peer is not choked, advertise the DownloadManager of this request. Otherwise, end connection since the peer does not respect the Bittorrent protocol
					if (peer.isChoked()) {
						fireTaskCompleted(peer.toString(), null, MALFORMED_MESSAGE);
					} else {
						firePeerRequest(peer.toString(), downloadPiece, Utils.toInt(Utils.subArray(peerMessage.getPayload(), 4, 4)), Utils.toInt(Utils.subArray(peerMessage.getPayload(), 8, 4)));
					}
					break;
				case Peer.PIECE:
					// Sets the block of data downloaded in the piece block list and update the peer download rate. Removes the piece block from the pending request list and change state.
					final int begin = Utils.toInt(Utils.subArray(peerMessage.getPayload(), 4, 4));
					final byte[] data = Utils.subArray(peerMessage.getPayload(), 8, peerMessage.getPayload().length - 8);
					if (downloadPiece == null) {
						Log.e(TAG, "Download peice is null!");
					} else {
						downloadPiece.setBlock(begin, data);
						peer.setDLRate(data.length);
						pendingRequest.remove(Integer.valueOf(begin));
						changeState(DownloadTask.DOWNLOADING);
					}
					break;
				case Peer.CANCEL:
					// TODO: Still to implement the cancel message. Not used here
					break;
				case Peer.PORT:
					// TODO: Still to implement the port message. Not used here
					break;
				case Peer.EXTENDED:
					Log.i(TAG, "Received Extended Message!");
					break;
				default:
					//Log.i(TAG, "Received Unknown Message Type: " + peerMessage.getType());
					//Log.i(TAG, "Payload: " + Utils.toString(peerMessage.getPayload()));
					break;
			}
			peerMessage = null;
		}

		message = null;

	}

	/**
	 * Change the state of the task. State depends on the previously received
	 * messages This is here that are taken the most important decisions about
	 * the messages to be sent to the remote peer
	 * 
	 * @param newState The new state of the download task
	 */
	private synchronized void changeState(final int newState) {
		state = newState;
		switch (newState) {
			case WAIT_BLOCK:
				/**
				 * Keep a certain number of unanswered requests, for
				 * performance. If only sending 1 request an waiting, it is a
				 * loss of time and bandwidth because of the RTT to the remote
				 * peer
				 */
				if ((pendingRequest.size() < 5) && (offset < downloadPiece.getLength())) {
					changeState(DOWNLOADING);
				}
				break;
			case READY_2_DL:
				//Advertise the DownloadManager that this task is ready to download
				firePeerReady(peer.toString());
				break;
			case DOWNLOADING:
				/**
				 * If offset is bigger than the piece length and the pending
				 * request size is 0, then we have downloaded all the piece
				 * blocks and we can verify the integrity of the data
				 */
				if (offset >= downloadPiece.getLength()) {
					if (pendingRequest.size() == 0) {
						offset = 0;

						if (downloadPiece.verify(manager.getTorrent().getPieceHashAsBinary().get(downloadPiece.getIndex()))) {
							firePieceCompleted(downloadPiece, true);
						} else {
							firePieceCompleted(downloadPiece, false);
						}
						clear();
						changeState(READY_2_DL);
					}
				} else if ((downloadPiece != null) && !peer.isChoking()) {
					final byte[] pieceIndex = Utils.toArray(downloadPiece.getIndex());
					final byte[] begin = Utils.toArray(offset);

					int length = downloadPiece.getLength() - offset;
					if (length >= Peer.BLOCK_SIZE) {
						length = Peer.BLOCK_SIZE;
					}
					sender.addMessageToQueue(new PeerProtocolMessage(Peer.REQUEST, Utils.concat(pieceIndex, Utils.concat(begin, Utils.toArray(length))), 2));
					if (updateTime == 0) {
						updateTime = System.currentTimeMillis();
					}
					pendingRequest.add(Integer.valueOf(offset));
					offset += Peer.BLOCK_SIZE;
					changeState(DownloadTask.WAIT_BLOCK);
				}
				break;
			default:
				break;
		}

	}

	/**
	 * Fired to inform if the given piece is requested or not...
	 * 
	 * @param piece the piece of the torrent this task was working on
	 * @param requested true if the downloaded piece is requested, false if not
	 */
	private synchronized void firePieceRequested(final int piece, final boolean requested) {
		manager.pieceRequested(piece, requested);
	}

	/**
	 * Fired to inform that the given piece has been completed or not
	 * 
	 * @param piece the piece of the torrent this task was working on
	 * @param complete true if the downloaded piece is complete, false if not
	 */
	private synchronized void firePieceCompleted(final Piece piece, final boolean complete) {
		manager.pieceCompleted(peer.toString(), piece, complete);
	}

	/**
	 * Fired to inform that the task is finished for a certain reason
	 * 
	 * @param peerId the peer "id" in the format of ip address:port (xxx.xxx.xxx.xxx:pppp)
	 * @param reason Reason why the task ended
	 */
	private synchronized void fireTaskCompleted(final String peerId, final String message, final int reason) {
		end();
		manager.taskCompleted(peerId, message, reason);
	}

	/**
	 * Fired to inform that this task is ready to download
	 * 
	 * @param peerId String
	 */
	private synchronized void firePeerReady(final String peerId) {
		manager.peerReady(peerId);
	}

	/**
	 * Fired to inform that the peer requests a piece block
	 * 
	 * @param peerID the peer "id" in the format of ip address:port (xxx.xxx.xxx.xxx:pppp)
	 * @param piece int
	 * @param begin int
	 * @param length int
	 */
	private synchronized void firePeerRequest(final String peerID, final Piece piece, final int begin, final int length) {
		manager.peerRequest(peerID, piece, begin, length);
	}

	/**
	 * Fired to inform that the availability of this peer has changed
	 * 
	 * @param peerId the peer "id" in the format of ip address:port (xxx.xxx.xxx.xxx:pppp)
	 * @param hasPiece BitSet
	 */
	private synchronized void firePeerAvailability(final String peerId, final BitSet hasPiece) {
		manager.peerAvailability(peerId, hasPiece);
	}

	/**
	 * Stops this thread by setting the 'run' variable to false and closing the
	 * communication thread (Message receiver and sender). Closes the connection
	 * to the remote peer
	 */
	protected synchronized void end() {
		changeState(DownloadTask.IDLE);
		setRunning(false);

		if (sender != null) {
			sender.stopThread();
			sender = null;
		}
		if (receiver != null) {
			receiver.stopThread();
			receiver = null;
		}
		try {
			peerConnection.close();
		} catch (final Exception e) {}
		try {
			input.close();
		} catch (final Exception e) {}
		try {
			output.close();
		} catch (final Exception e) {}
		peerConnection = null;

		notifyAll();
	}

	/**
	 * @param true if the thread should continue to run, false to have it quit
	 */
	private synchronized void setRunning(boolean b) {
		running = b;
	}

	/**
	 * @return true if the thread should be running, false if it's time to quit
	 */
	private synchronized boolean isRunning() {
		return running;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		if (peerConnection != null) {
			try {
				input.close();
				output.close();

				if (!peerConnection.isClosed()) {
					peerConnection.close();
				}
			} finally {
				super.finalize();
			}
		}
		super.finalize();
	}
}