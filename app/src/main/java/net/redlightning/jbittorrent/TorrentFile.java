/*
 * Java Bittorrent API as its name indicates is a JAVA API that implements the
 * Bittorrent Protocol This project contains two packages: 1. jBittorrentAPI is
 * the "client" part, i.e. it implements all classes needed to publish files,
 * share them and download them. This package also contains example classes on
 * how a developer could create new applications. 2. trackerBT is the "tracker"
 * part, i.e. it implements a all classes needed to run a Bittorrent tracker
 * that coordinates peers exchanges. * Copyright (C) 2007 Baptiste Dubuis,
 * Artificial Intelligence Laboratory, EPFL This file is part of
 * jbittorrentapi-v1.0.zip Java Bittorrent API is free software and a free user
 * study set-up; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version. Java
 * Bittorrent API is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with Java Bittorrent API; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * @version 1.0
 * @author Baptiste Dubuis To contact the author: email:
 * baptiste.dubuis@gmail.com More information about Java Bittorrent API:
 * http://sourceforge.net/projects/bitext/
 */

package net.redlightning.jbittorrent;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import android.util.Log;

/**
 * Representation of a torrent file
 * 
 * @author Baptiste Dubuis (baptiste.dubuis@gmail.com)
 * @author Michael Isaacson (michael@redlightning.net)
 * @version 11.3.7
 */
public class TorrentFile {
	private static final String TAG = TorrentFile.class.getSimpleName();
	private static final String DOUBLE_TAB = "\t\t";
	private String torrentFileName;
	private String torrentFileURL;
	private String comment;
	private String createdBy;
	private String infoHashAsHex;
	private String infoHashAsURL;
	private byte[] infoHashAsBinary;
	private long creationDate;
	private int pieceLength;
	private long totalLength;
	private List<String> fileNames;
	private List<String> selectedFiles;
	private List<Integer> length;
	private List<String> announceURLs;
	private List<byte[]> pieceHashAsBinary;
	private List<String> pieceHashAsHex;
	private List<String> pieceHashAsURL;
	private Map<Integer, Integer> incompletePieces = new Hashtable<Integer, Integer>();
	private Map<Integer, Piece> inProgressPieces = new Hashtable<Integer, Piece>();
	private String savePath;
	private String status;
	private String errorMessage;
	private String note;
	private BitSet complete;
	private BitSet isRequested;
	private int allocated;
	private int blocked;
	private String magneto;
	private byte[] peerID;
	private long downloaded = 0;
	private long uploaded = 0;
	private long left = 0;
	private List<String> keywords;
	private boolean privateTorrent;

	/**
	 * Create the TorrentFile object and initiate its instance variables
	 */
	public TorrentFile() {
		super();
		creationDate = -1;
		totalLength = -1;
		pieceLength = -1;
		fileNames = new ArrayList<String>();
		selectedFiles = new ArrayList<String>();
		length = new ArrayList<Integer>();
		status = "Starting up...";
		pieceHashAsBinary = new ArrayList<byte[]>();
		pieceHashAsURL = new ArrayList<String>();
		pieceHashAsHex = new ArrayList<String>();
		infoHashAsBinary = new byte[20];
		announceURLs = new ArrayList<String>();
		incompletePieces = new Hashtable<Integer, Integer>();
		inProgressPieces = new Hashtable<Integer, Piece>();
		peerID = Utils.generateID();
	}

	/**
	 * Create a Torrent from a Magent URI
	 * 
	 * @param magnet properly formatted Magnet URI
	 */
	public TorrentFile(final String magnet) {
		super();
		creationDate = -1;
		totalLength = -1;
		pieceLength = -1;
		fileNames = new ArrayList<String>();
		selectedFiles = new ArrayList<String>();
		length = new ArrayList<Integer>();
		status = "Starting up...";
		pieceHashAsBinary = new ArrayList<byte[]>();
		pieceHashAsURL = new ArrayList<String>();
		pieceHashAsHex = new ArrayList<String>();
		infoHashAsBinary = new byte[20];
		announceURLs = new ArrayList<String>();
		incompletePieces = new Hashtable<Integer, Integer>();
		inProgressPieces = new Hashtable<Integer, Piece>();
		peerID = Utils.generateID();

		
		//Many likely only include an infohash (xt), dn, and tr
		String[] parts = (magnet.replace("magnet:?", "")).split("\\&");
		for (int p = 0; p < parts.length; p++) {
			Log.i(TAG, "P: " + parts[p]);
			String[] item = parts[p].split("=");
			
			if (item[0].equalsIgnoreCase("dn")) {
				Log.d(TAG, "Torrent Name: " + URLDecoder.decode(item[1]));
				setTorrentFileName(URLDecoder.decode(item[1]));
			} else if (item[0].equalsIgnoreCase("xl")) {
				Log.d(TAG, "Total Length set to: " + URLDecoder.decode(item[1]));
				setTotalLength(Long.valueOf(URLDecoder.decode(item[1])));
				setLeft(getTotalLength());
			} else if (item[0].equalsIgnoreCase("xt")) {
				if(item[1].startsWith("urn:btih:")) {
					Log.d(TAG, "Info Hash: " + item[1]);
					setInfoHashAsHex(item[1].replace("urn:btih:", ""));
					setInfoHashAsBinary(Utils.toArray(getInfoHashAsHex()));
				} else {
					Log.w(TAG, "Unsupported Info Hash: " + URLDecoder.decode(item[1]));
				}
			} else if (item[0].equalsIgnoreCase("as")) {
				Log.d(TAG, "Acceptable Source: " + URLDecoder.decode(item[1]));
				setTorrentFileURL(URLDecoder.decode(item[1]));
			} else if (item[0].equalsIgnoreCase("xs")) {
				Log.d(TAG, "Exact Source: " + URLDecoder.decode(item[1]));
				//torrent.setTorrentFileURL(URLDecoder.decode(item[1]));
			} else if (item[0].equalsIgnoreCase("kt")) {
				Log.d(TAG, "Keywords: " + item[1]);
				List<String> keywords = new ArrayList<String>();
				keywords.addAll(Arrays.asList(URLDecoder.decode(item[1]).split(" ")));
				setKeywords(keywords);
			} else if (item[0].equalsIgnoreCase("mt")) {
				Log.d(TAG, "Magneto: " + URLDecoder.decode(item[1]));
				setMagneto(item[1]);
			} else if (item[0].equalsIgnoreCase("tr")) {
				Log.d(TAG, "Added Tracker: " + URLDecoder.decode(item[1]));
				getAnnounceURLs().add(URLDecoder.decode(item[1]));
			}
		}
	}

	/**
	 * Create the TorrentFile object and initiate its instance variables
	 */
	public TorrentFile(final String fileName, final String saveTo, final String progress) {
		super();
		torrentFileName = fileName;
		creationDate = -1;
		totalLength = -1;
		pieceLength = -1;
		fileNames = new ArrayList<String>();
		selectedFiles = new ArrayList<String>();
		length = new ArrayList<Integer>();
		status = "Starting up...";
		savePath = saveTo;
		pieceHashAsBinary = new ArrayList<byte[]>();
		pieceHashAsURL = new ArrayList<String>();
		pieceHashAsHex = new ArrayList<String>();
		infoHashAsBinary = new byte[20];
		announceURLs = new ArrayList<String>();
		incompletePieces = new Hashtable<Integer, Integer>();
		inProgressPieces = new Hashtable<Integer, Piece>();
		complete = new BitSet();
		isRequested = new BitSet();
		peerID = Utils.generateID();
		setComplete(progress);
	}

	/**
	 * @param index the piece number to mark as complete
	 */
	protected synchronized void completePiece(final int index) {
		incompletePieces.remove(index);
		inProgressPieces.remove(index);
	}

	public synchronized int getAllocated() {
		return allocated;
	}

	/**
	 * @return the announceURLs
	 */
	public synchronized List<String> getAnnounceURLs() {
		return announceURLs;
	}

	public synchronized int getBlocked() {
		return blocked;
	}

	/**
	 * @return the comment
	 */
	public synchronized String getComment() {
		return comment;
	}

	/**
	 * @return the bitset showing which pieces are complete
	 */
	public synchronized BitSet getComplete() {
		return complete;
	}

	/**
	 * @return the createdBy
	 */
	public synchronized String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @return the creationDate
	 */
	public synchronized long getCreationDate() {
		return creationDate;
	}

	/**
	 * @return the errorMessage
	 */
	public synchronized String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @return the fileNames
	 */
	public synchronized List<String> getFileNames() {
		return fileNames;
	}

	/**
	 * @return the incompletePieces
	 */
	public synchronized Map<Integer, Integer> getIncompletePieces() {
		return incompletePieces;
	}

	/**
	 * @return the infoHashAsBinary
	 */
	public synchronized byte[] getInfoHashAsBinary() {
		return infoHashAsBinary;
	}

	/**
	 * @return the infoHashAsHex
	 */
	public synchronized String getInfoHashAsHex() {
		return infoHashAsHex;
	}

	/**
	 * @return the infoHashAsURL
	 */
	public synchronized String getInfoHashAsURL() {
		return infoHashAsURL;
	}

	/**
	 * @return the inProgressPieces
	 */
	public synchronized Map<Integer, Piece> getInProgressPieces() {
		return inProgressPieces;
	}

	/**
	 * @return the bitset showing which pieces are requested
	 */
	public synchronized BitSet getIsRequested() {
		return isRequested;
	}

	/**
	 * @return the left
	 */
	public synchronized long getLeft() {
		return left;
	}

	/**
	 * @return the length
	 */
	public synchronized List<Integer> getLength() {
		return length;
	}

	/**
	 * @return the pieceHashAsBinary
	 */
	public synchronized List<byte[]> getPieceHashAsBinary() {
		return pieceHashAsBinary;
	}

	/**
	 * @return the pieceHashAsHex
	 */
	public synchronized List<String> getPieceHashAsHex() {
		return pieceHashAsHex;
	}

	/**
	 * @return the pieceHashAsURL
	 */
	public synchronized List<String> getPieceHashAsURL() {
		return pieceHashAsURL;
	}

	/**
	 * @return the pieceLength
	 */
	public synchronized int getPieceLength() {
		return pieceLength;
	}

	/**
	 * @return a String of 1s and 0s representing the 'completed' BitSet
	 */
	public synchronized String getProgressString() {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < complete.length(); i++) {
			if (complete.get(i)) {
				buffer.append('1');
			} else {
				buffer.append('0');
			}
		}
		return buffer.toString();
	}

	/**
	 * @return the savePath
	 */
	public synchronized String getSavePath() {
		return savePath;
	}

	/**
	 * @return the selectedFiles
	 */
	public synchronized List<String> getSelectedFiles() {
		return selectedFiles;
	}

	/**
	 * @return the status
	 */
	public synchronized String getStatus() {
		return status;
	}

	/**
	 * @return the torrentFileName
	 */
	public synchronized String getTorrentFileName() {
		return torrentFileName;
	}

	/**
	 * @return the totalLength
	 */
	public synchronized long getTotalLength() {
		return totalLength;
	}

	/**
	 * Print the torrent information in a readable manner.
	 * 
	 * @param detailed Choose if we want a detailed output or not. Detailed
	 *            output prints the comment, the files list and the pieces
	 *            hashes while the standard output simply prints tracker url,
	 *            creator, creation date and info hash
	 */
	public synchronized void printData(final boolean detailed) {
		Log.i(TAG, "Tracker URLs: ");
		for (int a = 0; a < announceURLs.size(); a++) {
			Log.i(TAG, "\t" + this.announceURLs.get(a));
		}
		Log.i(TAG, "Torrent created by : " + this.createdBy);
		Log.i(TAG, "Torrent creation date : " + new Date(this.creationDate));
		Log.i(TAG, "Info hash :\n");
		Log.i(TAG, DOUBLE_TAB + Arrays.toString(this.infoHashAsBinary));
		Log.i(TAG, DOUBLE_TAB + this.infoHashAsHex);
		Log.i(TAG, DOUBLE_TAB + this.infoHashAsURL);
		Log.i(TAG, "Piece length : " + this.pieceLength + ") :\n");

		Log.i(TAG, "Comment :" + this.comment);
		Log.i(TAG, "\nFiles List :\n");
		for (int i = 0; i < this.length.size(); i++) {
			Log.i(TAG, "\t- " + this.fileNames.get(i) + " ( " + this.length.get(i) + " Bytes )");
		}
		Log.i(TAG, "\n");

		Log.i(TAG, "Pieces hashes (piece length = " + this.pieceLength + ") :\n");
		for (int i = 0; i < this.pieceHashAsBinary.size(); i++) {
			Log.i(TAG, (i + 1) + ":" + DOUBLE_TAB + this.pieceHashAsBinary.get(i));
			Log.i(TAG, DOUBLE_TAB + this.pieceHashAsHex.get(i));
			Log.i(TAG, DOUBLE_TAB + this.pieceHashAsURL.get(i));
		}
	}

	public synchronized void setAllocated(final int allocated) {
		this.allocated = allocated;
	}

	/**
	 * @param announceURLs the announceURLs to set
	 */
	public synchronized void setAnnounceURLs(List<String> announceURLs) {
		this.announceURLs = announceURLs;
	}

	public synchronized void setBlocked(final int blocked) {
		this.blocked = blocked;
	}

	/**
	 * @param comment the comment to set
	 */
	public synchronized void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @param complete the bitset showing which pieces are complete
	 */
	public synchronized void setComplete(final BitSet complete) {
		this.complete = complete;
	}

	/**
	 * Mark a piece as complete or not according to the parameters
	 * 
	 * @param piece The index of the piece to be updated
	 * @param is True if the piece is now complete, false otherwise
	 */
	public synchronized void setComplete(final int piece, final boolean is) {
		complete.set(piece, is);
	}

	/**
	 * @param bits a binary string representation of a bitset
	 */
	public synchronized void setComplete(final String bits) {
		if (bits != null) {
			for (int i = 0; i < bits.length(); i++) {
				if (bits.charAt(i) == '1') {
					complete.set(i, true);
				} else {
					complete.set(i, false);
				}
			}
		}
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public synchronized void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public synchronized void setCreationDate(long creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public synchronized void setErrorMessage(final String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @param fileNames the fileNames to set
	 */
	public synchronized void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}

	/**
	 * @param incompletePieces the incompletePieces to set
	 */
	public synchronized void setIncompletePieces(Map<Integer, Integer> incompletePieces) {
		this.incompletePieces = incompletePieces;
	}

	/**
	 * @param infoHashAsBinary the infoHashAsBinary to set
	 */
	public synchronized void setInfoHashAsBinary(byte[] infoHashAsBinary) {
		this.infoHashAsBinary = infoHashAsBinary;
	}

	/**
	 * @param infoHashAsHex the infoHashAsHex to set
	 */
	public synchronized void setInfoHashAsHex(String infoHashAsHex) {
		this.infoHashAsHex = infoHashAsHex;
	}

	/**
	 * @param infoHashAsURL the infoHashAsURL to set
	 */
	public synchronized void setInfoHashAsURL(String infoHashAsURL) {
		this.infoHashAsURL = infoHashAsURL;
	}

	/**
	 * @param inProgressPieces the inProgressPieces to set
	 */
	public synchronized void setInProgressPieces(Map<Integer, Piece> inProgressPieces) {
		this.inProgressPieces = inProgressPieces;
	}

	/**
	 * @param isRequested the bitset showing which pieces are requested
	 */
	public synchronized void setIsRequested(final BitSet isRequested) {
		this.isRequested = isRequested;
	}

	/**
	 * @param left the left to set
	 */
	public synchronized void setLeft(long left) {
		this.left = left;
	}

	/**
	 * @return the downloaded
	 */
	public synchronized long getDownloaded() {
		return downloaded;
	}

	/**
	 * @param downloaded the downloaded to set
	 */
	public synchronized void setDownloaded(long downloaded) {
		this.downloaded = downloaded;
	}

	/**
	 * @return the uploaded
	 */
	public synchronized long getUploaded() {
		return uploaded;
	}

	/**
	 * @param uploaded the uploaded to set
	 */
	public synchronized void setUploaded(long uploaded) {
		this.uploaded = uploaded;
	}

	/**
	 * @param length the length to set
	 */
	public synchronized void setLength(List<Integer> length) {
		this.length = length;
	}

	/**
	 * @param pieceHashAsBinary the pieceHashAsBinary to set
	 */
	public synchronized void setPieceHashAsBinary(List<byte[]> pieceHashAsBinary) {
		this.pieceHashAsBinary = pieceHashAsBinary;
	}

	/**
	 * @param pieceHashAsHex the pieceHashAsHex to set
	 */
	public synchronized void setPieceHashAsHex(List<String> pieceHashAsHex) {
		this.pieceHashAsHex = pieceHashAsHex;
	}

	/**
	 * @param pieceHashAsURL the pieceHashAsURL to set
	 */
	public synchronized void setPieceHashAsURL(List<String> pieceHashAsURL) {
		this.pieceHashAsURL = pieceHashAsURL;
	}

	/**
	 * @param pieceLength the pieceLength to set
	 */
	public synchronized void setPieceLength(int pieceLength) {
		this.pieceLength = pieceLength;
	}

	/**
	 * @param savePath the savePath to set
	 */
	public synchronized void setSavePath(String savePath) {
		this.savePath = savePath;
	}

	/**
	 * @param selectedFiles the selectedFiles to set
	 */
	public synchronized void setSelectedFiles(List<String> selectedFiles) {
		this.selectedFiles = selectedFiles;
	}

	/**
	 * @param status the status to set
	 */
	public synchronized void setStatus(final String status) {
		this.status = status;
	}

	/**
	 * @param torrentFileName the torrentFileName to set
	 */
	public synchronized void setTorrentFileName(String torrentFileName) {
		this.torrentFileName = torrentFileName;
	}

	/**
	 * @param totalLength the totalLength to set
	 */
	public synchronized void setTotalLength(long totalLength) {
		this.totalLength = totalLength;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public synchronized String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(torrentFileName);
		buffer.append(':');
		buffer.append(getProgressString());
		if (selectedFiles != null) {
			for (int i = 0; i < selectedFiles.size(); i++) {
				buffer.append(':');
				buffer.append(selectedFiles.get(i));
			}
		}
		return buffer.toString();
	}

	/**
	 * @param magneto the magneto to set
	 */
	public synchronized void setMagneto(final String magneto) {
		this.magneto = magneto;
	}

	/**
	 * @return the magneto
	 */
	public synchronized String getMagneto() {
		return magneto;
	}

	/**
	 * @param keywords the keywords to set
	 */
	public synchronized void setKeywords(final List<String> keywords) {
		this.keywords = keywords;
	}

	/**
	 * @return the keywords
	 */
	public synchronized List<String> getKeywords() {
		return keywords;
	}

	/**
	 * @param torrentFileURL the torrentFileURL to set
	 */
	public synchronized void setTorrentFileURL(final String torrentFileURL) {
		this.torrentFileURL = torrentFileURL;
	}

	/**
	 * @return the torrentFileURL
	 */
	public String getTorrentFileURL() {
		return torrentFileURL;
	}

	/**
	 * @return the peerID
	 */
	public synchronized byte[] getPeerID() {
		return peerID;
	}

	/**
	 * @param peerID the peerID to set
	 */
	public synchronized void setPeerID(final byte[] peerID) {
		this.peerID = peerID;
	}

	public synchronized boolean isPrivate() {
		return privateTorrent;
	}
	
	public synchronized void setPrivate(boolean priv) {
		privateTorrent = priv;
	}

	/**
	 * @return the note
	 */
	public synchronized String getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public synchronized void setNote(String note) {
		this.note = note;
	}
}