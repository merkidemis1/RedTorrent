/**
 * 
 */
package net.redlightning.jbittorrent;

import android.util.Log;

/**
 * For debugging purposes, log uncaught exceptions from the download threads and
 * log the error messages
 * 
 * @author Michael Isaacson (michael@redlightning.net)
 * @version 11.3.3
 */
public class DownloadThreadGroup extends ThreadGroup {
	private static final String TAG = DownloadThreadGroup.class.getSimpleName();

	/**
	 * Constructor
	 */
	public DownloadThreadGroup() {
		super("jBittorrent Thread Group");
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.ThreadGroup#uncaughtException(java.lang.Thread,
	 * java.lang.Throwable)
	 */
	@Override
	public void uncaughtException(final Thread thread, final Throwable exception) {
		Log.e(TAG, "RT - Uncaught Exception: " + exception.getClass().getSimpleName() + " - " + exception.getMessage());
		exception.printStackTrace();
	}
}
