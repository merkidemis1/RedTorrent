/*
 * Java Bittorrent API as its name indicates is a JAVA API that implements the
 * Bittorrent Protocol This project contains two packages: 1. jBittorrentAPI is
 * the "client" part, i.e. it implements all classes needed to publish files,
 * share them and download them. This package also contains example classes on
 * how a developer could create new applications. 2. trackerBT is the "tracker"
 * part, i.e. it implements a all classes needed to run a Bittorrent tracker
 * that coordinates peers exchanges. * Copyright (C) 2007 Baptiste Dubuis,
 * Artificial Intelligence Laboratory, EPFL This file is part of
 * jbittorrentapi-v1.0.zip Java Bittorrent API is free software and a free user
 * study set-up; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version. Java
 * Bittorrent API is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with Java Bittorrent API; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * @version 1.0
 * @author Baptiste Dubuis To contact the author: email:
 * baptiste.dubuis@gmail.com More information about Java Bittorrent API:
 * http://sourceforge.net/projects/bitext/
 */

package net.redlightning.jbittorrent;

import java.util.*;

/**
 * Class representing a bittorrent peer
 * 
 * @author Baptiste Dubuis (baptiste.dubuis@gmail.com)
 * @author Michael Isaacson (michael@redlightning.net)
 * @version 11.1.7
 */
public class Peer {
	protected static final int HANDSHAKE = -1;
	protected static final int KEEP_ALIVE = 0;
	protected static final int CHOKE = 1;
	protected static final int UNCHOKE = 2;
	protected static final int INTERESTED = 3;
	protected static final int NOT_INTERESTED = 4;
	protected static final int HAVE = 5;
	protected static final int BITFIELD = 6;
	protected static final int REQUEST = 7;
	protected static final int PIECE = 8;
	protected static final int CANCEL = 9;
	protected static final int PORT = 10;
	protected static final int EXTENDED = 20;
	protected static final int BLOCK_SIZE = 16384;
	private String ip;
	private int port;
	private boolean interested = false;
	private boolean choked = true;
	private boolean interesting = false;
	private boolean choking = true;
	private BitSet hasPiece= new BitSet();
	private int downloaded = 0;
	private float dlrate = 0;
	private float ulrate = 0;
	private int uploaded = 0;
	private boolean connected = false;
	private boolean supportsEncryption = false;
	
	/**
	 * 
	 * @param id
	 * @param ip
	 * @param port
	 */
	protected Peer(final String ip, final int port) {
		this.ip = ip;
		this.port = port;
	}

	/**
	 * Returns the number of bytes downloaded since the last reset
	 * 
	 * @param reset true if the download rate should be reset
	 * @return float
	 */
	protected float getDLRate(boolean reset) {
		if (reset) {
			float tmp = this.dlrate;
			this.dlrate = 0;
			return tmp;
		} else
			return this.dlrate;

	}

	/**
	 * Returns the number of bytes uploaded since the last reset.
	 * 
	 * @param reset true if the download rate should be reset
	 * @return float
	 */
	protected float getULRate(boolean reset) {
		if (reset) {
			float tmp = this.ulrate;
			this.ulrate = 0;
			return tmp;
		} else
			return this.ulrate;
	}

	/**
	 * Returns the total number of bytes downloaded from this peer
	 * 
	 * @return int
	 */
	public int getDL() {
		return this.downloaded;
	}

	/**
	 * Returns the total number of bytes uploaded to this peer
	 * 
	 * @return int
	 */
	public int getUL() {
		return this.uploaded;
	}

	/**
	 * Updates the downloaded values
	 * 
	 * @param dl int
	 */
	public void setDLRate(int dl) {
		this.dlrate += dl;
		this.downloaded += dl;
	}

	/**
	 * Updates the uploaded values
	 * 
	 * @param ul int
	 */
	public void setULRate(int ul) {
		this.ulrate += ul;
		this.uploaded += ul;
	}

	/**
	 * Returns the IP address of this peer
	 * 
	 * @return String
	 */
	public String getIP() {
		return this.ip;
	}

	/**
	 * Returns the listening port of this peer
	 * 
	 * @return int
	 */
	public int getPort() {
		return this.port;
	}

	/**
	 * Returns the pieces availability of this peer
	 * 
	 * @return BitSet
	 */
	public BitSet getHasPiece() {
		return this.hasPiece;
	}

	/**
	 * Sets the IP address of this peer
	 * 
	 * @param ip String
	 */
	public void setIP(String ip) {
		this.ip = ip;
	}

	/**
	 * Sets the listening port of this peer
	 * 
	 * @param port int
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * Returns if this peer is interested or not
	 * 
	 * @return boolean
	 */
	public boolean isInterested() {
		return this.interested;
	}

	/**
	 * Returns if this peer is choked or not
	 * 
	 * @return boolean
	 */
	public boolean isChoked() {
		return this.choked;
	}

	/**
	 * Returns if this peer is interesting or not
	 * 
	 * @return boolean
	 */
	public boolean isInteresting() {
		return this.interesting;
	}

	/**
	 * Returns if this peer is choking or not
	 * 
	 * @return boolean
	 */
	public boolean isChoking() {
		return this.choking;
	}

	/**
	 * Sets if this peer is intereseted or not
	 * 
	 * @param i boolean
	 */
	public void setInterested(boolean i) {
		this.interested = i;
	}

	/**
	 * Sets if this peer is choked or not
	 * 
	 * @param c boolean
	 */
	public void setChoked(boolean c) {
		this.choked = c;
	}

	/**
	 * Sets if this peer is interesting or not
	 * 
	 * @param i boolean
	 */
	public void setInteresting(boolean i) {
		this.interesting = i;
	}

	/**
	 * Sets if this peer is choking or not
	 * 
	 * @param c boolean
	 */
	public void setChoking(boolean c) {
		this.choking = c;
	}

	/**
	 * Updates this peer availability according to the received bitfield
	 * 
	 * @param bitfield byte[]
	 */
	public void setHasPiece(byte[] bitfield) {
		boolean[] b = Utils.toBitArray(bitfield);
		for (int i = 0; i < b.length; i++)
			this.hasPiece.set(i, b[i]);
	}

	/**
	 * Updates the availability of the piece in parameter
	 * 
	 * @param piece int
	 * @param has boolean
	 */
	protected void setHasPiece(int piece, boolean has) {
		this.hasPiece.set(piece, has);
	}

	public boolean isConnected() {
		return this.connected;
	}

	public void setConnected(boolean connectionStatus) {
		this.connected = connectionStatus;
	}

	/**
	 * Compares if this peer is equal to the peer in parameter
	 * 
	 * @param p Peer
	 * @return boolean
	 */
	@Override
	public boolean equals(final Object compare) {
		boolean returnVal = false;

		if (compare instanceof Peer) {
			final Peer peer = (Peer) compare;
			if (peer != null && (ip != null && ip.equals(peer.getIP())) && port == peer.getPort()) {
				returnVal = true;
			}
		}
		return returnVal;
	}

	@Override
	public int hashCode() {
		return super.hashCode();

	}

	/**
	 * Returns this peer characteristics in the form <ip address>:<port>
	 * 
	 * @return String
	 */
	public String toString() {
		return (this.ip + ":" + this.port);
	}

	/**
	 * @param supportsEncryption the supportsEncryption to set
	 */
	public void setSupportsEncryption(boolean supportsEncryption) {
		this.supportsEncryption = supportsEncryption;
	}

	/**
	 * @return the supportsEncryption
	 */
	public boolean doesSupportEncryption() {
		return supportsEncryption;
	}
}
