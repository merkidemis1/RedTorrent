/*
 * Java Bittorrent API as its name indicates is a JAVA API that implements the
 * Bittorrent Protocol This project contains two packages: 1. jBittorrentAPI is
 * the "client" part, i.e. it implements all classes needed to publish files,
 * share them and download them. This package also contains example classes on
 * how a developer could create new applications. 2. trackerBT is the "tracker"
 * part, i.e. it implements a all classes needed to run a Bittorrent tracker
 * that coordinates peers exchanges. * Copyright (C) 2007 Baptiste Dubuis,
 * Artificial Intelligence Laboratory, EPFL This file is part of
 * jbittorrentapi-v1.0.zip Java Bittorrent API is free software and a free user
 * study set-up; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version. Java
 * Bittorrent API is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with Java Bittorrent API; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * @version 1.0
 * @author Baptiste Dubuis To contact the author: email:
 * baptiste.dubuis@gmail.com More information about Java Bittorrent API:
 * http://sourceforge.net/projects/bitext/
 */

package net.redlightning.jbittorrent;

import java.io.*;
import java.util.*;
import java.util.zip.GZIPInputStream;
import android.util.Log;

/**
 * Class enabling to process a torrent file
 * 
 * @author Baptiste Dubuis (baptiste.dubuis@gmail.com)
 * @author Michael Isaacson (michael@redlightning.net)
 * @version 11.1.18
 */
public class TorrentProcessor {
	private static final String TAG = TorrentProcessor.class.getSimpleName();
	private TorrentFile torrent;

	public TorrentProcessor(TorrentFile torrent) {
		this.torrent = torrent;
	}

	public TorrentProcessor() {
		this.torrent = new TorrentFile();
	}

	/**
	 * Given the path of a torrent, parse the file and represent it as a Map
	 * 
	 * @param filename String
	 * @return Map
	 */
	@SuppressWarnings("rawtypes")
	public Map parseTorrent(String filename) {
		return this.parseTorrent(new File(filename));
	}

	/**
	 * Given a File (supposed to be a torrent), parse it and represent it as a
	 * Map
	 * 
	 * @param file File
	 * @return Map
	 */
	@SuppressWarnings("rawtypes")
	private Map parseTorrent(File file) {
		Map returnVal = null;

		try {
			//See if the file is in GZIP format.  If so, read it in
			InputStream gzip = new GZIPInputStream(new FileInputStream(file));
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			int len;
			while ((len = gzip.read(buf)) != -1) {
				out.write(buf, 0, len);
			}

			returnVal = BDecoder.decode(out.toByteArray());
			out.close();
			gzip.close();
		} catch (FileNotFoundException e) {
			Log.e(TAG, "File Not found");
		} catch (IOException e) {
			try {
				//File is not compressed
				returnVal = BDecoder.decode(IOManager.readBytesFromFile(file));
			} catch (IOException ioe) {
				Log.e(TAG, "Error reading file");
			}
		}

		return returnVal;
	}

	/**
	 * Given a Map, retrieve all useful information and represent it as a
	 * TorrentFile object
	 * 
	 * @param m Map
	 * @return TorrentFile
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public TorrentFile getTorrentFile(Map m) {
		if (m == null)
			return null;
		if (m.containsKey("announce")) { // mandatory key 
			this.torrent.getAnnounceURLs().add(new String((byte[]) m.get("announce")));
		} else {
			return null;
		}
		if (m.containsKey("announce-list")) { // optional key
			try {
				List<ArrayList<byte[]>> list = (List<ArrayList<byte[]>>) m.get("announce-list");
				for (int l = 0; l < list.size(); l++) {
					this.torrent.getAnnounceURLs().add(new String((byte[]) list.get(l).get(0)));
				}
			} catch (Exception e) {
				Log.e(TAG, "Unable to parse announce-list");
			}
		}

		if (m.containsKey("comment")) { // optional key
			try {
				this.torrent.setComment(new String((byte[]) m.get("comment")));
			} catch (ClassCastException e) {
				this.torrent.setComment(null);
			}
		}
		if (m.containsKey("created by")) { // optional key
			try {
				this.torrent.setCreatedBy(new String((byte[]) m.get("created by")));
			} catch (ClassCastException e) {
				this.torrent.setCreatedBy(null);
			}
		}
		if (m.containsKey("creation date")) { // optional key
			try {
				this.torrent.setCreationDate((Long) m.get("creation date"));
			} catch (ClassCastException e) {
				this.torrent.setCreationDate(0);
			}
		}

		//Store the info field data
		if (m.containsKey("info")) {
			Map info = (Map) m.get("info");
			try {

				this.torrent.setInfoHashAsBinary(Utils.hash(BEncoder.encode(info)));
				this.torrent.setInfoHashAsHex(Utils.toString(this.torrent.getInfoHashAsBinary()));
				this.torrent.setInfoHashAsURL(Utils.toURLString(this.torrent.getInfoHashAsBinary()));
			} catch (IOException ioe) {
				return null;
			}
			//			if (info.containsKey("name")) {
			//				this.torrent.savePath = String.valueOf((byte[]) info.get("name"));
			//			}
			if (info.containsKey("piece length")) {
				this.torrent.setPieceLength(Integer.parseInt(String.valueOf(info.get("piece length"))));
			} else {
				return null;
			}

			if (info.containsKey("pieces")) {
				byte[] piecesHash2 = (byte[]) info.get("pieces");
				if (piecesHash2.length % 20 != 0)
					return null;

				for (int i = 0; i < piecesHash2.length / 20; i++) {
					byte[] temp = Utils.subArray(piecesHash2, i * 20, 20);
					this.torrent.getPieceHashAsBinary().add(temp);
					this.torrent.getPieceHashAsHex().add(Utils.toString(temp));
					this.torrent.getPieceHashAsURL().add(Utils.toURLString(temp));
				}
			} else
				return null;

			//this.torrent.fileNames.clear();
			if (info.containsKey("files")) {
				List multFiles = (List) info.get("files");
				this.torrent.setTotalLength(0);
				for (int i = 0; i < multFiles.size(); i++) {
					List path = (List) ((Map) multFiles.get(i)).get("path");
					StringBuffer filePath = new StringBuffer();
					for (int j = 0; j < path.size(); j++) {
						filePath.append(new String((byte[]) path.get(j)));
					}
					if (!this.torrent.getFileNames().contains(filePath.toString())) {
						this.torrent.getFileNames().add(filePath.toString());
						this.torrent.getLength().add(((Long) ((Map) multFiles.get(i)).get("length")).intValue());
						this.torrent.setTotalLength(this.torrent.getTotalLength() + ((Long) ((Map) multFiles.get(i)).get("length")).intValue());
					}
				}
			} else {
				String filePath = new String((byte[]) info.get("name"));
				if (!this.torrent.getFileNames().contains(filePath)) {
					this.torrent.getLength().add(((Long) info.get("length")).intValue());
					this.torrent.setTotalLength(((Long) info.get("length")).intValue());
					this.torrent.getFileNames().add(new String((byte[]) info.get("name")));
				}
			}
		} else {
			return null;
		}
		return this.torrent;
	}

	/**
	 * Sets the TorrentFile object of the Publisher equals to the given one
	 * 
	 * @param torr TorrentFile
	 */
	public void setTorrent(TorrentFile torr) {
		this.torrent = torr;
	}

	/**
	 * Returns the local TorrentFile in its current state
	 * 
	 * @return TorrentFile
	 */
	public TorrentFile getTorrent() {
		return this.torrent;
	}
}
