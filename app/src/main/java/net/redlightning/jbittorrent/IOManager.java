/*
 * Java Bittorrent API as its name indicates is a JAVA API that implements the
 * Bittorrent Protocol This project contains two packages: 1. jBittorrentAPI is
 * the "client" part, i.e. it implements all classes needed to publish files,
 * share them and download them. This package also contains example classes on
 * how a developer could create new applications. 2. trackerBT is the "tracker"
 * part, i.e. it implements a all classes needed to run a Bittorrent tracker
 * that coordinates peers exchanges. * Copyright (C) 2007 Baptiste Dubuis,
 * Artificial Intelligence Laboratory, EPFL This file is part of
 * jbittorrentapi-v1.0.zip Java Bittorrent API is free software and a free user
 * study set-up; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version. Java
 * Bittorrent API is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with Java Bittorrent API; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * @version 1.0
 * @author Baptiste Dubuis To contact the author: email:
 * baptiste.dubuis@gmail.com More information about Java Bittorrent API:
 * http://sourceforge.net/projects/bitext/
 */

package net.redlightning.jbittorrent;

import java.io.*;
import android.util.Log;

/**
 * Utility methods for I/O operations
 * 
 * @author Baptiste Dubuis (baptiste.dubuis@gmail.com)
 * @author Michael Isaacson (michael@redlightning.net)
 * @version 0.2
 */
public class IOManager {
	private static final String TAG = IOManager.class.getSimpleName();

	/**
	 * Read all available bytes from the given file
	 * 
	 * @param file File
	 * @return byte[]
	 */
	protected static byte[] readBytesFromFile(final File file) {
		long file_size_long = -1;
		byte[] file_bytes = null;
		InputStream stream = null;

		try {
			stream = new FileInputStream(file);

			if (!file.exists()) {
				Log.e(TAG, "Error: The file \"" + file.getName() + "\" does not exist. Please make sure you have the correct path to the file.");
				return null;
			}

			if (!file.canRead()) {
				Log.e(TAG, "Error: Cannot read from \"" + file.getName() + "\". Please make sure the file permissions are set correctly.");
				return null;
			}

			file_size_long = file.length();

			if (file_size_long > Integer.MAX_VALUE) {
				Log.e(TAG, "Error: The file \"" + file.getName() + "\" is too large to be read by this class.");
				return null;
			}
			file_bytes = new byte[(int) file_size_long];
			int file_offset = 0;
			int bytes_read = 0;

			while (file_offset < file_bytes.length && (bytes_read = stream.read(file_bytes, file_offset, file_bytes.length - file_offset)) >= 0) {
				file_offset += bytes_read;
			}
			if (file_offset < file_bytes.length) {
				throw new IOException("Could not completely read file \"" + file.getName() + "\".");
			}

		} catch (FileNotFoundException e) {
			Log.e(TAG, "Error: The file \"" + file.getName() + "\" does not exist. Please make sure you have the correct path to the file.");
			return null;
		} catch (IOException e) {
			Log.e(TAG, "Error: There was a general, unrecoverable I/O error while reading from \"" + file.getName() + "\".");
			Log.e(TAG, e.getMessage());
		} finally {
			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
					Log.e(TAG, e.getMessage());
				}
			}
		}
		return file_bytes;
	}
}
