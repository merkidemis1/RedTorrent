/*
 * BEncoder.java Created on June 4, 2003, 10:17 PM Copyright (C) 2003, 2004,
 * 2005, 2006 Aelitis, All Rights Reserved. This program is free software; you
 * can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. This program is
 * distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details. You
 * should have received a copy of the GNU General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place
 * - Suite 330, Boston, MA 02111-1307, USA. AELITIS, SAS au capital de 46,603.30
 * euros 8 Allee Lenotre, La Grille Royale, 78600 Le Mesnil le Roi, France.
 */

package net.redlightning.jbittorrent;

import java.io.*;
import java.nio.*;
import java.util.*;
import android.util.Log;

/**
 * A set of utility methods to encode a Map into a bencoded array of byte.
 * integer are represented as Long, String as byte[], dictionnaries as Map, and
 * list as List.
 * 
 * @author TdC_VgA
 */
public class BEncoder {
	private static final String TAG = BEncoder.class.getSimpleName();

	/**
	 * 
	 * @param object
	 * @return
	 * @throws IOException
	 */
	public static byte[] encode(final Map<?,?> object) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		new BEncoder().encode(baos, object);
		return baos.toByteArray();
	}

	/**
	 * 
	 * @param baos
	 * @param object
	 * @throws IOException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void encode(final ByteArrayOutputStream baos, final Object object) throws IOException {
		if (object instanceof String || object instanceof Float) {
			final String tempString = (object instanceof String) ? (String) object : String.valueOf((Float) object);
			final ByteBuffer buffer = Utils.DEFAULT_CHARSET.encode(tempString);
			write(baos, Utils.DEFAULT_CHARSET.encode(String.valueOf(buffer.limit())));
			baos.write(':');
			write(baos, buffer);
		} else if (object instanceof Map<?,?>) {
			Map<?,?> tempMap = (Map<?,?>) object;
			SortedMap<?,?> tempTree = null;

			// unfortunately there are some occasions where we want to ensure that
			// the 'key' of the map is not mangled by assuming its UTF-8 encodable.
			// In particular the response from a tracker scrape request uses the
			// torrent hash as the KEY. Hence the introduction of the type below
			// to allow the constructor of the Map to indicate that the keys should
			// be extracted using a BYTE_ENCODING

			boolean byte_keys = false; //object instanceof ByteEncodedKeyHashMap;

			//write the d
			baos.write('d');

			//are we sorted?
			if (tempMap instanceof TreeMap<?,?>) {
				tempTree = (TreeMap<?,?>) tempMap;
			} else {
				//do map sorting here
				tempTree = new TreeMap(tempMap);
			}

			Iterator<?> it = tempTree.entrySet().iterator();

			while (it.hasNext()) {
				Map.Entry entry = (Map.Entry) it.next();
				Object o_key = entry.getKey();
				Object value = entry.getValue();

				if (value != null) {
					if (o_key instanceof byte[]) {
						encode(baos, (byte[]) o_key);
						encode(baos, value);
					} else {
						final String key = (String) o_key;

						if (byte_keys) {
							try {
								encode(baos, Utils.BYTE_CHARSET.encode(key));
								encode(baos, tempMap.get(key));
							} catch (UnsupportedEncodingException e) {
								throw (new IOException("BEncoder: unsupport encoding: " + e.getMessage()));
							}
						} else {
							encode(baos, key); // Key goes in as UTF-8
							encode(baos, value);
						}
					}
				}
			}

			baos.write('e');
		} else if (object instanceof List<?>) {
			List<Object> tempList = (List<Object>) object;

			//write out the l

			baos.write('l');

			for (int i = 0; i < tempList.size(); i++) {
				encode(baos, tempList.get(i));
			}

			baos.write('e');

		} else if (object instanceof Long) {
			Long tempLong = (Long) object;
			//write out the l
			baos.write('i');
			write(baos, Utils.DEFAULT_CHARSET.encode(tempLong.toString()));
			baos.write('e');
		} else if (object instanceof Integer) {
			Integer tempInteger = (Integer) object;
			//write out the l
			baos.write('i');
			write(baos, Utils.DEFAULT_CHARSET.encode(tempInteger.toString()));
			baos.write('e');
		} else if (object instanceof byte[]) {
			byte[] tempByteArray = (byte[]) object;
			write(baos, Utils.DEFAULT_CHARSET.encode(String.valueOf(tempByteArray.length)));
			baos.write(':');
			baos.write(tempByteArray);
		} else if (object instanceof ByteBuffer) {
			ByteBuffer buffer = (ByteBuffer) object;
			write(baos, Utils.DEFAULT_CHARSET.encode(String.valueOf(buffer.limit())));
			baos.write(':');
			write(baos, buffer);
		}
	}

	/**
	 * 
	 * @param stream
	 * @param buffer
	 * @throws IOException
	 */
	private void write(final OutputStream stream, final ByteBuffer buffer) throws IOException {
		stream.write(buffer.array(), 0, buffer.limit());
	}

	/**
	 * 
	 * @param object1
	 * @param object2
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static boolean objectsAreIdentical(final Object object1, final Object object2) {
		Object comp1, comp2;
		comp1 = object1;
		comp2 = object2;

		if (comp1 == null && comp2 == null) {
			return (true);
		} else if (comp1 == null || comp2 == null) {
			return (false);
		}
		if (comp1 instanceof Integer) {
			comp1 = Long.valueOf((Integer) comp1);
		}
		if (comp2 instanceof Integer) {
			comp2 = Long.valueOf((Integer) comp2);
		}
		if (comp1 instanceof Float) {
			comp1 = String.valueOf((Float) comp1);
		}
		if (comp2 instanceof Float) {
			comp2 = String.valueOf((Float) comp2);
		}
		if (comp1.getClass() != comp2.getClass()) {
			return (false);
		}

		if (comp1 instanceof Long) {
			return (comp1.equals(comp2));
		} else if (comp1 instanceof byte[]) {
			return (Arrays.equals((byte[]) comp1, (byte[]) comp2));
		} else if (comp1 instanceof ByteBuffer) {
			return (comp1.equals(comp2));
		} else if (comp1 instanceof String) {
			return (comp1.equals(comp2));
		} else if (comp1 instanceof List<?>) {
			return (listsAreIdentical((List<Object>) comp1, (List<Object>) comp2));
		} else if (comp1 instanceof Map<?,?>) {
			return (mapsAreIdentical((Map<Object, Object>) comp1, (Map<Object, Object>) comp2));
		} else {
			Log.e(TAG, "Invalid type: " + comp1);
			return (false);
		}
	}

	/**
	 * 
	 * @param list1
	 * @param list2
	 * @return
	 */
	public static boolean listsAreIdentical(final List<?> list1, final List<?> list2) {
		if (list1 == null && list2 == null) {
			return (true);
		} else if (list1 == null || list2 == null) {
			return (false);
		}

		if (list1.size() != list2.size()) {
			return (false);
		}

		for (int i = 0; i < list1.size(); i++) {
			if (!objectsAreIdentical(list1.get(i), list2.get(i))) {
				return (false);
			}
		}
		return (true);
	}

	/**
	 * 
	 * @param map1
	 * @param map2
	 * @return
	 */
	public static boolean mapsAreIdentical(final Map<Object, Object> map1, final Map<Object, Object> map2) {
		if (map1 == null && map2 == null) {
			return (true);
		} else if (map1 == null || map2 == null) {
			return (false);
		}

		if (map1.size() != map2.size()) {
			return (false);
		}

		Iterator<Object> it = map1.keySet().iterator();

		while (it.hasNext()) {
			Object key = it.next();
			Object object1 = map1.get(key);
			Object object2 = map2.get(key);

			if (!objectsAreIdentical(object1, object2)) {
				return (false);
			}
		}

		return (true);
	}
}