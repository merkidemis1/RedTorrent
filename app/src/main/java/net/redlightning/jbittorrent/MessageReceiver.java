/*
 * Java Bittorrent API as its name indicates is a JAVA API that implements the
 * Bittorrent Protocol This project contains two packages: 1. jBittorrentAPI is
 * the "client" part, i.e. it implements all classes needed to publish files,
 * share them and download them. This package also contains example classes on
 * how a developer could create new applications. 2. trackerBT is the "tracker"
 * part, i.e. it implements a all classes needed to run a Bittorrent tracker
 * that coordinates peers exchanges. * Copyright (C) 2007 Baptiste Dubuis,
 * Artificial Intelligence Laboratory, EPFL This file is part of
 * jbittorrentapi-v1.0.zip Java Bittorrent API is free software and a free user
 * study set-up; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version. Java
 * Bittorrent API is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with Java Bittorrent API; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * http://sourceforge.net/projects/bitext/
 */

package net.redlightning.jbittorrent;

import java.io.*;
import android.util.Log;
import net.redlightning.jbittorrent.interfaces.AbstractMessage;

/**
 * Thread created to listen for incoming message from remote peers. When data is
 * read, message type is determined and a new Message object (either Message_HS
 * or Message_PP) is created and passed to the corresponding receiver
 * 
 * @author Baptiste Dubuis (baptiste.dubuis@gmail.com)
 * @author Michael Isaacson (michael@redlightning.net)
 * @version 11.3.3
 */
public class MessageReceiver extends Thread {
	private static final String TAG = MessageReceiver.class.getSimpleName();
	private boolean run = true;
	private InputStream input = null;
	private DataInputStream dis = null;
	private boolean hsOK = false;
	private final DownloadTask task;

	/**
	 * Create a new Message receiver for a given peer
	 * 
	 * @param id The id of the peer that has been assigned this receiver
	 * @param stream InputStream
	 * @throws IOException
	 */
	protected MessageReceiver(final DownloadTask manager, final String id, final InputStream stream) throws IOException {
		super("MessageReceiver");
		input = stream;
		dis = new DataInputStream(stream);
		task = manager;
	}

	/**
	 * Reads bytes from the DataInputStream
	 * 
	 * @param data byte[]
	 * @return int
	 */
	private int read(final byte[] data) {
		try {
			dis.readFully(data);
		} catch (final IOException ioe) {
			return -1;
		}
		return data.length;
	}

	/**
	 * Reads data from the inputstream, creates new messages according to the
	 * received data and fires MessageReceived method of the listeners with the
	 * new message in parameter. Loops as long as the 'run' variable is true
	 */
	@Override
	public void run() {
		final byte[] lengthHS = new byte[1];
		final byte[] protocol = new byte[19];
		final byte[] reserved = new byte[8];
		final byte[] fileID = new byte[20];
		final byte[] peerID = new byte[20];
		final byte[] length = new byte[4];

		HandshakeMessage hs = new HandshakeMessage();
		PeerProtocolMessage mess = new PeerProtocolMessage();

		while (run) {
			int l = 1664;
			try {
				if (!hsOK) {
					if (read(lengthHS) > 0) {
						for (int i = 0; i < 19; i++) {
							protocol[i] = (byte) input.read();
						}
						for (int i = 0; i < 8; i++) {
							reserved[i] = (byte) input.read();
						}
						for (int i = 0; i < 20; i++) {
							fileID[i] = (byte) input.read();
						}
						for (int i = 0; i < 20; i++) {
							peerID[i] = (byte) input.read();
						}

						hs = new HandshakeMessage(lengthHS, protocol, reserved, fileID, peerID);
					} else {
						hs = null;
					}
				} else {
					//TODO Check reserved bits to see what type of message this is

					int id;
					if (read(length) > 0) {
						l = Utils.toInt(length);
						if (l == 0) {
							mess.setData(Peer.KEEP_ALIVE);
						} else {
							id = input.read();
							if (id == -1) {
								mess = null;
							} else {
								if (l == 1) {
									mess.setData(id + 1);
								} else {
									l = l - 1;
									byte[] payload = new byte[l];
									if (read(payload) > 0) {
										mess.setData(id + 1, payload);
									}
									payload = null;
								}
							}
						}
					} else {
						mess = null;
					}
				}
			} catch (final IOException ioe) {
				ioe.printStackTrace();
				fireMessageReceived(null);
				return;
			} catch (final Exception e) {
				Log.e(TAG, l + "Error in MessageReceiver..." + e.getMessage() + " " + e.toString());
				fireMessageReceived(null);
				return;
			}

			if (!hsOK) {
				fireMessageReceived(hs);
				hsOK = true;
			} else {
				fireMessageReceived(mess);
			}
		}
		try {
			dis.close();
			dis = null;
		} catch (final Exception e) {
			//Log.e(TAG, "Error with message: " + e.toString());
		}
		try {
			input.close();
			input = null;
		} catch (final Exception e) {
			//Log.e(TAG, "Error with message: " + e.toString());
		}
	}

	//	/**
	//	 * @param listener the listener to add
	//	 */
	//	protected void addIncomingListener(IncomingListener listener) {
	//		listeners.add(listener);
	//	}

	/**
	 * @param message the message to fire
	 */
	private void fireMessageReceived(final AbstractMessage message) {
		task.messageReceived(message);
	}

	/**
	 * Stops the current thread by completing the run() method
	 */
	protected void stopThread() {
		run = false;
	}
}
