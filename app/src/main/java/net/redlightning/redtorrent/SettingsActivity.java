/*
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent;

import net.redlightning.redtorrent.common.SSIDManageListener;
import net.redlightning.redtorrent.common.Settings;
import net.redlightning.redtorrent.common.TorrentActivity;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;


/**
 * Activity for displaying and changing settings
 * 
 * @author Michael Isaacson
 * @version 17.8.29
 */
public class SettingsActivity extends Settings {

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(final Bundle icicle) {
		super.onCreate(icicle);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.settings);

		((ImageView) findViewById(R.id.div)).setImageDrawable(TorrentActivity.GRADIENT);
		((ImageView) findViewById(R.id.div2)).setImageDrawable(TorrentActivity.GRADIENT);
		((ImageView) findViewById(R.id.div3)).setImageDrawable(TorrentActivity.GRADIENT);

		setupWiFi();
		setupTimeframe();
		setupPluggedIn();
		setupOptions();
		setupSliders();
	}

	/**
	 * Setup options for the ongoing notification, download location, and ports
	 */
	private void setupOptions() {
		//Use Ongoing
		useOngoingCheckbox = findViewById(R.id.use_ongoing);
		useOngoingCheckbox.setOnClickListener(useOngoingClickListener);
		useOngoingCheckbox.setChecked(Settings.useOngoing());

		//Download Location
		savePathClassName = "net.redlightning.redtorrent.SavePathBrowser";
		downloadPathEdit = findViewById(R.id.download_path);
		downloadPathEdit.setOnEditorActionListener(pathEditListener);
		downloadPathEdit.setOnFocusChangeListener(pathFocusListener);
		downloadPathEdit.setImeActionLabel("Save", EditorInfo.IME_ACTION_DONE);
		downloadPathEdit.setText(Settings.getDownloadPath());
		downloadButton = findViewById(R.id.download_button);
		downloadButton.setOnClickListener(pathSearchClickListener);

		//Ports
		lowPortEdit = findViewById(R.id.low_port);
		lowPortEdit.setText(String.valueOf(Settings.getLowPort()));
		lowPortEdit.setOnFocusChangeListener(portFocusListener);
		lowPortEdit.setOnEditorActionListener(portEditListener);
		lowPortEdit.setImeActionLabel(getString(R.string.save), EditorInfo.IME_ACTION_DONE);

		highPortEdit = findViewById(R.id.high_port);
		highPortEdit.setText(String.valueOf(Settings.getHighPort()));
		highPortEdit.setOnFocusChangeListener(portFocusListener);
		highPortEdit.setOnEditorActionListener(portEditListener);
		highPortEdit.setImeActionLabel(getString(R.string.save), EditorInfo.IME_ACTION_DONE);
	}

	/**
	 * Setup controls for restricting activity to when the device is plugged in
	 */
	private void setupPluggedIn() {
		//Plugged In Only Mode
		pluggedInOnlyCheckbox = findViewById(R.id.plugged_only);
		pluggedInOnlyCheckbox.setOnClickListener(pluggedInClickListener);
		pluggedInOnlyCheckbox.setChecked(Settings.isPluggedInOnly());

		//Resume on Plugged In
		pluggedInResumeCheckbox = findViewById(R.id.plugged_resume);
		pluggedInResumeCheckbox.setOnClickListener(pluggedInResumeClickListener);
		pluggedInResumeCheckbox.setChecked(Settings.isPluggedInResume());

//		if (pluggedInResumeCheckbox.isChecked()) {
//			pluggedInResumeCheckbox.setVisibility(View.VISIBLE);
//		} else {
//			pluggedInResumeCheckbox.setVisibility(View.GONE);
//		}
	}

	/**
	 * Set up sliders for max active, max pieces, and max peers
	 */
	private void setupSliders() {
		//Max Active slider
		maxActiveText = findViewById(R.id.max_active_count);
		maxActiveText.setText(String.valueOf(Settings.getMaxActive()));
		maxActiveBar = findViewById(R.id.max_active_bar);
		maxActiveBar.setProgress(Settings.getMaxActive() - 1);
		maxActiveBar.setOnSeekBarChangeListener(maxActiveChangeListener);

		//Pieces slider
		piecesText = findViewById(R.id.pieces_count);
		piecesText.setText(String.valueOf(Settings.getMaxPieces()));
		piecesBar = findViewById(R.id.pieces_bar);
		piecesBar.setProgress((Settings.getMaxPieces() - 5) / 5);
		piecesBar.setOnSeekBarChangeListener(piecesChangeListener);

		//Peers slider
		connectionsText = findViewById(R.id.connections_count);
		connectionsText.setText(String.valueOf(Settings.getMaxConnections()));
		connectionsBar = findViewById(R.id.connections_bar);
		connectionsBar.setProgress((Settings.getMaxConnections() - 10) / 10);
		connectionsBar.setOnSeekBarChangeListener(maxConnectionsChangeListener);
	}

	/**
	 * Configure controls for limiting activity to a specific timeframe
	 */
	private void setupTimeframe() {
		//Timeframe Only
		timeframeOnlyCheckbox = findViewById(R.id.timeframe_only);
		timeframeOnlyCheckbox.setOnClickListener(timeframeClickListener);
		timeframeOnlyCheckbox.setChecked(Settings.isTimeframeOnly());

		startTimeLabel = findViewById(R.id.startTimeLabel);
		startTimePicker = findViewById(R.id.startTimePicker);
		startTimePicker.setHour(Settings.getStartHour());
		startTimePicker.setMinute(Settings.getStartMinute());
		startTimePicker.setOnTimeChangedListener(startTimeListener);

		endTimeLabel = findViewById(R.id.endTimeLabel);
		endTimePicker = findViewById(R.id.endTimePicker);
		endTimePicker.setHour(Settings.getEndHour());
		endTimePicker.setMinute(Settings.getEndMinute());
		endTimePicker.setOnTimeChangedListener(endTimeListener);

		//Resume on timeframe
		timeframeResumeCheckbox = findViewById(R.id.timeframe_resume);
		timeframeResumeCheckbox.setOnClickListener(timeframeResumeClickListener);
		timeframeResumeCheckbox.setChecked(Settings.isTimeframeResume());
		if (timeframeOnlyCheckbox.isChecked()) {
			startTimeLabel.setVisibility(View.VISIBLE);
			endTimeLabel.setVisibility(View.VISIBLE);
			timeframeResumeCheckbox.setVisibility(View.VISIBLE);
			startTimePicker.setVisibility(View.VISIBLE);
			endTimePicker.setVisibility(View.VISIBLE);
		} else {
			startTimeLabel.setVisibility(View.GONE);
			endTimeLabel.setVisibility(View.GONE);
			timeframeResumeCheckbox.setVisibility(View.GONE);
			startTimePicker.setVisibility(View.GONE);
			endTimePicker.setVisibility(View.GONE);
		}
	}

	/**
	 * Setup wifi controls
	 */
	private void setupWiFi() {
		//WiFi Only Mode
		wifiOnlyCheckbox = findViewById(R.id.wifi_only);
		wifiOnlyCheckbox.setOnClickListener(wifiClickListener);
		wifiOnlyCheckbox.setChecked(Settings.isWifiOnly());

		//Resume on WiFi
		wifiResumeCheckbox = findViewById(R.id.wifi_resume);
		wifiResumeCheckbox.setOnClickListener(resumeClickListener);
		wifiResumeCheckbox.setChecked(Settings.isWifiResume());
//		if (wifiOnlyCheckbox.isChecked()) {
//			wifiResumeCheckbox.setVisibility(View.VISIBLE);
//		} else {
//			wifiResumeCheckbox.setVisibility(View.GONE);
//		}

		//WiFi Networks
		wifiNetworksButton = findViewById(R.id.wifi_manage);
		wifiNetworksButton.setOnClickListener(new SSIDManageListener(this, SSIDActivity.class));
		wifiNetworksCheckbox = findViewById(R.id.wifi_networks);
		wifiNetworksCheckbox.setOnClickListener(wifiNetworksClickListener);
		wifiNetworksCheckbox.setChecked(Settings.isWifiNetworks());
		if (wifiNetworksCheckbox.isChecked()) {
			wifiNetworksButton.setVisibility(View.VISIBLE);
		} else {
			wifiNetworksButton.setVisibility(View.GONE);
		}
	}
}