/*
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import net.redlightning.redtorrent.common.PowerStateReceiver;
import net.redlightning.redtorrent.common.TorrentService;
import net.redlightning.redtorrent.common.WifiStateReceiver;

/**
 * Handles startup for the background service
 * 
 * @author Michael Isaacson
 * @version 17.8.29
 */
public class RedService extends TorrentService {
	/*
	 * (non-Javadoc)
	 * @see android.app.Service#onStart(android.content.Intent, int)
	 */
	@Override
	public void onStart(final Intent intent, final int startId) {
		pendingIntent = PendingIntent.getActivity(context, 0, new Intent(context, RedTorrent.class), 0);
		context = this.getApplicationContext();
		wiFiReceiver = new WifiStateReceiver();
		powerReceiver = new PowerStateReceiver();
		notifyManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		//Add actions
		final IntentFilter wifiFilter = new IntentFilter();
		final IntentFilter powerFilter = new IntentFilter();
		wifiFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
		wifiFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
		powerFilter.addAction(Intent.ACTION_POWER_CONNECTED);
		powerFilter.addAction(Intent.ACTION_POWER_DISCONNECTED);

		registerReceiver(wiFiReceiver, wifiFilter);
		registerReceiver(powerReceiver, powerFilter);

		final WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(WIFI_SERVICE);
		ipAddress = wifiManager.getConnectionInfo().getIpAddress();
		loadSettings(context);
		parseTorrents();
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Service#onCreate()
	 */
	@Override
	public void onCreate() {
		context = getBaseContext();
		parseTorrents();
	}
}