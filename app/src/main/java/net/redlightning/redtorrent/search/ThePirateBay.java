/*
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import net.redlightning.json.JSONArray;
import net.redlightning.json.JSONException;
import net.redlightning.json.JSONObject;
import android.os.Handler;
import android.util.Log;

/**
 * Runs a search against search.thepiratebay.org and parses the response HTML into a JSON object
 * 
 * @author Michael Isaacson
 * @version 11.12.21
 */
public class ThePirateBay extends Thread {
	private static final String TAG = ThePirateBay.class.getCanonicalName();
	private transient String query;
	private transient Handler handler;

	/**
	 * Constructor
	 * 
	 * @param query the item to search for
	 * @param handler the handler to send messages to
	 */
	public ThePirateBay(final String query, final Handler handler) {
		super("The Pirate Bay");
		this.query = query;
		this.handler = handler;
	}

	/**
	 * @return the Handler object
	 */
	public Handler getHandler() {
		return handler;
	}

	/**
	 * @return the current query
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * Parse the HTML from thepiratebay.org into a JSON object
	 * 
	 * @param response the HTML containing the search results
	 * @return the search results as a JSON conplex object
	 */
	private JSONObject parseResponse(final String response) {
		JSONObject returnVal = null;
		JSONObject json = null;

		try {
			returnVal = new JSONObject();
			json = new JSONObject();
			final JSONArray array = new JSONArray();

			//Parse the HTML and build the JSON objects for display
			final String[] rows = response.split("<tr");
			for (final String row : rows) {
				final JSONObject item = new JSONObject();
				if (row.contains("detName")) {
					boolean seedsFound = false;
					final String[] cells = row.split("<td");
					for (final String cell : cells) {
						if (cell.contains("detName")) {
							String title = cell.replace("</td>", "").replace("<div class=\"detName\">", "").trim();
							title = title.substring(title.indexOf(">") + 1);
							title = title.substring(title.indexOf(">") + 1);
							title = title.substring(0, title.indexOf("</a>"));
							item.put("title", title);

							String link = cell.replace("</td>", "").replace("<div class=\"detName\">", "").trim();
							link = link.substring(link.indexOf("<a href=\"http://torrents.thepiratebay.org"));
							link = link.substring(9);
							link = link.substring(0, link.indexOf("\""));
							item.put("enclosure_url", link);

							String size = cell.replace("</td>", "").replace("<div class=\"detName\">", "").trim();
							size = size.substring(size.indexOf("<font class=\"detDesc\">Uploaded"));
							size = size.substring(size.indexOf("Size ") + 5);
							size = size.substring(0, size.indexOf(", "));
							size = size.replace("&nbsp;", " ");
							item.put("size", size);
						} else if (cell.contains("align=\"right\">")) {
							final String clean = cell.replace("align=\"right\">", "").replace("</td>", "").replace("</tr>", "").trim();
							if (seedsFound) {
								item.put("leechers", Integer.parseInt(clean.trim()));
							} else {
								item.put("Seeds", Integer.parseInt(clean.trim()));
								seedsFound = true;
							}
						}
					}
				}
				array.put(item);
			}

			json.put("list", array);
			returnVal.put("items", json);
		} catch (final JSONException e) {
			Log.e(TAG, "JSONException: " + e.getMessage());
		}
		return returnVal;
	}

	/*
	 * (non-Javadoc)
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 */
	@Override
	public void run() {
		final JSONObject results = search();
		if ((results == null) && (handler != null)) {
			handler.sendMessage(handler.obtainMessage(TorrentSearch.ERROR, TorrentSearch.ERROR_MESSAGE));
		} else if (handler != null) {
			handler.sendMessage(handler.obtainMessage(TorrentSearch.SUCCESS, results));
		}
	}

	/**
	 * Submit the search query to isoHunt, and retrieve the results
	 * 
	 * @return the results of the search, null if there was an error
	 */
	public JSONObject search() {
		JSONObject returnVal = null;
		try {
			final URL source = new URL("http://www.thepiratebay.org/search/" + URLEncoder.encode(query) + "/0/7/0");
			final URLConnection uc = source.openConnection();
			final BufferedReader inputStream = new BufferedReader(new InputStreamReader(uc.getInputStream()));

			final StringBuilder buffer = new StringBuilder();
			String read = inputStream.readLine();
			boolean found = false;
			while (read != null) {
				if (read.contains("searchResult")) {
					found = true;
				}
				if (found) {
					buffer.append(read);
					buffer.append("\n");
				}
				read = inputStream.readLine();
			}

			inputStream.close();
			returnVal = parseResponse(buffer.toString().substring(0, buffer.toString().indexOf("</table>")));
		} catch (final MalformedURLException e) {
			Log.e(TAG, "MalformedURLException: " + e.getMessage());
		} catch (final IOException e) {
			Log.e(TAG, "IOException: " + e.getMessage());
		} catch (final StringIndexOutOfBoundsException e) {
			Log.i(TAG, "Search returned an empty set.");
		}
		return returnVal;
	}
}