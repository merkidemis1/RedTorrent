/*
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.common;

import java.util.List;
import java.util.Map;

import net.redlightning.dht.vuze.DHTAnnounceResult;
import net.redlightning.dht.vuze.DHTScrapeResult;
import net.redlightning.jbittorrent.DownloadManager;
import net.redlightning.jbittorrent.TorrentFile;
import net.redlightning.jbittorrent.TorrentProcessor;
import android.content.Intent;
import android.os.Looper;
import android.util.Log;

/**
 * @author Michael Isaacson
 * @version 11.3.8
 */
public class Downloader extends Thread {
	private static final String TAG = Downloader.class.getSimpleName();
	private static final String COMPLETE_INTENT = "net.redlightning.redtorrent.COMPLETE";
	private static final String ERROR_INTENT = "net.redlightning.redtorrent.ERROR";
	private TorrentFile torrent;
	private transient DownloadManager manager;
	private transient float rate;
	private transient float percentDone;
	private boolean complete = false;
	private int currentState;
	private transient String progress;
	private ValidationTask fileSetupTask = new ValidationTask();
	//public static final int ST_WAITING = 1; // waiting to be told to start preparing
	//public static final int ST_PREPARING = 2; // getting files ready (allocating/checking)
	//public static final int ST_READY = 3; // ready to be started if required
	public static final int ST_DOWNLOADING = 4;// downloading   
	public static final int ST_SEEDING = 5;// seeding   
	//public static final int ST_STOPPING = 6; // stopping
	//public static final int ST_STOPPED = 7;// stopped, do not auto-start!
	//public static final int ST_ERROR = 8; // failed
	public static final int ST_QUEUED = 9;// stopped, but ready for auto-starting   

	//public static final String[] ST_NAMES = { "", "Waiting", "Preparing", "Ready", "Downloading", "Seeding", "Stopping", "Stopped", "Error", "Queued", };
	private DHTAnnounceResult announceResult;
	private DHTScrapeResult scrapeResult;

	/**
	 * Constructor
	 * 
	 * @param file the path to the .torrent file
	 * @param saveTo the base folder to save the downloaded files to
	 * @param files the UI message handler
	 * @param progress the list of file names the user selected to download
	 */
	public Downloader(final String file, final String saveTo, final List<String> files, final String progress) {
		super("Downloader");
		this.progress = progress;
		torrent = new TorrentFile(file, saveTo, progress);
		torrent.setSelectedFiles(files);
		getManager();
	}

	
	/**
	 * Constructor
	 * 
	 * @param file the path to the .torrent file
	 */
	public Downloader(final TorrentFile file) {
		super("Downloader");
		this.progress = null;
		torrent = file;
		
		getManager();
		//manager.checkTempFiles();
	}
	
	/**
	 * @return the manager
	 */
	public DownloadManager getManager() {
		if (manager == null) {
			final TorrentProcessor processor = new TorrentProcessor(torrent);
			final Map<?, ?> data = processor.parseTorrent(torrent.getTorrentFileName());

			Log.i(TAG, "TorrentFile: " + torrent.getTorrentFileName());
			if (data != null) {
				torrent = processor.getTorrentFile(data);
				torrent.setComplete(progress);
			}
			manager = new DownloadManager(torrent);
		}
		return manager;
	}

	/**
	 * @return the percentDone
	 */
	public float getPercentDone() {
		return percentDone;
	}

	/**
	 * @return the rate
	 */
	public float getRate() {
		return rate;
	}

	/**
	 * @return the torrent
	 */
	public TorrentFile getTorrent() {
		return torrent;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		Looper.prepare();
		if (torrent == null) {
			Log.e(TAG, "Provided file is not a valid torrent file");
			getManager().getTorrent().setStatus("Invalid .torrent file");
			getManager().getTorrent().setErrorMessage("Invalid .torrent file");
			TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.ERROR, torrent.getTorrentFileName()
					+ ",Invalid .torrent file"));
		} else {
			torrent.setSavePath(Settings.getDownloadPath());
			manager = new DownloadManager(torrent);
			fileSetupTask.execute(manager);

			//Try a DHT request
			//DHTHandler.dhts.get(DHTtype.IPV4_DHT).getPeers(new GetPeersRequest(new Key(torrent.getInfoHashAsHex())));
			manager.getTorrent().setComplete(progress);
			manager.updateProgress();
			manager.getTorrent().setStatus("Connecting to Tracker...");
			manager.startListening(Settings.getLowPort(), Settings.getHighPort());
			manager.startTrackerUpdate();

			int loopCount = 0;

			while (!complete) {
				try {
					synchronized (this) {
						wait(1000);
						percentDone = manager.getCompleted();
						rate = manager.getDLRate();
						//ulRate = manager.getULRate();

						if (loopCount == 10) {
							manager.unchokePeers();
							notifyAll();
							loopCount = 0;
						}
						loopCount++;
					}
				} catch (Exception e) {
					Log.e(TAG, "Error handling Torrent: " + e.getMessage());
					TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.ERROR, torrent.getTorrentFileName()
							+ ",Error handling Torrent"));
					complete = true;
					Intent i = new Intent();
					i.setAction(ERROR_INTENT);
					i.getExtras().putString("torrentName", torrent.getTorrentFileName());
					TorrentService.sendIntent(i);
				}
				if (manager == null) {
					complete = true;
				} else {
					if (manager.isComplete()) {
						TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.COMPLETE, torrent.getTorrentFileName()));
						complete = true;
						try {
							Intent i = new Intent();
							i.setAction(COMPLETE_INTENT);
							i.getExtras().putString("torrentName", torrent.getTorrentFileName());
							i.getExtras().putString("downloadPath", torrent.getSavePath());
							TorrentService.sendIntent(i);
						} catch (NullPointerException e) {
							Log.i(TAG, "Unable to send complete notification");
						}
					} else if (manager.getUpdater() != null && manager.getUpdater().isConnected()) {
						TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.DOWNLOADING, torrent.getTorrentFileName()));
					}
				}
			}
		}
	}

	/**
	 * Stop the tracker updates and close any open files
	 */
	public void stopDownloading() {
		//Log.d(TAG, "Stopping " + torrent.getTorrentFileName());
		complete = true;
		fileSetupTask.cancel(true);

		if (manager != null) {
			if (manager.getTorrent() != null) {
				manager.getTorrent().setStatus("Stopped");
			}
			manager.stopListening();
			Log.d(TAG, "Stopping tracker update...");
			manager.stopTrackerUpdate();
			manager.closeTempFiles();
		}
	}

	/**
	 * @return the currentState
	 */
	public int getCurrentState() {
		return currentState;
	}

	/**
	 * @return the complete
	 */
	public synchronized boolean isComplete() {
		return complete;
	}

	/**
	 * @param complete the complete to set
	 */
	public synchronized void setComplete(boolean complete) {
		this.complete = complete;
	}

	ValidationTask getFileSetupTask() {
		return fileSetupTask;
	}

	public void setAnnounceResult(DHTAnnounceResult announceResult) {
		this.announceResult = announceResult;
	}

	public DHTAnnounceResult getAnnounceResult() {
		return announceResult;
	}

	public void setScrapeResult(DHTScrapeResult scrapeResult) {
		this.scrapeResult = scrapeResult;
	}

	public DHTScrapeResult getScrapeResult() {
		return scrapeResult;
	}
}