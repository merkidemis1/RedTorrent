/*
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.common;

import android.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import net.redlightning.jbittorrent.DownloadManager;
import net.redlightning.jbittorrent.TorrentFile;

/**
 * Handle when the user taps on a Torrent on the main activity
 * 
 * @author Michael Isaacson
 * @version 17.9.8
 */
public class TorrentClickListener implements OnClickListener {
	private static final String TAG = TorrentClickListener.class.getSimpleName();
	private static transient CharSequence[] itemList = { "Stop", "Remove" };
	private final transient TorrentActivity context;
	private final transient int position;

	/**
	 * Constructor
	 * 
	 * @param activity the current TorrentActivity
	 * @param pos the view's position in the ListView
	 */
	protected TorrentClickListener(final TorrentActivity activity, final int pos) {
		context = activity;
		position = pos;
	}

	/*
	 * (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(final View view) {
		final AlertDialog dialog = buildDialog();
		if (dialog != null) {
			dialog.show();
		}
	}

	/**
	 * Build the dialog
	 * 
	 * @return the prepared dialog
	 */
	private AlertDialog buildDialog() {
		AlertDialog alertDialog = null;
        final Downloader downloader = TorrentActivity.getAdapter().getItem(position);
        if(downloader != null) {
            final DownloadManager manager = downloader.getManager();
            final TorrentFile current = manager.getTorrent();

            try {
                if ("Stopped".equals(current.getStatus()) || "Queued".equals(current.getStatus())) {
                    itemList[0] = "Start";
                } else {
                    itemList[0] = "Stop";
                }

                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setIcon(android.R.drawable.ic_menu_info_details);
                builder.setTitle(current.getTorrentFileName().substring(current.getTorrentFileName().lastIndexOf('/') + 1, current.getTorrentFileName().indexOf(".torrent")));
                builder.setItems(itemList, new TorrentActionListener(context, position));
                alertDialog = builder.create();
            } catch (IndexOutOfBoundsException e) {
                Log.e(TAG, "Unknown item " + position);
            }

        }
        return alertDialog;
	}
}
