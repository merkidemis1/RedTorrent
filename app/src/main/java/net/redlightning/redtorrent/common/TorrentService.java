/*
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.common;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.redlightning.jbittorrent.TorrentFile;
import net.redlightning.jbittorrent.Utils;
import net.redlightning.jbittorrent.udp.UDPTracker;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

/**
 * Background service that manages tasks for downloading torrents
 * 
 * @author Michael Isaacson
 * @version 17.9.8
 */
public class TorrentService extends Service {
	private static final String TAG = TorrentService.class.getSimpleName();
	private static final String PREFERENCES = "Red Torrent";
	public static final int NOTIFY_ID = 230948524;
	public static final int ONGOING_ID = 230948524;
	public static final int STOP_ALL = 100;
	public static final int START_ALL = 101;
	public static final int ERROR = -1;
	public static final int DOWNLOADING = 0;
	public static final int COMPLETE = 1;
	public static final int STOP = 2;
	public static final int START = 3;
	public static final int QUIT = 4;
	public static final int ALLOCATING = 5;
	public static final int CONNECTING = 7;
	public static final int UPDATE = 8;
	public static final int DHT = 9;
	public static Map<String, Downloader> downloaders = new Hashtable<String, Downloader>();
	public static Handler uiHandler;
	public static SharedPreferences settings;
	public static int activeCount;
	public static String torrents = "";
	protected static Context context;
	protected static int ipAddress;
	protected static PendingIntent pendingIntent;
	protected static transient WifiStateReceiver wiFiReceiver;
	protected static transient PowerStateReceiver powerReceiver;
	protected static NotificationManager notifyManager;

	// Handle messages sent to this service
	private static Handler handler = new Handler() {
		/*
		 * (non-Javadoc)
		 * @see android.os.Handler#handleMessage(android.os.Message)
		 */
		@Override
		public void handleMessage(final Message msg) {
			if ((getDownloaders().size() > 0) && (msg != null)) {
				switch (msg.what) {
					case START:
						Downloader downloader = (Downloader) msg.obj;
						downloader.getTorrent().setStatus("Starting...");
						Log.i(TAG, "Starting Torrent: " + downloader.getTorrent().getTorrentFileName());
						try {
							if (!downloader.isAlive()) {
								downloader.start();
							}
						} catch (final IllegalThreadStateException e) {
							downloader.getManager().startListening(Settings.getLowPort(), Settings.getHighPort());
							downloader.getManager().startTrackerUpdate();
						}
						if (!getDownloaders().containsKey(downloader.getTorrent().getTorrentFileName())) {
							getDownloaders().put(downloader.getTorrent().getTorrentFileName(), downloader);
						}
						activeCount++;
						break;
					case STOP:
						break;
					case COMPLETE:
						String[] parts = ((String) msg.obj).split(",");
						if (getDownloaders().get(parts[0]) != null) {
							getDownloaders().get(parts[0]).stopDownloading();
							activeCount--;
						}

						TorrentService.generateNotification(context, "Download Complete", "Download Complete: "
								+ getDownloaders().get(parts[0]).getTorrent().getTorrentFileName());

						//Start the next queued download, if there is one
						if ((msg.what == COMPLETE) && (getDownloaders().size() > 1)) {
							Iterator<String> itr = getDownloaders().keySet().iterator();
							while (itr.hasNext() && (activeCount < Settings.getMaxActive())) {
								final Downloader current = getDownloaders().get(itr.next());
								if ("Queued".equals(current.getManager().getTorrent().getStatus())) {
									current.getManager().startListening(Settings.getLowPort(), Settings.getHighPort());
									current.getManager().startTrackerUpdate();
									activeCount++;
								}
							}
						}
						break;
					case QUIT:
					case STOP_ALL:
						Log.i(TAG, "Stopping Downloaders");
                        for(Downloader downloaderToStop : getDownloaders().values()) {
                            downloaderToStop.stopDownloading();
						}
						activeCount = 0;
						break;
					case DOWNLOADING:
						try {
							//getDownloaders().get(msg.obj.toString()).update();
							getDownloaders().get(msg.obj.toString()).getManager().getTorrent().setStatus("Downloading");
							getDownloaders().get(msg.obj.toString()).getManager().getTorrent().setErrorMessage(null);
						} catch (final NullPointerException e) {
							Log.e(TAG, e.getMessage());
						}
						break;
					case CONNECTING:
						try {
							Log.d(TAG, "Connecting: " + msg.obj.toString());
							getDownloaders().get(msg.obj.toString()).getManager().getTorrent().setStatus("Connecting to Tracker...");
						} catch (final NullPointerException e) {
							Log.e(TAG, e.getMessage());
						}
						break;
					case START_ALL:
						for (Downloader current : getDownloaders().values()) {
							if (current.isAlive()) {
								current.getManager().startListening(Settings.getLowPort(), Settings.getHighPort());
								current.getManager().startTrackerUpdate();
							} else {
								try {
									current.start();
								} catch (final IllegalThreadStateException e) {
									current.getManager().startListening(Settings.getLowPort(), Settings.getHighPort());
									current.getManager().startTrackerUpdate();
									//Log.i(TAG, "Thread already running");
								}
							}
						}
						activeCount = 0;
						break;
					case ERROR:
						if (msg.obj.toString().contains(",")) {
							parts = ((String) msg.obj).split(",");
							if (getDownloaders().get(parts[0]) != null) {
								getDownloaders().get(parts[0]).getManager().getTorrent().setErrorMessage(parts[1]);
							}
						}
						break;
					case UPDATE:
						if (msg.what == TorrentService.UPDATE) {
							final String[] torrentList = TorrentService.getTorrents().split(",");
							for (int t = 0; t < torrentList.length; t++) {
								if (torrentList[t].contains(msg.obj.toString()) && (torrentList[t].length() > 1)) {
									torrentList[t] = TorrentService.getDownloaders().get(torrentList[t].split(":")[0]).getTorrent().toString();
									TorrentService.setTorrents(TorrentService.getTorrentListString(torrentList));
									//Log.i(TAG, "Updated: " + TorrentService.getDownloaders().get(torrentList[t].split(":")[0]).getTorrent().toString());
								}
							}
							TorrentService.saveSettings();
						}
						break;
					default:
						break;
				}

				//Pop the messages to the UI, if the activity is open
				if (uiHandler != null) {
					//Log.i(TAG, "Sending message " + msg.what);
					uiHandler.sendMessage(uiHandler.obtainMessage(msg.what, msg.obj));
				}

				if (Settings.useOngoing()) {
					displayOngoingNotification();
				}
			} else if (msg != null) {
				switch (msg.what) {
					case QUIT:
						Log.i(TAG, "Quit called w/0 downloaders");
						break;
					default:
						break;
				}
			}
		}
	};

	/**
	 * Remove the ongoing notification
	 */
	public static void cancelOngoingNotification() {
		notifyManager.cancelAll();
		Log.i(TAG, "Ongoing notification canceled");
	}

	/**
	 * Display the ongoing notification
	 */
	public static void displayOngoingNotification() {
		notifyManager.notify(ONGOING_ID, generateOngoingNotify());
	}

	/**
	 * @param downloader downloader to add to the Map
	 */
	protected synchronized static void addDownloader(final Downloader downloader) {
		getDownloaders().put(downloader.getTorrent().getTorrentFileName(), downloader);
	}

	/**
	 * Add a torrent to the download queue
	 * 
	 * @param filename the torrent to download
	 * @param filenames The list of selected files from the torrent to download
	 */
	public synchronized static void addTorrent(final Context context, final String filename, final List<String> filenames, final String progress) {
		if ((getDownloaders().size() >= 1) && Utils.name.contains("Lite")) {
			Toast.makeText(context, "Lite is limited to one download at a time", Toast.LENGTH_LONG).show();
		} else {
			if (!getDownloaders().containsKey(filename)) {
				TorrentService.loadSettings(context);
				final Downloader downloader = new Downloader(filename, Settings.getDownloadPath(), filenames, progress);
					TorrentService.addDownloader(downloader);
					TorrentActivity.getDownloaders().add(downloader);
					try {
						TorrentActivity.getAdapter().notifyDataSetChanged();
					} catch (final Exception e) {
						Log.i(TAG, "Update called from different thread");
					}

					//Check to see if we are wifi only limited before starting
					if ((!Settings.isWifiOnly() || WifiStateReceiver.isWifiConnected(context)) && (!Settings.isTimeframeOnly() || TimeframeReceiver.isTimeframeActive())) {
						if (TorrentService.activeCount < Settings.getMaxActive()) {
							TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.START, downloader));
						}
					}

					if (!TorrentService.torrents.contains(filename)) {
						TorrentService.torrents = TorrentService.torrents.concat("," + filename);

						if (filenames != null) {
							final StringBuilder buffer = new StringBuilder();
							for (int n = 0; n < filenames.size(); n++) {
								buffer.append(':');
								buffer.append(filenames.get(n));
							}
							torrents = torrents.concat(buffer.toString());
						}
						torrents = torrents.concat(":");
						torrents = torrents.concat(downloader.getTorrent().getProgressString());

						saveSettings();
					}
			}
		}
	}

	/**
	 * Generate a notification when the download is complete
	 * 
	 * @param context the current context
	 * @param message the message to append to the alert
	 */
	public static void generateNotification(final Context context, final CharSequence title, final CharSequence message) {
		if (pendingIntent == null) {
			pendingIntent = PendingIntent.getActivity(context, 0, new Intent(context, TorrentActivity.class), 0);
		}
		final Notification notification = new Notification.Builder(context)
				.setSmallIcon(android.R.drawable.stat_sys_download_done)
				.setContentIntent(pendingIntent)
				.setContentTitle(title)
				.setContentText(message)
				.build();
		notifyManager.notify(NOTIFY_ID, notification);
	}

	/**
	 * @return notification showing current progress and number of active torrents
	 */
	protected static Notification generateOngoingNotify() {
		Notification notification;
		if (pendingIntent == null) {
			pendingIntent = PendingIntent.getActivity(context, 0, new Intent(context, TorrentActivity.class), 0);
		}

		int progress = 0;
		if ((getDownloaders() == null) || (getDownloaders().size() == 0)) {
			notification = new Notification.Builder(context)
				.setSmallIcon(android.R.drawable.stat_sys_download_done)
				.setContentIntent(pendingIntent)
				.setContentTitle("Red Torrent")
				.setContentText("Idle")
				.setOngoing(true)
				.build();
		} else {
            for(Downloader current: getDownloaders().values()){
				progress += current.getPercentDone();
			}
			notification = new Notification.Builder(context)
				.setSmallIcon(android.R.drawable.stat_sys_download_done)
				.setNumber(progress / getDownloaders().size())
				.setContentIntent(pendingIntent)
				.setContentTitle("Red Torrent")
				.setContentText(progress + "% complete (" + getDownloaders().size() + " active)")
				.setOngoing(true)
				.build();

//			if (notification.number == 100) {
//				notification.number = 0;
//				notification.icon = R.drawable.stat_sys_download_done;
//				notification.setLatestEventInfo(context, "Red Torrent", "Idle", pendingIntent);
//			}
		}
		return notification;
	}

	/**
	 * @return a List of downloaders
	 */
	public synchronized static List<Downloader> getDownloaderList() {
        final List<Downloader> list = new ArrayList<>(getDownloaders().size());
		for (Downloader current: getDownloaders().values()) {
			list.add(current);
		}
		return list;
	}

	/**
	 * @return the downloads
	 */
	public synchronized static Map<String, Downloader> getDownloaders() {
		synchronized (TorrentService.class) {
			if (downloaders == null) {
				downloaders = new Hashtable<>();
			}
			return downloaders;
		}
	}

	/**
	 * @return the message handler
	 */
	public synchronized static Handler getHandler() {
		return handler;
	}

	/**
	 * @return the ipAddress
	 */
	public static synchronized int getIpAddress() {
		return ipAddress;
	}

	/**
	 * @return the notifyManager
	 */
	public static synchronized NotificationManager getNotifyManager() {
		return notifyManager;
	}

	/**
	 * Creates a comma delimited string of torrent data
	 * 
	 * @param torrentList the array of Torrent information
	 * @return a serialized version of the torrent array data
	 */
	public synchronized static String getTorrentListString(final String[] torrentList) {
		final StringBuilder buffer = new StringBuilder();
		for (final String element : torrentList) {
			if (element.trim().length() > 0) {
				buffer.append(element);
				buffer.append(',');
			}
		}
		return buffer.toString();
	}

	/**
	 * @return the torrents
	 */
	public static String getTorrents() {
		synchronized (TorrentService.class) {
			return torrents;
		}
	}

	/**
	 * Handles Magnet links
	 * 
	 * @param intent the intent containing the magnet link data
	 */
	public static void handleMagnet(final Intent intent) {
		if (intent.getData().toString().startsWith("magnet:")) {
            try {
                Log.i(TAG, URLDecoder.decode(intent.getData().toString(), "UTF-8"));
            } catch (final UnsupportedEncodingException O_o) {
                Log.e(TAG, O_o.getMessage());
            }
            final TorrentFile magnetTorrent = new TorrentFile(intent.getData().toString());
			magnetTorrent.setSavePath(Settings.getDownloadPath());
			final UDPTracker client = new UDPTracker(magnetTorrent);
			client.getTrackerInfo(magnetTorrent.getAnnounceURLs().get(0));
			//Downloader down = new Downloader(magnetTorrent);

			//TODO download additional info from the trackers/DHT/PEX
			intent.setData(null);
		}
	}

	/**
	 * Load the settings from the SharedPreferences
	 * 
	 * @param context the current context
	 */
	public synchronized static void loadSettings(final Context context) {
		settings = context.getSharedPreferences(PREFERENCES, 0);
		torrents = settings.getString("torrents", "");
		Settings.setDownloadPath(settings.getString("saveTo", Environment.DIRECTORY_DOWNLOADS));
		Settings.setWifiOnly(settings.getBoolean("wifiOnly", false));
		Settings.setWifiResume(settings.getBoolean("wifiResume", false));
		Settings.setWifiNetworks(settings.getBoolean("wifiNetworks", false));
		Settings.setWifiNetworkSSIDs(settings.getString("SSIDs", ""));
		Settings.setTimeframeOnly(settings.getBoolean("timeframeOnly", false));
		Settings.setTimeframeResume(settings.getBoolean("timeframeResume", false));
		Settings.setStartHour(settings.getInt("startHour", 0));
		Settings.setStartMinute(settings.getInt("startMinute", 0));
		Settings.setEndHour(settings.getInt("endHour", 0));
		Settings.setEndMinute(settings.getInt("endMinute", 0));
		Settings.setUseDHT(settings.getBoolean("useDHT", true));
		Settings.setDownloadPath(settings.getString("downloadPath", Environment.DIRECTORY_DOWNLOADS));
		Settings.setMaxActive(settings.getInt("maxActive", 3));
		Settings.setMaxConnections(settings.getInt("maxConnections", 100));
		Settings.setMaxPieces(settings.getInt("maxPieces", 30));
		Settings.setLowPort(Short.parseShort(settings.getString("lowPortNum", "6881")));
		Settings.setHighPort(Short.parseShort(settings.getString("highPortNum", "6889")));
		Settings.setSearchProvider(settings.getInt("searchProvider", 0));
	}

	/**
	 * Remove a torrent file from the saved list
	 * 
	 * @param context the current application context
	 * @param position the DownloadListAdapter location of the torrent to remove
	 */
	public synchronized static void removeTorrent(final Context context, final int position) {
		try {
			final Downloader selected = TorrentActivity.getAdapter().getItem(position);
            if(selected !=null) {
                final String file = selected.getTorrent().getTorrentFileName();
                getDownloaders().remove(file);
                TorrentActivity.downloaders.remove(TorrentActivity.getAdapter().getItem(position));

                //Rebuild torrent list torrents from a saved config file
                TorrentService.loadSettings(context);
                final String[] torrentList = TorrentService.torrents.split(",");
                TorrentService.torrents = "";
                for (String torrent : torrentList) {
                    if (!torrent.contains(file) && (torrent.length() > 1)) {
                        TorrentService.torrents = TorrentService.torrents.concat("," + torrent);
                    }
                }
                TorrentService.saveSettings();

                if (Settings.useOngoing()) {
                    displayOngoingNotification();
                }
            }
		} catch (final IndexOutOfBoundsException e) {
			Log.w(TAG, "Torrent not found to remove");
		}
	}

	/**
	 * Writes the settings to the Preferences location
	 */
	public synchronized static boolean saveSettings() {
		boolean returnVal = true;
		if (settings != null) {
			final SharedPreferences.Editor editor = settings.edit();
			editor.putString("torrents", torrents);
			editor.putString("saveTo", Settings.getDownloadPath());
			editor.putBoolean("wifiOnly", Settings.isWifiOnly());
			editor.putBoolean("wifiResume", Settings.isWifiResume());
			editor.putBoolean("wifiNetworks", Settings.isWifiNetworks());
			editor.putString("SSIDs", Settings.getWifiNetworkSSIDString());
			editor.putBoolean("pluggedInOnly", Settings.isPluggedInOnly());
			editor.putBoolean("pluggedInResume", Settings.isPluggedInResume());
			editor.putBoolean("timeframeOnly", Settings.isTimeframeOnly());
			editor.putBoolean("timeframeResume", Settings.isTimeframeResume());
			editor.putInt("startHour", Settings.getStartHour());
			editor.putInt("startMinute", Settings.getStartMinute());
			editor.putInt("endHour", Settings.getEndHour());
			editor.putInt("endMinute", Settings.getEndMinute());
			editor.putBoolean("useDHT", Settings.isUseDHT());
			editor.putString("downloadPath", Settings.getDownloadPath());
			editor.putInt("maxActive", Settings.getMaxActive());
			editor.putInt("maxConnections", Settings.getMaxConnections());
			editor.putInt("maxPieces", Settings.getMaxPieces());
			editor.putString("lowPortNum", Short.toString(Settings.getLowPort()));
			editor.putString("highPortNum", Short.toString(Settings.getHighPort()));
			editor.putInt("searchProvider", Settings.getSearchProvider());
			editor.apply();
		} else {
			returnVal = false;
		}
		return returnVal;
	}

	/**
	 * Broadcast an intent
	 * 
	 * @param intent the intent to broadcast
	 */
	public static void sendIntent(final Intent intent) {
		context.sendBroadcast(intent);
	}

	/**
	 * @param handler set the Message Handler to the specified handler
	 */
	public synchronized static void setHandler(final Handler handler) {
		TorrentService.handler = handler;
	}

	/**
	 * @param ipAddress the ipAddress to set
	 */
	public static synchronized void setIpAddress(final int ipAddress) {
		TorrentService.ipAddress = ipAddress;
	}

	/**
	 * @param torrents the torrents to set
	 */
	public static synchronized void setTorrents(final String torrents) {
		TorrentService.torrents = torrents;
	}

	/**
	 * @param uiHandler the uiHandler to set
	 */
	public synchronized static void setUiHandler(final Handler uiHandler) {
		TorrentService.uiHandler = uiHandler;
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Service#onBind(android.content.Intent)
	 */
	@Override
	public IBinder onBind(final Intent intent) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Service#onDestroy()
	 */
	@Override
	public void onDestroy() {
		notifyManager.cancel(ONGOING_ID);
		notifyManager.cancel(NOTIFY_ID);
		try {
			unregisterReceiver(wiFiReceiver);
			unregisterReceiver(powerReceiver);
		} catch (final IllegalArgumentException e) {
			Log.w(TAG, "onDestroy: " + e.getMessage());
		}
		for (Downloader current: getDownloaders().values() ) {
			current.stopDownloading();
			current.getFileSetupTask().cancel(true);
		}
		stopForeground(true);
		stopSelf();
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Service#onStart(android.content.Intent, int)
	 */
	@Override
	public void onStart(final Intent intent, final int startId) {
		context = getApplicationContext();
		wiFiReceiver = new WifiStateReceiver();
		powerReceiver = new PowerStateReceiver();
		notifyManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		//Add actions
		final IntentFilter wifiFilter = new IntentFilter();
		final IntentFilter powerFilter = new IntentFilter();
		wifiFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
		wifiFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
		powerFilter.addAction(Intent.ACTION_POWER_CONNECTED);
		powerFilter.addAction(Intent.ACTION_POWER_DISCONNECTED);

		registerReceiver(wiFiReceiver, wifiFilter);
		registerReceiver(powerReceiver, powerFilter);

		final WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
		ipAddress = wifiManager.getConnectionInfo().getIpAddress();
		loadSettings(context);
		parseTorrents();
	}

	/**
	 * Break the serialized version of the torrent data
	 */
	public synchronized void parseTorrents() {
		final String[] torrentList = getTorrents().split(",");

		for (final String element : torrentList) {
			if (element.contains(".torrent")) {
				final String[] torrentParts = element.split(":");
				if (torrentParts.length == 1) {
					addTorrent(getBaseContext(), torrentParts[0], null, null);
				} else if ((torrentParts.length == 2) && torrentParts[1].matches("[0-1]+")) {
					addTorrent(getBaseContext(), torrentParts[0], null, torrentParts[1]);
				} else {
					final List<String> files = new ArrayList<>();
					String progress = null;
					for (int f = 1; f < torrentParts.length; f++) {
						if (torrentParts[f].matches("[0-1]+")) {
							progress = torrentParts[f];
						} else {
							files.add(torrentParts[f]);
						}
					}

					addTorrent(getBaseContext(), torrentParts[0], files, progress);
				}
			}
		}
	}
}