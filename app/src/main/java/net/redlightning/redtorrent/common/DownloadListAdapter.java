/*
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.common;

import java.text.DecimalFormat;
import java.util.List;
import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.redlightning.redtorrent.R;

/**
 * @author Michael Isaacson
 * @version 11.2.7
 */
public class DownloadListAdapter extends ArrayAdapter<Downloader> {
	private final transient TorrentActivity context;
	private final transient List<Downloader> items;
	private static int layout;
	private static int name;
	private static int speed;
	private static int progress;
	private static int peers;
	private static int status;
	private static int error;
	private static int allocation;

	/**
	 * Sets the IDs of all views
	 * 
	 * @param layoutID the R.id for the row layout
	 * @param nameView the R.id for the Name view
	 * @param speedView the R.id for the Speed view
	 * @param statusView the R.id for the Status view
	 * @param allocationView the R.id for the Allocation/Validation view
	 * @param peersView the R.id for the Connected Peers view
	 * @param errorView the R.id for the Error view
	 * @param progressView the R.id for the progress bar
	 */
	public static void setViews(final int layoutID, final int nameView, final int speedView, final int statusView, final int allocationView, final int peersView, final int errorView, final int progressView) {
		layout = layoutID;
		name = nameView;
		speed = speedView;
		status = statusView;
		peers = peersView;
		error = errorView;
		allocation = allocationView;
		progress = progressView;
	}

	/**
	 * Constructor
	 * 
	 * @param bundle the current context
	 * @param resourceID the view ID
	 * @param items the list of items to add
	 */
	public DownloadListAdapter(final TorrentActivity bundle, final int resourceID, final List<Downloader> items) {
		super(bundle, resourceID, items);
		this.items = items;
		context = bundle;
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	@NonNull
	public View getView(final int position, final View convertView, @NonNull final ViewGroup parent) {
		View view = convertView;

		if (convertView == null) {
			LayoutInflater inflater;

			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(layout, null);
		}
		if (items.size() > 0) {
			if (items.get(position) != null) {
				setNameView(view, (TextView) view.findViewById(name), items.get(position), position);
				setSpeedView((TextView) view.findViewById(speed), items.get(position));
				setStatusView((TextView) view.findViewById(status), items.get(position));
				setAllocationView((TextView) view.findViewById(allocation), items.get(position));
				setPeersView((TextView) view.findViewById(peers), items.get(position));
				setErrorView((TextView) view.findViewById(error), items.get(position));
				setProgressView((ProgressBar) view.findViewById(progress), items.get(position));
			}
		}
		return view;
	}

	/**
	 * Sets the text of the Name view for this torrent
	 * 
	 * @param rowLayout the layout of the Downloader row
	 * @param view the view that will display the name of the torrent
	 * @param item the Downloader object
	 * @param position the position in the list adapter this view is in
	 */
	private void setNameView(final View rowLayout, final TextView view, final Downloader item, final int position) {
		if (view != null) {
			rowLayout.setOnClickListener(new TorrentClickListener(context, position));
			if (item.getTorrent().getTorrentFileName().contains("/")) {
				view.setText(item.getTorrent().getTorrentFileName().substring(item.getTorrent().getTorrentFileName().lastIndexOf('/') + 1, item.getTorrent().getTorrentFileName().indexOf(".torrent")));
			} else if (item.getTorrent().getTorrentFileName().contains(".torrent")) {
				view.setText(item.getTorrent().getTorrentFileName().substring(0, item.getTorrent().getTorrentFileName().indexOf(".torrent")));
			} else {
				view.setText(item.getTorrent().getTorrentFileName());
			}
		}
	}

	/**
	 * Set the error view's text, or hide it if there is no error
	 * 
	 * @param view for showing the text of the error
	 * @param item the Downloader object
	 */
	private void setErrorView(final TextView view, final Downloader item) {
		if (item.getTorrent().getErrorMessage() != null && item.getPercentDone() != 100.0 && !"Stopped".equalsIgnoreCase(item.getTorrent().getStatus())) {
			view.setVisibility(View.VISIBLE);
			view.setText(item.getTorrent().getErrorMessage());
		} else {
			view.setVisibility(View.GONE);
		}
	}

	/**
	 * Set the progress bar's progress value
	 * 
	 * @param view the ProgressBar view for this Downloader
	 * @param item the Downloader object
	 */
	private void setProgressView(final ProgressBar view, final Downloader item) {
		if (view != null) {
			if (item.getPercentDone() == 100.0) {
				view.setVisibility(View.GONE);
			} else {
				view.setVisibility(View.VISIBLE);
				view.setProgress((int) item.getPercentDone());
			}
		}
	}

	/**
	 * Set the speed view's text
	 * 
	 * @param view the view displating the current speed
	 * @param item the Downloader object
	 */
	private void setSpeedView(final TextView view, final Downloader item) {
		if (view != null) {
			if (item.getPercentDone() == 100.0) {
				view.setText(R.string.complete);
				view.setVisibility(View.VISIBLE);
			} else if (!"Stopped".equalsIgnoreCase(item.getTorrent().getStatus())) {
				view.setVisibility(View.VISIBLE);
				view.setText("Speed - " + new DecimalFormat("###0.0").format((float) item.getRate() / (float) 1024) + " kbps");
			} else {
				view.setVisibility(View.GONE);
			}
		}
	}

	/**
	 * Set the name view's text
	 * 
	 * @param view the view that will hold the torrent's name
	 * @param item the Downloader object
	 */
	private void setStatusView(final TextView view, final Downloader item) {
		if (view != null) {
			if (item.getPercentDone() == 100.0) {
				view.setVisibility(View.GONE);
			} else {
				view.setVisibility(View.VISIBLE);
				if (item.getManager().getTorrent().getStatus().equalsIgnoreCase("Downloading")) {
					view.setText("Downloading - " + new DecimalFormat("##0.00").format(item.getPercentDone()) + "%");
					if (item.getManager().getTorrent().getNote() != null) {
						view.setText(view.getText() + item.getManager().getTorrent().getNote());
					}
				} else {
					view.setText(item.getManager().getTorrent().getStatus());
				}
			}
		}
	}

	/**
	 * Sets the text of the view showing the allocation/verification progress
	 * 
	 * @param view the view to set the text of
	 * @param item the Downloader object
	 */
	private void setAllocationView(final TextView view, final Downloader item) {
		if (view != null) {
			if ("Stopped".equalsIgnoreCase(item.getTorrent().getStatus())) {
				view.setVisibility(View.GONE);
			} else if (item.getTorrent().getBlocked() != -1 && item.getPercentDone() != 100.0) {
				view.setVisibility(View.VISIBLE);
				view.setText("Setting Up Files - " + (item.getTorrent().getBlocked() + 1) + "/" + item.getTorrent().getLength().size());
			} else if (item.getTorrent().getAllocated() < item.getTorrent().getPieceHashAsBinary().size() - 1 && item.getPercentDone() != 100.0) {
				view.setVisibility(View.VISIBLE);
				view.setText("Validating - " + item.getTorrent().getAllocated() + "/" + item.getTorrent().getPieceHashAsBinary().size());
			} else {
				view.setVisibility(View.GONE);
			}
		}
	}

	/**
	 * Sets the text of the view showing the number of connected peers
	 * 
	 * @param view shows the number of connected peers
	 * @param item the Downloader object
	 */
	private void setPeersView(final TextView view, final Downloader item) {
		if (view != null) {
			if (item.getPercentDone() == 100.0 || "Stopped".equalsIgnoreCase(item.getTorrent().getStatus())) {
				view.setVisibility(View.GONE);
			} else {
				view.setVisibility(View.VISIBLE);
				int peerCount = item.getManager().getPeerList().size();
				if (peerCount == 0) {
					view.setText("Peers - 0/0");
				} else {
					view.setText("Peers - " + item.getManager().getConnectionCount() + "/" + peerCount);
				}
			}
		}
	}
}
