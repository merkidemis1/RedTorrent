/*
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent;

import java.io.File;
import net.redlightning.redtorrent.RedTorrent;
import net.redlightning.redtorrent.common.Settings;
import net.redlightning.redtorrent.common.TorrentActivity;
import net.redlightning.redtorrent.common.TorrentService;
import android.annotation.SuppressLint;
import android.test.ActivityInstrumentationTestCase2;

import com.robotium.solo.Solo;

/**
 * Tests for net.redlightning.redtorrent.Settings
 * 
 * @author Michael Isaacson
 * @version 12.7.5
 */
public class SettingsActivityTest extends ActivityInstrumentationTestCase2<RedTorrent> {
	private Solo robot;

	/**
	 * Default constructor
	 */
	public SettingsActivityTest() {
		super("net.redlightning.redtorrent", RedTorrent.class);
	}

	/**
	 * Constructor
	 * 
	 * @param pkg the full package name
	 * @param activityClass the class under test that extends Activity
	 */
	public SettingsActivityTest(final String pkg, final Class<RedTorrent> activityClass) {
		super(pkg, activityClass);
	}

	/*
	 * (non-Javadoc)
	 * @see android.test.ActivityInstrumentationTestCase2#setUp()
	 */
	@Override
	public void setUp() throws Exception {
		super.setUp();
		robot = new Solo(getInstrumentation(), getActivity());
		while (TorrentActivity.getAdapter().getCount() > 0) {
			TorrentService.removeTorrent(getActivity(), 0);
		}
	}

	/**
	 * Test to make sure we can change the download path
	 */
	@SuppressLint("SdCardPath")
	public void testDownloadPathSettings() {
		assertEquals("RedTorrent", robot.getCurrentActivity().getLocalClassName());

		robot.clickOnMenuItem("Settings");
		robot.sleep(200);

		assertEquals("SettingsActivity", robot.getCurrentActivity().getLocalClassName());

		//Clear the text input
		robot.goBack();

		//Create a temporary folder for testing
		new File("/sdcard/redTorrentTemp/").delete();
		assertTrue(new File("/sdcard/redTorrentTemp/").mkdir());
		
		//Create download folder if it doesn't exist (typically for emulators)
		new File("/sdcard/download/").mkdir(); 

		robot.clickOnEditText(2);
		robot.clearEditText(2);
		robot.enterText(2, "/sdcard/redTorrentTemp");
		robot.clickOnEditText(1);
		robot.sleep(1000);
		assertEquals("/sdcard/redTorrentTemp/", Settings.getDownloadPath());

		//Invalid: path does not exist
		robot.clickOnEditText(2);
		robot.clearEditText(2);
		robot.enterText(2, "/sdcard/doesNotExist/");
		robot.clickOnEditText(1);
		robot.sleep(1000);
		assertEquals("/sdcard/redTorrentTemp/", Settings.getDownloadPath());

		robot.clickOnEditText(2);
		robot.clearEditText(2);
		robot.enterText(2, "/sdcard/Download/");
		robot.clickOnEditText(1);
		robot.sleep(1000);
		assertEquals("/sdcard/Download/", Settings.getDownloadPath());

		robot.clickOnButton("...");
		robot.sleep(1000);
		assertEquals("SavePathBrowser", robot.getCurrentActivity().getLocalClassName());
		//TODO validate choosing a path actually changes the download path
		robot.clickLongOnScreen(50.0f, 150.0f, 5);
		//robot.clickLongOnText("/mnt/sdcard/redTorrentTemp/");
		robot.sleep(20);
		assertNotSame("/sdcard/download/", Settings.getDownloadPath());
		assertEquals("SettingsActivity", robot.getCurrentActivity().getLocalClassName());
		
		assertTrue(new File("/sdcard/redTorrentTemp/").delete()); //Cleanup the temporary folder
		robot.goBack();
		robot.goBack();
		Settings.setDownloadPath("/sdcard/Download/");

	}
	
	/**
	 * Test to make sure the persistent notification is displayed only when the option is enabled
	 */
	public void testNotificationSettings() {
		assertEquals("RedTorrent", robot.getCurrentActivity().getLocalClassName());

		robot.clickOnMenuItem("Settings");
		robot.sleep(200);

		assertEquals("SettingsActivity", robot.getCurrentActivity().getLocalClassName());

		//Get off the port input keyboard
		robot.goBack();
		robot.scrollUp();

		assertTrue(Settings.useOngoing());
		robot.clickOnCheckBox(6);
		robot.sleep(200);
		assertFalse(Settings.useOngoing());

		//TODO start a torrent downloading to make sure it isn't using the ongoing, then cancel it

		robot.clickOnCheckBox(6);
		robot.sleep(200);
		assertTrue(Settings.useOngoing());
		robot.goBack();
		robot.goBack();

	}

	/**
	 * Test to make sure the persistent notification is displayed only when the option is enabled
	 */
	public void testPluggedInSettings() {
		assertEquals("RedTorrent", robot.getCurrentActivity().getLocalClassName());

		robot.clickOnMenuItem("Settings");
		robot.sleep(200);

		assertEquals("SettingsActivity", robot.getCurrentActivity().getLocalClassName());

		//Get off the port input keyboard
		robot.goBack();
		robot.scrollUp();

		assertFalse(Settings.isPluggedInOnly());
		assertFalse(Settings.isPluggedInResume());

		robot.clickOnCheckBox(3);
		robot.clickOnCheckBox(4);
		robot.sleep(200);
		assertTrue(Settings.isPluggedInOnly());
		assertTrue(Settings.isPluggedInResume());

		robot.clickOnCheckBox(3);
		robot.clickOnCheckBox(4);
		robot.sleep(200);
		assertFalse(Settings.isPluggedInOnly());
		assertFalse(Settings.isPluggedInResume());
		robot.goBack();
		robot.goBack();
	}

	public void testPortsSettings() {
		assertEquals("RedTorrent", robot.getCurrentActivity().getLocalClassName());

		robot.clickOnMenuItem("Settings");
		robot.sleep(200);

		assertEquals("SettingsActivity", robot.getCurrentActivity().getLocalClassName());

		//Clear the text input
		//Valid, but higher then HIGHPORT
		robot.goBack();
		robot.clearEditText(0);
		robot.enterText(0, "7000");
		robot.clickOnEditText(1);
		robot.sleep(1000);
		assertEquals(7000, Settings.getLowPort());
		assertEquals(7001, Settings.getHighPort());

		//Invalid, too low (low port)
		robot.clickOnEditText(0);
		robot.clearEditText(0);
		robot.enterText(0, "19");
		robot.clickOnEditText(1);
		robot.sleep(1000);
		assertEquals(20, Settings.getLowPort());
		assertEquals(7001, Settings.getHighPort());

		//Invalid, too low (high port)
		robot.clickOnEditText(1);
		robot.clearEditText(1);
		robot.enterText(1, "19");
		robot.clickOnEditText(0);
		robot.sleep(1000);
		assertEquals(20, Settings.getLowPort());
		assertEquals(21, Settings.getHighPort());

		//Invalid, too high (low port)
		robot.clickOnEditText(0);
		robot.clearEditText(0);
		robot.enterText(0, "32767");
		robot.clickOnEditText(1);
		robot.sleep(1000);
		assertEquals(32766, Settings.getLowPort());
		assertEquals(32767, Settings.getHighPort());

		//Invalid, too high (high port)
		robot.clickOnEditText(1);
		robot.clearEditText(1);
		robot.enterText(1, "32768");
		robot.clickOnEditText(0);
		robot.sleep(1000);
		assertEquals(32766, Settings.getLowPort());
		assertEquals(32767, Settings.getHighPort());

		//Invalid (non-numeric)
		robot.clickOnEditText(0);
		robot.clearEditText(0);
		robot.enterText(0, "TEST");
		robot.clickOnEditText(1);
		robot.sleep(1000);
		assertEquals(6881, Settings.getLowPort());
		robot.clearEditText(1);
		robot.enterText(1, "PORT");
		robot.clickOnEditText(0);
		robot.sleep(1000);
		assertEquals(6881, Settings.getLowPort());
		assertEquals(6889, Settings.getHighPort());

		//Reset to defaults
		robot.clickOnEditText(0);
		robot.clearEditText(0);
		robot.enterText(0, "6881");
		robot.clickOnEditText(1);
		robot.sleep(1000);
		assertEquals(6881, Settings.getLowPort());
		robot.clearEditText(1);
		robot.enterText(1, "6889");
		robot.clickOnEditText(0);
		robot.sleep(1000);
		assertEquals(6881, Settings.getLowPort());
		assertEquals(6889, Settings.getHighPort());

		robot.goBack();
		robot.goBack();
	}

	/**
	 * Test to make sure the persistent notification is displayed only when the option is enabled
	 */
	public void testSliderSettings() {
		assertEquals("RedTorrent", robot.getCurrentActivity().getLocalClassName());

		robot.clickOnMenuItem("Settings");
		robot.sleep(200);

		assertEquals("SettingsActivity", robot.getCurrentActivity().getLocalClassName());

		//Get off the port input
		robot.goBack();
		robot.scrollDown();
		robot.scrollDown();
		
		//Max Active Torrents
		robot.setProgressBar(0, 5);
		robot.sleep(200);
		assertEquals(6, Settings.getMaxActive());
		
		robot.setProgressBar(0, 2);
		robot.sleep(200);
		assertEquals(3, Settings.getMaxActive());
		
		//Max Connections
		robot.setProgressBar(1, 5);
		robot.sleep(200);
		assertEquals(60, Settings.getMaxConnections());
		
		robot.setProgressBar(1, 4);
		robot.sleep(200);
		assertEquals(50, Settings.getMaxConnections());
		
		//Max Pieces
		robot.setProgressBar(2, 9);
		robot.sleep(200);
		assertEquals(50, Settings.getMaxPieces());
		
		robot.setProgressBar(2, 5);
		robot.sleep(200);
		assertEquals(30, Settings.getMaxPieces());
		robot.goBack();
		robot.goBack();
	}


	
	/**
	 * Test to make sure the persistent notification is displayed only when the option is enabled
	 */
	public void testTimeframeSettings() {
		assertEquals("RedTorrent", robot.getCurrentActivity().getLocalClassName());

		robot.clickOnMenuItem("Settings");
		robot.sleep(200);

		assertEquals("SettingsActivity", robot.getCurrentActivity().getLocalClassName());

		//Get off the port input
		robot.goBack();
		robot.scrollUp();

		assertFalse(Settings.isTimeframeOnly());
		assertFalse(Settings.isTimeframeResume());

		robot.clickOnCheckBox(5);
		robot.sleep(200);
		assertTrue(Settings.isTimeframeOnly());

		//Start Time
		robot.setTimePicker(0, 12, 30);
		robot.sleep(200);
		assertEquals(12, Settings.getStartHour());
		assertEquals(30, Settings.getStartMinute());

		robot.setTimePicker(0, 20, 10);
		robot.sleep(200);
		assertEquals(20, Settings.getStartHour());
		assertEquals(10, Settings.getStartMinute());

		robot.setTimePicker(0, 0, 0);
		robot.sleep(200);

		robot.scrollDown();
		robot.sleep(200);
		robot.scrollUp();

		//End Time
		robot.setTimePicker(1, 16, 50);
		robot.sleep(200);
		assertEquals(16, Settings.getEndHour());
		assertEquals(50, Settings.getEndMinute());

		robot.scrollDown();
		robot.sleep(200);
		robot.scrollUp();

		robot.setTimePicker(1, 5, 10);
		robot.sleep(200);
		assertEquals(5, Settings.getEndHour());
		assertEquals(10, Settings.getEndMinute());

		robot.scrollDown();
		robot.sleep(200);
		robot.scrollUp();

		robot.setTimePicker(1, 12, 0);

		robot.scrollDown();
		robot.sleep(200);
		robot.scrollUp();
		robot.scrollUp();

		robot.clickOnCheckBox(5);
		robot.sleep(200);
		assertFalse(Settings.isTimeframeOnly());
		robot.goBack();
		robot.goBack();
	}

	/**
	 * Test changing WiFi related settings via the GUI (WiFi Only and Resume)
	 */
	public void testWiFiSettings() {
		assertEquals("RedTorrent", robot.getCurrentActivity().getLocalClassName());
		robot.clickOnMenuItem("Settings");
		robot.sleep(1000);
		assertEquals("SettingsActivity", robot.getCurrentActivity().getLocalClassName());
		assertFalse(Settings.isWifiOnly());

		//Get off the port input
		robot.goBack();
		robot.scrollUp();

		robot.clickOnCheckBox(0);
		robot.sleep(200);
		assertTrue(Settings.isWifiOnly());

		//Auto-resume on wifi
		assertFalse(Settings.isWifiResume());
		robot.clickOnCheckBox(1);
		robot.sleep(200);
		assertTrue(Settings.isWifiResume());
		robot.clickOnCheckBox(1);
		robot.sleep(200);
		assertFalse(Settings.isWifiResume());

		//TODO test wifi networks restriction

		//Turn WiFi only back off
		robot.clickOnCheckBox(0);
		robot.sleep(200);
		assertFalse(Settings.isWifiOnly());
		robot.goBack();
		robot.goBack();
	}
}