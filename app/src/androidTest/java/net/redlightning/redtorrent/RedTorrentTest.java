/*
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent;

import net.redlightning.redtorrent.common.TorrentActivity;
import net.redlightning.redtorrent.common.TorrentService;
import android.app.Dialog;
import android.content.Intent;
import android.os.Environment;
import android.test.ActivityInstrumentationTestCase2;

import com.robotium.solo.Solo;

/**
 * UI tests for RedTorrent and TorrentActivity
 * 
 * @author Michael Isaacson
 * @version 11.12.14
 */
public class RedTorrentTest extends ActivityInstrumentationTestCase2<RedTorrent> {
	private Solo robot;

	/**
	 * Constructor
	 * 
	 * @param pkg the full package name
	 * @param activityClass the class under test that extends Activity
	 */
	public RedTorrentTest(final String pkg, final Class<RedTorrent> activityClass) {
		super(pkg, activityClass);
	}

	/**
	 * Default constructor
	 */
	public RedTorrentTest() {
		super("net.redlightning.redtorrent", RedTorrent.class);
	}

	/*
	 * (non-Javadoc)
	 * @see android.test.ActivityInstrumentationTestCase2#setUp()
	 */
	@Override
	public void setUp() throws Exception {
		super.setUp();
		robot = new Solo(getInstrumentation(), getActivity());
		while (RedTorrent.getAdapter().getCount() > 0) {
			TorrentService.removeTorrent(this.getActivity(), 0);
		}
	}

	/**
	 * Test of net.redlightning.redtorrent.RedTorrent.onCreate(Bundle)
	 */
	public void testOnCreate() {
		assertNotNull(RedTorrent.getAdapter());
		assertNotNull(RedTorrent.downloaders);
	}

	/**
	 * Test of net.redlightning.redtorrent.RedTorrent.onResume()
	 */
	public void testOnResume() {
		fail("Not yet implemented");
	}

	/**
	 * Test of net.redlightning.redtorrent.RedTorrent.onMenuItemSelected
	 */
	public void testOnMenuItemSelected() {
		robot.clickOnMenuItem("Add Torrent");
		robot.sleep(5000);
		assertEquals("DirectoryBrowser", robot.getCurrentActivity().getLocalClassName());
		robot.goBack();

		robot.clickOnMenuItem("Search");
		//TODO assert that the search box is showing
		robot.sleep(2000);
		robot.goBack();
		//TODO check to see if the search edit box is still visible
		robot.sleep(2000);
		robot.goBack();

		robot.clickOnMenuItem("Stop All");
		robot.sleep(2000);

		robot.clickOnMenuItem("Settings");
		robot.sleep(5000);
		assertEquals("SettingsActivity", robot.getCurrentActivity().getLocalClassName());
		robot.goBack();
		if ("SettingsActivity".equals(robot.getCurrentActivity().getLocalClassName())) {
			robot.goBack();
		}
		robot.clickOnMenuItem("Quit");
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.common.TorrentActivity#getAdapter()}.
	 */
	public void testGetAdapter() {
		assertEquals(0, TorrentActivity.getAdapter().getCount());
	}


	/**
	 * Test method for {@link net.redlightning.redtorrent.common.TorrentActivity#onActivityResult(int, int, android.content.Intent)}
	 */
	public void testOnActivityResult() {
		final Intent intent = new Intent();
		intent.putExtra("file", Environment.DIRECTORY_DOWNLOADS);

		//Test the negative button
		getActivity().onActivityResult(TorrentActivity.FILE_BROWSER, TorrentActivity.RESULT_OK, intent);
		assertNotNull(getActivity().pickerDialog);
		assertTrue(getActivity().pickerDialog.isShowing());
		getActivity().pickerDialog.getButton(Dialog.BUTTON_NEGATIVE).performClick();
		//assertFalse(getActivity().pickerDialog.isShowing());
		assertEquals(0, RedTorrent.getAdapter().getCount());

		//Test the positive button
		getActivity().onActivityResult(TorrentActivity.FILE_BROWSER, TorrentActivity.RESULT_OK, intent);
		assertNotNull(getActivity().pickerDialog);
		assertTrue(getActivity().pickerDialog.isShowing());
		getActivity().pickerDialog.getButton(Dialog.BUTTON_POSITIVE).performClick();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//assertFalse(getActivity().pickerDialog.isShowing());
		assertEquals(1, RedTorrent.getAdapter().getCount());
		TorrentService.removeTorrent(this.getActivity(), 0);
		assertEquals(0, RedTorrent.getAdapter().getCount());
	}

	/**
	 * Test method for {@link net.redlightning.redtorrent.common.TorrentActivity#onDestroy()}.
	 */
	public void testOnDestroy() {
		getActivity().onDestroy();
		assertNull(TorrentService.uiHandler);
	}
}
